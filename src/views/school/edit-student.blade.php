@extends('app')


@section('content')
    <h3 class="page-heading">
        Μαθητής: {{ $student->last_name . ' ' . $student->first_name }}
    </h3>

    @include('errors.list')

    @if($type == 'Λύκειο' || $type == 'ΕΠΑΛ')
        {{ Form::model($student, ['method'=>'POST', 'class'=>'form-horizontal', 'route'=> ['OFA::updateStudent', $student->am]]) }}
    @else
        {{ Form::model($student, ['method'=>'POST', 'class'=>'form-horizontal', 'route'=> ['OFA::primary.updateStudent', $student->am]]) }}
    @endif
        <div class="row">
            <div class="col-md-6">
                <div class="form-group form-inline">
                    {{ Form::label('am', 'Αριθμός Μητρώνου:', ['class'=>'col-md-3 control-label']) }}
                    {{ Form::text('am',null, ['class'=>'form-control', 'tabindex' => 1]) }}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group form-inline">
                    {{ Form::label('year_birth', 'Έτος Γέννησης:', ['class'=>'col-md-3 control-label']) }}
                    {{ Form::text('year_birth',null, ['class'=>'form-control', 'tabindex' => 2]) }}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group form-inline">
                    {{ Form::label('last_name', 'Επώνυμο:', ['class'=>'col-md-3 control-label']) }}
                    {{ Form::text('last_name',null, ['class'=>'form-control', 'tabindex' => 3]) }}
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group form-inline">
                    {{ Form::label('first_name', 'Όνομα:', ['class'=>'col-md-3 control-label']) }}
                    {{ Form::text('first_name',null, ['class'=>'form-control', 'tabindex' => 4]) }}
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group form-inline">
                    {{ Form::label('middle_name', 'Πατρώνυμο:', ['class'=>'col-md-3 control-label']) }}
                    {{ Form::text('middle_name',null, ['class'=>'form-control', 'tabindex' => 5]) }}
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group form-inline">
                    {{ Form::label('mothers_name', 'Μητρώνυμο:', ['class'=>'col-md-3 control-label']) }}
                    {{ Form::text('mothers_name',null, ['class'=>'form-control', 'tabindex' => 6]) }}
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group form-inline">
                    {{ Form::label('class', 'Τάξη:', ['class'=>'col-md-3 control-label']) }}
                    {{ Form::select('class',Config::get('requests.class'),null, ['class'=>'form-control', 'tabindex' => 7]) }}

                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group form-inline">
                    {{ Form::label('sex', 'Φύλο:', ['class'=>'col-md-3 control-label']) }}
                    {{ Form::select('sex',['Κορίτσι', 'Αγόρι'],null, ['class'=>'form-control', 'tabindex' => 8]) }}

                </div>
            </div>

            <div class="col-md-6 col-md-offset-3">
                {{ Form::submit('Ενημέρωση στοιχείων μαθητή', ['class' => 'btn btn-success']) }}
            </div>
        </div>
    {{ Form::close() }}

@endsection


