@extends('app')

@section('header.style')
    <meta id="token" name="csrf-token" content="{{csrf_token()}}">

    <style>
        #periodRoutes{

        }
        #participation-status thead tr th{
            text-align: center;
        }

        .suggestion-fake{
            color: red;
            font-weight: bold;
        }
        .button-change{
            color : red;
        }

        .pointers {
            cursor: pointer;
        }

        .delete-button{
            padding-top: 15px !important;
            color: red;
            font-weight: bold;
        }

        .loading{
            width: 80px;
            height: 80px;
        }

        .school-header{
            color: #006666;
            font-size: 15pt;
            font-weight: 700;
            font-family: 'Merriweather', 'Helvetica Neue', Arial, sans-serif;
        }
        .is_participate{
            background-color: #90EE90;
            font-weight: bold;
        }

    </style>
@endsection

@section('content')
    <div class="row">
        <h2 class="page-heading">Κατάσταση Συμμετοχής Ομαδικών Αθλημάτων</h2>

        <div class="panel panel-primary">
            <div v-if="loading" class="panel-body text-center">
                <img class="loading" src="{{ asset('images/Ripple.gif') }}"/>
                <h5>παρακαλώ περιμένετε...</h5>
            </div>

            <div v-cloak v-else class="panel-body">
                <div class="col-md-4">
                    <div class="form-group form-inline">
                        <label>Σχολικό Έτος:</label>
                        <select v-model="current_year" class="form-control" @change="fetchAllStudentsByGender">
                            <option selected v-bind:value="nullValue">Επέλεξε Έτος</option>
                            <option v-for="year in dataSet.years" v-bind:value="year.id">
                                @{{ year.name }}
                            </option>
                        </select>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group form-inline">
                        <label>Άθλημα:</label>
                        <select class="form-control" v-cloak @change="displayGender(current_sport)" v-model="current_sport">
                            <option selected v-bind:value="nullValue">Επέλεξε Άθλημα</option>
                            <option v-for="sport in dataSet.sports" v-bind:value="sport">
                                @{{ sport.name }}
                            </option>
                        </select>
                        <select class="form-control" v-cloak v-if="current_sport != null" @change="fetchAllStudentsByGender" v-model="current_sex">
                            <option selected v-bind:value="nullValue" style="background-color: #00a65a">Επέλεξε φύλο</option>
                            <option v-for="(gender, index) in sex" v-bind:value="index">
                                @{{ gender }}
                            </option>
                        </select>
                    </div>

                    <div v-if="showPhase">
                        <div v-if="loadingPhaseDiv" class="panel-body text-center">
                            <img class="loading" src="{{ asset('images/Ripple.gif') }}"/>
                            <h5>παρακαλώ περιμένετε...</h5>
                        </div>
                        <div v-else>
                            <div v-if="listExists" class="form-group form-inline">
                                <label>Αγωνιστική:</label>
                                <select @change="fetchStudentsFromList" v-model="current_phase" class="form-control">
                                    <option selected v-bind:value="nullValue">Επέλεξε παιχνίδι</option>
                                    <option v-for="(phase, index) in phases" v-bind:value="index">@{{ phase }}</option>
                                </select>
                            </div>
                            <div v-else class="col-md-12">
                                <div class="alert alert-danger text-center">
                                    <p>Η κατάσταση συμμετοχής με τα συγκεκριμένα κριτήρια</p>
                                    <p><strong>ΔΕΝ υπάρχει</strong></p>
                                </div>
                            </div>
                            <div v-show="listExists" class="form-group">
                                <label>Συνοδός Εκπαιδευτικός:</label>

                                <input type="text" class="form-control" placeholder='παρακαλώ συμπληρώστε τον συνοδό...' v-model="synodos" />
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    Σχολική Μονάδα: <label v-cloak class="school-header">{{ Auth::user()->userable->name }}</label>
                </div>

            </div>
        </div>
    </div>

    <div v-cloak v-if="showParticipationStatusDiv && listExists">
        <div v-if="loadingParticipationStatusTable" class="row text-center">
            <img class="loading" src="{{ asset('images/Ripple.gif') }}"/>
            <h5>παρακαλώ περιμένετε...</h5>
        </div>

        <div v-else class="row">
            <div v-if="canChangeParticipationStatus">
                <div class="table-responsive">
                    <table class="table table-bordered" id="participation-status">
                        <thead>
                            <tr>
                                <th colspan="6">
                                    <form method="POST" action="{{ route('pdfParticipationStatus', [Auth::user()->userable->id]) }}" target="_blank">
                                        {{ csrf_field() }}
                                        <div v-for="value in checkedStudents">
                                            <input type="hidden" name="checkedStudents[]" :value="value">
                                        </div>
                                            <input type="hidden" name="sport_name" :value="current_sport.name">
                                            <input type="hidden" name="year_id" :value="current_year">
                                            <input type="hidden" name="gender" :value="current_sex">
                                            <input type="hidden" name="phase" :value="current_phase">
                                            <input type="hidden" name="individual" :value="current_sport.individual">
                                            <input type="hidden" name="synodos" :value="synodos">
                                        <button v-if="checkedStudents.length > 0 && synodos !== ''" type="submit" class="btn btn-primary">
                                            Προβολή και Εκτύπωση εγγράφου.
                                        </button>
                                    </form>
                                </th>
                            </tr>
                            <tr>
                                <th>A/A</th>
                                <th>Επιλογή</th>
                                <th>Ονοματεπώνυμο</th>
                                <th>Α.Μ.</th>
                                <th>Πατρώνυμο</th>
                                <th>Έτος Γέννησης</th>
                            </tr>
                        </thead>

                        <tbody>
                            <tr v-for="(student, index) in studentsInList" v-bind:class="{
                                'is_participate' : checkedStudents.includes(student.am)
                            }">
                                <td class="text-center">@{{ (index + 1) }}</td>
                                <td class="text-center">
                                    <div v-if="student.loader">
                                        <i class="fa fa-cog fa-spin fa-fw"></i>
                                    </div>
                                    <div v-else>
                                        <input @change="toggleStudentInStatus(student.am, checkedStudents.includes(student.am), index)" :checked="checkedStudents.includes(student.am)" type="checkbox">
                                    </div>
                                </td>
                                <td>@{{ student.last_name }} @{{ student.first_name }}</td>
                                <td class="text-center">
                                    @{{ student.am }}
                                </td>
                                <td>
                                    @{{ student.middle_name }}
                                </td>
                                <td class="text-center">
                                    @{{ student.year_birth }}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>

    <alert ref="alert"></alert>

    @include('vueloader')
@endsection

@section('scripts.footer')
    <script>
        window.base_url = "{{ url('/') }}";
        window.api_token = "{{ Auth::user()->api_token }}";
        window.current_url = "{{ url()->current() }}";
    </script>
    
    <script src="{{ mix('vendor/ofa/js/ofaSchoolParticipations.js') }}"></script>
@endsection