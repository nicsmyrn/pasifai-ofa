@extends('app')

@section('header.style')
    <link href="https://cdn.datatables.net/1.10.9/css/dataTables.bootstrap.min.css" rel="stylesheet">
@endsection

@section('content')
    <h3 class="page-heading">
        Αρχείο Δηλώσεων για ΑθλοΠαιδεία
    </h3>

        @if($lists->isEmpty())
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <hr>
                        <div class="alert alert-info text-center" role="alert">Δεν υπάρχει κανένας εγγεγραμένος καθηγητής</div>
                    </div>
                </div>
            </div>
        @else
            <div class="table-responsive">
                        <table id="lists" class="table table-bordered table-hover" cellspacing="0" width="100%">
                            <thead>
                                <tr class="active">
                                    <th class="text-center">Σχ. Έτος</th>
                                    <th class="text-center">Άθλημα</th>
                                    <th class="text-center">Ατομικό</th>
                                    <th class="text-center">Φύλο</th>
                                    <th class="text-center">Συνοδός</th>
                                    <th class="text-center">Κλειδωμένο</th>
                                    <th class="text-center">Αρχείο</th>
                                    <th class="text-center">Προβολή</th>
                                </tr>
                            </thead>

                            <tbody>
                                @foreach($lists as $list)
                                    <tr>
                                        <td class="text-center">{{ $list->year->name }}</td>

                                        <td>{{ $list->sport->name }}</td>
                                        <td class="text-center">
                                            @if($list->sport->individual)
                                                <span style="color: #00a65a">
                                                    <i class="fa fa-check-circle" aria-hidden="true"></i>
                                                </span>
                                            @endif
                                        </td>

                                        <td class="text-center">
                                            @if($list->gender == 0)
                                                Κορίτσια
                                            @elseif($list->gender == 1)
                                                Αγόρια
                                            @else
                                                Μικτό
                                            @endif
                                        <td>{{ $list->synodos }}</td>
                                        <td class="text-center">
                                            @if($list->locked)
                                                <span style="color: #ff4d4d">
                                                    <i class="fa fa-lock" aria-hidden="true"></i>
                                                </span>
                                            @endif
                                        </td>
                                        <td class="text-center">
                                            @if($list->filename != null || $list->filename != '')
                                                <a href="{{ route('downloadPdfSportEducation', [
                                                   'year_name' => $list->year->name,
                                                   'sport_name' => $list->sport->name,
                                                   'gender'     => $list->gender
                                                ]) }}">
                                                    <img height="30px" width="30px" src="{{ asset('img/download_icon.jpg') }}"/>
                                                </a>
                                            @endif
                                        </td>
                                        <td class="text-center">
                                            <a href="{{ route('OFA::showListPrimary', Crypt::encrypt($list->id)) }}">
                                                <img height="30px" width="30px" src="{{ asset('img/details-icon.png') }}"/>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach

                            </tbody>
                        </table>
            </div>

        @endif

@endsection

@section('scripts.footer')
    <script src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.9/js/dataTables.bootstrap.min.js"></script>

    <script>

        $(document).ready(function() {
            var table = $('#lists').DataTable({
                    "order": [[ 2, "asc" ]],
                    "aoColumnDefs": [ { "bSortable": false, "aTargets": [ 3, 7, 8 ] } ],
                    "language": {
                                "lengthMenu": "Προβολή _MENU_ εγγραφών ανά σελίδα",
                                "zeroRecords": "Δεν βρέθηκε καμία εγγραφή",
                                "info": "Προβολή σελίδας _PAGE_ από _PAGES_",
                                "infoEmpty": "Καμία εγγραφή διαθέσιμη",
                                "infoFiltered": "(φιλτράρισμα  από  _MAX_ συνολικές εγγραφές)",
                                "search": "Αναζήτηση:",
                                "paginate": {
                                      "previous": "Προηγούμενη",
                                      "next" : "Επόμενη"
                                    }
                            }
            });
        });
    </script>
@endsection

