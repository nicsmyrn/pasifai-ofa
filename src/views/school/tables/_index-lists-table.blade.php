        @if($lists->isEmpty())
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <hr>
                        <div class="alert alert-info text-center" role="alert">Δεν υπάρχει αρχείο δηλώσεων</div>
                    </div>
                </div>
            </div>
        @else
            <div class="table-responsive">
                        <table id="lists" class="table table-bordered table-hover" cellspacing="0" width="100%">
                            <thead>
                                <tr class="active">
                                    <th class="text-center">Σχ. Έτος</th>
                                    <th class="text-center">Είδος</th>
                                    <th class="text-center">Άθλημα</th>
                                    <th class="text-center">Ατομικό</th>
                                    <th class="text-center">Φύλο</th>
                                    <th class="text-center">Συνοδός</th>
                                    <th class="text-center">Καθ. Φ.Α.</th>
                                    <th class="text-center">Κλειδωμένο</th>
                                    <th class="text-center">Αρχείο</th>
                                    <th class="text-center">Προβολή</th>
                                </tr>
                            </thead>

                            <tbody>
                                @foreach($lists as $list)

                                    <tr>
                                        <td class="text-center">{{ $list->year->name }}</td>
                                        <td class="text-center">
                                            @if($list->sport->individual)
                                                Κατάσταση Συμμετοχής
                                            @else
                                                Λίστα Αγώνων
                                            @endif
                                        </td>
                                        <td>{{ $list->sport->name }}</td>
                                        <td class="text-center">
                                            @if($list->sport->individual)
                                                <span style="color: #00a65a">
                                                    <i class="fa fa-check-circle" aria-hidden="true"></i>
                                                </span>
                                            @endif
                                        </td>

                                        <td class="text-center">
                                            @if($list->gender == 0)
                                                Κορίτσια
                                            @elseif($list->gender == 1)
                                                Αγόρια
                                            @else
                                                Μικτό
                                            @endif
                                        <td>{{ $list->synodos }}</td>
                                        <td>{{ $list->teacher_name }}</td>
                                        <td class="text-center">
                                            @if(Auth::user()->isRole('ofa'))
                                                @if($list->locked)
                                                    <a href="#" class="btn btn-default btn-xs">
                                                        <span style="color: #ff4d4d">
                                                            <i class="fa fa-lock" aria-hidden="true"></i>
                                                        </span>
                                                    </a>
                                                @else
                                                    <a href="#" class="btn btn-default btn-xs">
                                                        <span style="color: #ff4d4d">
                                                            <i class="fa fa-unlock" aria-hidden="true"></i>
                                                        </span>
                                                    </a>
                                                @endif
                                            @else
                                                @if($list->locked)
                                                    <span style="color: #ff4d4d">
                                                        <i class="fa fa-lock" aria-hidden="true"></i>
                                                    </span>
                                                @endif
                                            @endif
                                        </td>
                                        <td class="text-center">
                                            @if($list->filename != null || $list->filename != '')
                                                @if(Auth::user()->isRole('ofa'))
                                                    <a href="{{ route('OFA::admin::downloadPdfListAdmin', [
                                                       'year_name' => $list->year->name,
                                                       'sport_name' => $list->sport->name,
                                                       'gender'     => $list->gender,
                                                       'school_name'    => str_replace(' ', '-', $name),
                                                       'school_type'    => $list->school->type

                                                    ]) }}">
                                                        <img height="30px" width="30px" src="{{ asset('img/download_icon.jpg') }}"/>
                                                    </a>
                                                @else
                                                    <a href="{{ route('OFA::downloadPdfList', [
                                                       'year_name' => $list->year->name,
                                                       'sport_name' => $list->sport->name,
                                                       'gender'     => $list->gender
                                                    ]) }}">
                                                        <img height="30px" width="30px" src="{{ asset('img/download_icon.jpg') }}"/>
                                                    </a>
                                                @endif
                                            @endif
                                        </td>
                                        <td class="text-center">
                                            @if(Auth::user()->isRole('ofa'))
                                                <a href="{{ route('OFA::admin::showListAdmin', Crypt::encrypt($list->id)) }}">
                                                    <img height="30px" width="30px" src="{{ asset('img/details-icon.png') }}"/>
                                                </a>
                                            @else
                                                <a href="{{ route('OFA::showList', Crypt::encrypt($list->id)) }}">
                                                    <img height="30px" width="30px" src="{{ asset('img/details-icon.png') }}"/>
                                                </a>
                                            @endif

                                        </td>
                                    </tr>
                                @endforeach


                                @foreach($parts as $part)
                                    <tr>
                                        <td class="text-center">{{ $part['year_name'] }}</td>
                                        <td class="text-center">
                                                Κατάσταση Συμμετοχής
                                        </td>
                                        <td>{{ $part['sport_name'] }}</td>
                                        <td class="text-center">

                                        </td>

                                        <td class="text-center">
                                            @if($part['gender'] == 0)
                                                Κορίτσια
                                            @elseif($part['gender'] == 1)
                                                Αγόρια
                                            @else
                                                Μικτό
                                            @endif
                                        <td>{{ $part['synodos'] }}</td>
                                        <td>-</td>
                                        <td class="text-center">
                                        </td>
                                        <td class="text-center">
                                            @if(Auth::user()->isRole('ofa'))
                                                <form method="POST" action="{{ route('OFA::admin::pdfParticipationStatusAdmin', [$school_id]) }}" target="_blank">
                                            @else
                                                <form method="POST" action="{{ route('pdfParticipationStatus', [Auth::user()->userable->id]) }}" target="_blank">
                                            @endif
                                                {{ csrf_field() }}
                                                    @foreach($part['checkedStudents'] as $k=>$v)
                                                        <input type="hidden" name="checkedStudents[]" value="{{ $v }}">
                                                    @endforeach
                                                    <input type="hidden" name="sport_name" value="{{ $part['sport_name'] }}">
                                                    <input type="hidden" name="year_id" value="{{ $part['year_id'] }}">
                                                    <input type="hidden" name="gender" value="{{ $part['gender'] }}">
                                                    <input type="hidden" name="phase" value="{{ $part['phase'] }}">
                                                    <input type="hidden" name="individual" value="{{ $part['individual'] }}">
                                                    <button type="submit">
                                                        <img height="30px" width="30px" src="{{ asset('img/pdf-reader-icon.png') }}"/>
                                                    </button>
                                            </form>
                                        </td>
                                        <td class="text-center">
                                            @if(Auth::user()->isRole('ofa'))
                                                <a href="{{ route('OFA::admin::showParticipationAdmin', Crypt::encrypt($part["part_id"])) }}">
                                                    <img height="30px" width="30px" src="{{ asset('img/details-icon.png') }}"/>
                                                </a>
                                            @else
                                                <a href="{{ route('OFA::showParticipation', Crypt::encrypt($part["part_id"])) }}">
                                                    <img height="30px" width="30px" src="{{ asset('img/details-icon.png') }}"/>
                                                </a>
                                            @endif

                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
            </div>

        @endif