<table id="students" class="table table-bordered table-hover" cellspacing="0" width="100%">
    <thead>
        <tr class="active">
            <th class="text-center">Αρ. Μητρώου</th>
            <th class="text-center">Επώνυμο</th>
            <th class="text-center">Όνομα</th>
            <th class="text-center">Πατρώνυμο</th>
            <th class="text-center">Μητρώνυμο</th>
            <th class="text-center">Φύλο</th>
            <th class="text-center">Τάξη</th>
            <th class="text-center">Έτος Γέννησης</th>
            <th class="text-center">Ενέργειες</th>
        </tr>
    </thead>

    <tbody>
        <tr v-for="student in students">
            <td class="text-center">
                <a :href="student.edit_url">
                    @{{ student.am }}
                </a>
            </td>

            <td class="text-center">
                <a :href="student.edit_url">
                    @{{ student.last_name }}
                </a>
            </td>

            <td class="text-center">
                @{{ student.first_name }}
            </td>

            <td class="text-center">
                @{{ student.middle_name }}
            </td>

            <td class="text-center">
                @{{ student.mothers_name }}
            </td>

            <td class="text-center">
                <span v-if="student.sex">
                    ΑΡΡΕΝ
                </span>

                <span v-else>
                    ΘΗΛΥ
                </span>
            </td>

            <td class="text-center">
                <span v-if="student.class == 1">
                    Α
                </span>
                <span v-if="student.class == 2">
                    Β
                </span>
                <span v-if="student.class == 3">
                    Γ
                </span>
            </td>

            <td class="text-center">
                @{{ student.year_birth }}
            </td>

            <td class="text-center">
                <button @click="destroy(student.am)" type="button" title="Διαγραφή" class="btn btn-danger btn-xs"><i class="glyphicon glyphicon-trash"></i></button>
            </td>
        </tr>
    </tbody>
</table>

