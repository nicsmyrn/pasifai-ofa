@extends('app')

@section('header.style')
    <meta id="token" name="csrf-token" content="{{csrf_token()}}">

    <link href="https://cdn.datatables.net/1.10.9/css/dataTables.bootstrap.min.css" rel="stylesheet">
    @include('user._style')

        <style>
            #periodRoutes{

            }
            #periodRoutes thead tr th{
                text-align: center;
            }

            .suggestion-fake{
                color: red;
                font-weight: bold;
            }
            .button-change{
                color : red;
            }

            .pointers {
                cursor: pointer;
            }

            .delete-button{
                padding-top: 15px !important;
                color: red;
                font-weight: bold;
            }

            .loading{
                width: 80px;
                height: 80px;
            }

            .school-header{
                color: #006666;
                font-size: 15pt;
                font-weight: 700;
                font-family: 'Merriweather', 'Helvetica Neue', Arial, sans-serif;
            }

        </style>

@endsection

@section('content')

    @include('ofa::school.csv-file-attachment', ['type' => Auth::user()->userable->type])

    <h3 class="page-heading">
        Καταστάσεις Μαθητών
    </h3>

    <div class="row">
        <div class="col-md-12">
                <div v-cloak v-if="loading" class="panel-body text-center">
                    <img class="loading" src="{!! asset('images/Ripple.gif') !!}"/>
                    <h5>παρακαλώ περιμένετε...</h5>
                </div>

                <div v-cloak v-else>
                    <div v-if="students.length">
                        @include('ofa::school.tables._index-students-table')
                    </div>
                    <div v-else>
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6 col-md-offset-3">
                                    <hr>
                                    <div class="alert alert-info text-center" role="alert">Δεν υπάρχει κανένας Μαθητής</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>


@endsection

@section('scripts.footer')
    <script src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.9/js/dataTables.bootstrap.min.js"></script>

    <script>

        $(document).ready(function() {

            var table = $('#students').DataTable({
                    "order": [[ 1, "asc" ]],
                    "aoColumnDefs": [ { "bSortable": false, "aTargets": [ 8 ] } ],
                    "pageLength": 100,
                    "language": {
                                "lengthMenu": "Προβολή _MENU_ εγγραφών ανά σελίδα",
                                "zeroRecords": "Δεν βρέθηκε καμία εγγραφή",
                                "info": "Προβολή σελίδας _PAGE_ από _PAGES_",
                                "infoEmpty": "Καμία εγγραφή διαθέσιμη",
                                "infoFiltered": "(φιλτράρισμα  από  _MAX_ συνολικές εγγραφές)",
                                "search": "Αναζήτηση:",
                                "paginate": {
                                      "previous": "Προηγούμενη",
                                      "next" : "Επόμενη"
                                    }
                            }
            });

            $('#checkCsvAttachment').on('change', function(){
                if ($(this).is(":checked")){
                    $('#csv_myschool_attachments').show(500);
                }else{
                    $('#csv_myschool_attachments').hide(500);
                }
            });

//            $('.deleteStudent').on('click',{
//                am : $(this).data("name")
//            }, swalAlertDelete);
            $('.deleteStudent').on('click', function(e){
                e.preventDefault();
                var am = $(this).data("name");
                swalAlertDelete(am);
            })
        });


        function swalAlertDelete(am){

            swal({
                title: "Προσοχή!",
                text: "Είστε βέβαιος ότι θέλετε να διαγράψετε τον μαθητή με Αριθμό Μητρώου : " + am + " ;",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "ΝΑΙ",
                cancelButtonText: "Άκυρο",
                closeOnConfirm: false,
                showLoaderOnConfirm: true,
                closeOnCancel: true },
                function(){
                    $.ajax({
                        type : 'post',
                        data : {
                            am : am,
                            _token : $('#token').attr('content')
                        },
                        url : "{{ (Auth::user()->userable->type == 'Λύκειο' || Auth::user()->userable->type == 'ΕΠΑΛ') ?  route('OFA::deleteStudent') : route('OFA::primary.deleteStudent')}}",
                        success : function(data){
                            location.reload(true);
//                            console.log(data);
                        },
                        error: function (request, status, error) {
                           console.log('Error 87654678');
                       }
                    });
                }
            );
        }

        function bs_input_file() {
            $(".input-file").before(
                function() {
                    if ( ! $(this).prev().hasClass('input-ghost') ) {
                        var element = $("<input type='file' class='input-ghost' style='visibility:hidden; height:0'>");
                        element.attr("name",$(this).attr("name"));
                        element.change(function(){
                            element.next(element).find('input').val((element.val()).split('\\').pop());
                        });
                        $(this).find("button.btn-choose").click(function(){
                            element.click();
                        });
                        $(this).find("button.btn-reset").click(function(){
                            element.val(null);
                            $(this).parents(".input-file").find('input').val('');
                        });
                        $(this).find('input').css("cursor","pointer");
                        $(this).find('input').mousedown(function() {
                            $(this).parents('.input-file').prev().click();
                            return false;
                        });
                        return element;
                    }
                }
            );
        }
        $(function() {
            bs_input_file();
        });
    </script>

    <script>
        window.base_url = "{{ url('/') }}";
        window.api_token = "{{ Auth::user()->api_token }}";
    </script>
    <script src="{{ mix('vendor/ofa/js/indexStudents.js') }}"></script>

@endsection

