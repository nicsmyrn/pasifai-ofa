@extends('app')

@section('title')
    Δήλωση Αθλημάτων
@endsection

@section('loader')
    @include('loader')
@endsection

@section('header.style')
    @include('user._style')

    <style>
        .statement-span{
            color: #0000Ff;
        }

        .statement-span:hover{
            font-weight: bold;
            text-decoration: underline;
            color: #0000ff;
        }

        .statement-checked{
            font-weight: bold;
            color: #008000;
        }

        .statement-not-checked{
            font-weight: normal;
            color: red;
        }

        @-webkit-keyframes blinker {
          from {opacity: 1.0;}
          to {opacity: 0.0;}
        }
        .blink{
          color : red;
            text-decoration: blink;
            -webkit-animation-name: blinker;
            -webkit-animation-duration: 0.6s;
            -webkit-animation-iteration-count:infinite;
            -webkit-animation-timing-function:ease-in-out;
            -webkit-animation-direction: alternate;
        }
    </style>
@endsection

@section('content')

    <div class="row">
        <div class="col-md-12">
            <h5 class="page-heading">
                Δήλωση Συμμετοχής σε Αθλήματα
            </h5>
        </div>
    </div>

    <div class="row">
        @if(Auth::user()->userable->type == 'Γυμνάσιο')
            {{ Form::open(['method' => 'post', 'route' => 'OFA::postSchoolStatementPrimary']) }}
        @else
            {{ Form::open(['method' => 'post', 'route' => 'OFA::postSchoolStatement']) }}
        @endif
        <div class="col-md-1"></div>
        <div class="col-md-5">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Επιλέξτε τα αθλήματα που θα συμμετέχετε για τη φετινή χρονιά</h3>
                    </div>
                    <div class="panel-body">

                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th class="text-center">Άθλημα</th>
                                    <th class="text-center">Αγόρια</th>
                                    <th class="text-center">Κορίτσια</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($sports as $key=>$sport)
                                    <tr>
                                        <td>
                                            {!! $sport !!}
                                        </td>
                                        <td class="text-center">
                                            <div class="checkbox sport-div">
                                                <label style="font-size: 1.1em">
                                                    <input
                                                        @if($statements->where('school_id', $school_id)->where('sport_id', $key)->where('gender', 'male')->first() != null)
                                                            checked
                                                        @endif
                                                        id="sportId{!! $key !!}male" value="{!! $sport !!}" class="statement-input" name="sport[{!! $key !!}][male]" type="checkbox">
                                                    <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                                                        @if($statements->where('school_id', $school_id)->where('sport_id', $key)->where('gender', 'male')->first() != null)
                                                            <span class="statement-span statement-checked">NAI</span>
                                                        @else
                                                            <span class="statement-span statement-not-checked">OXI</span>
                                                        @endif
                                                </label>
                                            </div>
                                        </td>
                                        <td class="text-center">
                                            <div class="checkbox sport-div">
                                                <label style="font-size: 1.1em">
                                                    <input
                                                        @if($statements->where('school_id', $school_id)->where('sport_id', $key)->where('gender', 'female')->first() != null)
                                                            checked
                                                        @endif
                                                        id="sportId{!! $key !!}female" value="{!! $sport !!}" class="statement-input" name="sport[{!! $key !!}][female]" type="checkbox">
                                                    <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                                                        @if($statements->where('school_id', $school_id)->where('sport_id', $key)->where('gender', 'female')->first() != null)
                                                            <span class="statement-span statement-checked">NAI</span>
                                                        @else
                                                            <span class="statement-span statement-not-checked">OXI</span>
                                                        @endif
                                                </label>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach





                            </tbody>
                        </table>

                    </div>
                </div>

        </div>
        <div class="col-md-4">
            <div class="alert alert-warning" role="alert">
                <h4 class="text-center blink"><span style="color: red;text-decoration: underline; font-weight: bold">ΟΔΗΓΙΕΣ</span></h4>
                <ol>
                    <li>Για συμμετοχή σε άθλημα επιλέξτε το αντίστοιχο κουτάκι και θα μετατραπεί το <span style="color: red; font-weight: bold">ΟΧΙ</span> σε <span style="color: #008000; font-weight: bold">ΝΑΙ</span></li>
                    <li>Για τα αθλήματα στα οποία δε θα συμμετάσχετε, αφήστε το κουτάκι ως έχει (κενό)</li>
                    <li>Για να σταλούν τα στοιχεία στην Ομάδα Φυσικής Αγωγής πρέπει να πατήσετε Αποστολή</li>
                    <li>Με την Αποστολή θα σας σταλεί αποδεικτικό E-mail με τις προτιμήσεις σας</li>
                </ol>
            </div>
            <div class="form-group">
                Τελευταία Ενημέρωση: <strong>{!! $statements->first() != null ?  \Carbon\Carbon::parse($statements->first()->updated_at)->format('d/m/Y h:i:s') : 'ΚΑΜΙΑ' !!}</strong>
            </div>
            <div class="form-group">
                {!! Form::submit('Αποστολή', ['class' => 'btn btn-primary btn-lg button-submit']) !!}
            </div>
            <div class="form-group" style="text-align: center; font-weight: bold; color: red">
                <span class="fa fa-exclamation-circle"></span>
                <span>ΛΗΞΗ ΔΗΛΩΣΕΩΝ σε: <span id="clock_statement"></span> </span>
            </div>
        </div>
        <div class="col-md-2"></div>
        {!! Form::close() !!}
    </div>

@endsection


@section('scripts.footer')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.countdown/2.2.0/jquery.countdown.min.js" integrity="sha256-Ikk5myJowmDQaYVCUD0Wr+vIDkN8hGI58SGWdE671A8=" crossorigin="anonymous"></script>
    <script>
        $(document).ready(function() {

            $('input[type=checkbox]').on('change',function(e) {
                if ($(this).prop('checked')) {
                    $(this).parent().find('.statement-span').text('ΝΑΙ').removeClass('statement-not-checked');
                } else {
                    $(this).parent().find('.statement-span').text('ΟΧΙ').removeClass('statement-checked');
                }
            });

            $('.button-submit').on('click', function(){
                $('#loader').removeClass('invisible')
                    .addClass('loading');
            });

            $('#clock_statement').countdown("{{ config()->get('requests.OFA_STATEMENTS_END')}}", function(event) {
                $(this).html(event.strftime('<strong>%-D</strong> ημέρες <strong>%-H</strong> ώρες <strong>%-M</strong> λεπτά <strong>%-S</strong> δευτερόλεπτα'));
            })
            .on('finish.countdown', function(){
                location.reload();
            });

        });
    </script>
@endsection
