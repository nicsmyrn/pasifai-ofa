    <table id="header">
        <tr>
            <td class="header-left"></td>
            <td class="head-1">
                ΠΑΝΕΛΛΗΝΙΟΙ ΑΓΩΝΕΣ {{ $school_type == 'Γυμνάσιο' ? 'ΓΥΜΝΑΣΙΩΝ' : 'ΛΥΚΕΙΩΝ' }}
            </td>
            <td class="header-right">
                <span id="header-right-content">
                    {{ $individual ? '2' : '3' }}
                </span>
            </td>
        </tr>

        <tr>
            <td class="head-3">
                <div>Τίτλος Σχολείου:</div>
                <div><span class="bold-title">{{ $school_name }}</span></div>
            </td>
            <td>

            </td>
            <td class="head-3">
                <span class="bold-title">
                    {{ $school_type == 'Γυμνάσιο' ? 'Γυμνασίου' : 'Λυκείου' }} - {{ $individual ? 'Ατομικά' : 'Ομαδικά' }}
                </span>
            </td>
        </tr>

        <tr>
            <td colspan="3" class="head-2">
                ΚΑΤΑΣΤΑΣΗ ΣΥΜΜΕΤΟΧΗΣ  <b>{{ $gender == 0 ? 'ΜΑΘΗΤΡΙΩΝ': 'ΜΑΘΗΤΩΝ' }}</b> ΣΤΟ ΑΘΛΗΜΑ <b>&laquo;{{ $sport_name }}&raquo; </b> ΣΧ. ΕΤΟΥΣ  <b>{{ $year_name }}</b>
            </td>
        </tr>
        <tr>
            <td colspan="3" class="head-2">
                @if($phase == 0)
                    1oς Αγώνας
                @elseif($phase == 1)
                    2oς Αγώνας
                @elseif($phase == 2)
                    3oς Αγώνας
                @else
                    1oς Αγώνας
                @endif
            </td>
        </tr>
    </table>

    <table id="content">
        <tr class="content-header">
            <td width="3%">A/A</td>
            <td width="30%">Επώνυμο</td>
            <td width="20%">Όνομα</td>
            <td width="10%">Όνομα Πατέρα</td>
            <td width="10%">Όνομα Μητέρας</td>
            <td width="10%">Έτος Γεννησ.</td>
            <td width="7%">Αρ. Μητρ.</td>
            <td width="4%">Τάξη</td>
            <td width="6%">Υπεύθυνη Δήλωση Γονέα</td>
        </tr>

        @foreach($students as $index=>$student)
            <tr class="content-body">
                <td>{{ ($index + 1) }}</td>
                <td class="left-content">{{ $student->last_name }}</td>
                <td class="left-content">{{ $student->first_name }}</td>
                <td class="left-content">{{ $student->middle_name }}</td>
                <td class="left-content">{{ $student->mothers_name }}</td>
                <td>{{ $student->year_birth }}</td>
                <td>{{ $student->am }}</td>
                <td>{{ Config::get('requests.class')[$student->class]  }} </td>
                <td>ΝΑΙ</td>
            </tr>
        @endforeach

    </table>

    <table id="footer">
        <tr>
            <td width="30%">
                <div class="right_footer_date"></div>

                <div class="header-signature">
                    Συνοδός καθηγητής ορίζεται
                </div>
                <div class="footer-signature">
                    {{ $synodos }}
                </div>
                <div class="print_date">
                    Ημερομηνία εκτύπωσης: {{ \Carbon\Carbon::now()->format('d-m-Y H:i:s') }}
                </div>
            </td>
            <td width="40%"></td>
            <td width="30%">
                <div class="right_footer_date">
                    {{Config::get('requests.CITY_OFA')}} {{ \Carbon\Carbon::now()->format('d/m/Y') }}
                </div>
                <div class="header-signature">
                    @if($sex == 0)
                        Η Διευθύντρια
                    @else
                        Ο Διευθυντής
                    @endif
                </div>
                <div class="footer-signature">
                    {{ $full_name }}
                </div>
            </td>
        </tr>
    </table>

