<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <style>
        body{
            font-family: DejaVu Sans, sans-serif;
            /*font-family: "DejaVu Serif";*/
            /* margin: 0 20px 0 20px; */
            font-size: 8pt;
            text-align: justify;
        }

        table{
            width: 100%;
            /* margin: 8px; */
            border-collapse: collapse;
        }

        #header{
            margin-bottom: 5px;
        }

        table .head-1{
            text-align: center;
            font-size: 8pt;
            font-weight: bold;
        }

        table .head-2{
            text-align: center;
            font-size: 11pt;
        }

        table .head-3{
            padding-left: 25px;
            text-align: left;
            font-size: 12pt;
        }

        table .head-information{
            font-size: 10pt;
            text-align: left;
        }
        table .head-description{
            padding-top: 30px;
            font-size: 8pt;
            text-align: center;
        }

        #content{
            border: #000000 solid 2px;
        }

        #content .content-header{
            text-align: center;
            font-size: 9pt;
            font-weight: bold;
            height: 50px;
            word-break: break-all;
        }
        #content .content-header td{
            border: #000000 solid 1px;
        }

        #content .content-body{
            text-align: center;
            font-size: 10pt;
        }

        #content .content-body td{
            border: #000000 solid 1px;
            text-align: center;
        }

        .left-content{
            text-align: left !important;
            padding-left: 10px;
        }

        #footer{
            margin-top: 10px;
            width: 100%;
            font-size: 10pt;
            position: absolute;
            /* bottom: 118px; */
        }

        .right_footer_date{
            text-align: left !important;
        }

        .header-signature{
             text-align: center;
             font-weight: bold;
        }

        .footer-signature{
            text-align: center;
            margin-top: 70px;
             font-weight: bold;
        }

        .bold-title{
            font-weight: bold;
        }

        .header-right{
            text-align: right;
        }

        .header-right #header-right-content{
            border: #000000 solid 1px;
            font-size: 14pt;
            padding: 20px;
            padding-bottom: 30px;
            font-weight: bold;
        }

    </style>
</head>