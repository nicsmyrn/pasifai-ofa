    <table id="header">
        <tr>
            <td class="header-left"></td>
            <td class="head-1">
            </td>
            <td class="header-right">
                <span id="header-right-content">
                </span>
            </td>
        </tr>

        <tr>
            <td class="head-3" colspan="3">
                Τίτλος Σχολείου: <span class="bold-title">{{ $attr['school']['name'] }}</span>
            </td>
        </tr>

        <tr>
            <td colspan="3" class="head-2">
                <b>{{ $attr['gender'] == 0 ? 'ΚΟΡΙΤΣΙΑ': ($attr['gender'] == 1 ? 'ΑΓΟΡΙΑ' :'ΜΙΚΤΗ') }}</b> ΣΤΟ ΑΘΛΗΜΑ  <strong>&laquo;{{ $attr['sport']['name'] }}&raquo;</strong> Σχ. Έτους <strong>{{ $attr['year']['name'] }}</strong>
            </td>
        </tr>
    </table>

    <table id="content">
        <tr class="content-header">
            <td width="3%">α/α</td>
            <td width="22%">Επώνυμο</td>
            <td width="20%">Όνομα</td>
            <td width="12%">Όνομα Πατέρα</td>
            <td width="12%">Όνομα Μητέρας</td>
            <td width="10%">Έτος Γεννησ.</td>
            <td width="7%">Αρ. Μητρ.</td>
            <td width="4%">Τάξη</td>
            <td width="4%">ΑΓΩΝΙΣΜΑ</td>
            <td width="6%"></td>
        </tr>


        @foreach($attr['studentsList'] as $key=>$student)

                <tr class="content-body">
                    <td>{{ ($key) }}</td>
                    <td class="left-content">{{ $student['last_name'] }}</td>
                    <td class="left-content">{{ $student['first_name'] }}</td>
                    <td class="left-content">{{ $student['middle_name'] }}</td>
                    <td class="left-content">{{ $student['mothers_name'] }}</td>
                    <td>{{ $student['year_birth'] }}</td>
                    <td>{{ $student['am'] }}</td>
                    <td>{{ Config::get('requests.class')[$student['class']] }}</td>
                    <td>
                        {{ $student['sport_name_special'] }}
                    </td>
                    <td></td>
                </tr>
        @endforeach

    </table>



