@extends('app')

@section('header.style')
<meta id="token" name="csrf-token" content="{{csrf_token()}}" xmlns="http://www.w3.org/1999/html">

    <style>
        #periodRoutes{

        }
        #periodRoutes thead tr th{
            text-align: center;
        }

        .suggestion-fake{
            color: red;
            font-weight: bold;
        }
        .button-change{
            color : red;
        }

        .pointers {
            cursor: pointer;
        }

        .delete-button{
            padding-top: 15px !important;
            color: red;
            font-weight: bold;
        }

        .loading{
            width: 80px;
            height: 80px;
        }

        .school-header{
            color: #006666;
            font-size: 15pt;
            font-weight: 700;
            font-family: 'Merriweather', 'Helvetica Neue', Arial, sans-serif;
        }

    </style>
@endsection

@section('scripts.footer')
    <script>
        window.base_url = "{{ url('/') }}";
        window.api_token = "{{ Auth::user()->api_token }}";
        window.current_url = "{{ url()->current() }}";
    </script>
    
    <script src="{{ mix('vendor/ofa/js/ofaSchoolGymnasiou.js') }}"></script>

@endsection

@section('title')
    Δημιουργία Αθλοπαιδείας
@endsection

@section('content')

    <div class="row">
        <h3 class="page-heading">Κατάσταση συμμετοχής σε αγώνες ΑθλοΠαιδείας</h3>

        <div class="panel panel-primary">

            <div v-if="loading" class="panel-body text-center">
                <img class="loading" src="{{ asset('images/Ripple.gif') }}"/>
                <h5>παρακαλώ περιμένετε...</h5>
            </div>

            <div v-cloak v-else class="panel-body">
                <div class="col-md-4">
                    <div class="form-group">
                        <span>Σχολικό Έτος: </span> <label v-cloak class="school-header"> @{{ dataSet.year.name }} </label>
                    </div>
                    <div class="form-group">
                        <label>Καθηγητής Φυσικής Αγωγής</label>
                        <input class="form-control" type="text" v-model="teacher_name" placeholder="γράψτε τον υπεύθυνο Φυσικής Αγωγής του Σχολείου"/>
                    </div>
                </div>

                <div class="col-md-4 text-center">
                    <div class="form-group form-inline">
                        <label>Άθλημα:</label>
                        <select class="form-control" v-cloak @change="displayGender(current_sport)" v-model="current_sport">
                            <option selected v-bind:value="nullValue">Επέλεξε Άθλημα</option>
                            <option v-for="sport in dataSet.sports" v-bind:value="sport">
                                @{{ sport.name }}
                            </option>
                        </select>
                    </div>
                    <div class="form-group form-inline" v-if="current_sport != null">
                        <div v-if="current_sport.name != 'ΣΤΙΒΟΣ'">
                            <label>Φύλο:</label>
                            <select class="form-control" v-cloak @change="fetchAllStudentsByGender()" v-model="current_sex">
                                <option disabled style="background-color: #00a65a">Επέλεξε φύλο</option>
                                <option v-for="(gender, index) in sex" v-bind:value="index">
                                    @{{ gender }}
                                </option>
                            </select>
                        </div>
                    </div>

                </div>

                <div class="col-md-4">
                    <div class="form-group form-inline">
                        Σχολική Μονάδα: <label v-cloak class="school-header">{{ Auth::user()->userable->name }}</label>
                    </div>
                    <div class="form-group">
                        <label>Συνοδός Εκπαιδευτικός</label>
                        <input class="form-control" type="text" v-model="synodos" placeholder="γράψτε τον συνοδό εκπαιδευτικό του Σχολείου"/>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div v-cloak v-if="showListForm || showIndividualForm">
        
        <div v-if="loadingForExistence" class="row text-center">
            <img class="loading" src="{{ asset('images/Ripple.gif') }}"/>
            <h5>παρακαλώ περιμένετε...</h5>
        </div>

        <div v-else class="row">
            <div v-if="notExistsPdfFile">

                <div v-if="current_sport.individual">
                    <create-or-select-student
                        :students-list.sync="studentsList"
                        :all-students-by-gender.sync="allStudentsIndividual"
                        :current-sport="current_sport"
                        :current-sex.sync="current_sex"
                        :token="token"
                        v-on:display-message="displayMessage"
                        :individual="true"
                        :individual-sports="individualSports"
                    >
                    </create-or-select-student>

                    <list-students-table
                        :students-list.sync="studentsList"
                        :current-sport="current_sport"
                        :individual="true"
                        :teacher-name="synodos"
                        v-on:delete-student="deleteStudentIndividual"
                        v-on:temporary-save="temporarySave"
                        v-on:open-modal="openModal"
                    >
                    </list-students-table>
                </div>

                <div v-if="!current_sport.individual">
                    <create-or-select-student
                        :students-list.sync="studentsList"
                        :all-students-by-gender.sync="allStudentsByGender"
                        :current-sport="current_sport"
                        :current-sex.sync="current_sex"
                        :token="token"
                        v-on:display-message="displayMessage"
                    >
                    </create-or-select-student>

                    <list-students-table
                        :students-list.sync="studentsList"
                        :current-sport="current_sport"
                        :individual="false"
                        v-on:delete-student="deleteStudent"
                        v-on:temporary-save="temporarySave"
                        v-on:open-modal="openModal"
                    >
                    </list-students-table>
                </div>

                <modal-agreement 
                    :show="showModalAgreement" 
                    :loader="hideLoaderAgreement"
                    :token="token"
                    :individual="current_sport.individual == 1"
                    school-type="gym"
                    v-on:close-aggreement-modal="hideModalAgreement"
                    v-on:sent-request="sentRequest"
                ></modal-agreement>
            </div>

            <div v-else>
                <div class="alert alert-danger col-md-4 col-md-offset-4 text-center">
                    <h4>Κατέβασμα Κατάστασης Συμμετοχής στον υπολογιστή</h4>
                    <a @click="downloadList" :href="downloadUrl" class="btn btn-success btn-lg">Αποθήκευση</a>
                </div>
            </div>
        </div>
    </div>

    <alert ref="alert"></alert>
    
@endsection

@section('loader')
    @include('vueloader')
@endsection