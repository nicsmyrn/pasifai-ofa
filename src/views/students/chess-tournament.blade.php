@extends('app')

@section('header.style')
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/@mdi/font@4.x/css/materialdesignicons.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/vuetify@2.x/dist/vuetify.min.css" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui">
@endsection 

@section('scripts.footer')
    <script>
        window.api_token = "{{ Auth::user()->api_token }}";
    </script>
    <script src="{{ mix('vendor/ofa/js/ofaChess.js') }}"></script>

@endsection

@section('title')
    ΠΑΓΚΡΗΤΙΟΙ ΣΧΟΛΙΚΟΙ ΑΓΩΝΕΣ ΣΚΑΚΙ ΜΕΣΩ ΔΙΑΔΙΚΤΥΟΥ
@endsection

@section('content')
    <chess-request
        v-if="hideLoader && notRegistered"
        :profile="profile"
        :token="token"
    ></chess-request>

    <chess-registered 
        v-if="hideLoader && !notRegistered" 
        :token="token"
    />
@endsection

@section('loader')
    @include('vueloader')
@endsection