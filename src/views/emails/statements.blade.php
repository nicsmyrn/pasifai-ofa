@component('mail::message')

    Η Σχολική Μονάδα {{ $school }} σας έστειλε Δήλωση Συμμετοχής σε Αθλήματα


@component('mail::panel')
    @if($records != null)
        <table  cellpadding="5" style="border-collapse: collapse; border: 1px solid #000000">
            <thead>
                <tr>
                    <th style="border: 1px solid #000000">Άθλημα</th>
                    <th style="border: 1px solid #000000">Αγόρια</th>
                    <th style="border: 1px solid #000000">Κορίτσια</th>
                </tr>
            </thead>
            <tbody>
                @foreach($sports as $sport)
                    <tr>
                        <td style="border: 1px solid #000000">{!! $sport->name !!}</td>
                        @foreach($gender as $k=>$v)
                            <td style="text-align:center; border: 1px solid #000000">
                                @if(array_key_exists($sport->id, $records))
                                    @if(array_key_exists($v, $records[$sport->id]))
                                        NAI
                                    @else
                                        OXI
                                    @endif
                                @else
                                    OXI
                                @endif
                            </td>
                        @endforeach
                    </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="3">
                        Ημερομηνία καταχώρησης: {!! \Carbon\Carbon::now()->format('d/m/Y h:i:s') !!}
                    </td>
                </tr>
            </tfoot>
        </table>
    @else
        ΤΟ ΣΧΟΛΕΙΟ ΔΕ ΘΑ ΣΥΜΜΕΤΑΣΧΕΙ ΣΕ ΚΑΝΕΝΑΝ ΑΓΩΝΑ
    @endif
@endcomponent


@component('mail::signature', ['email' => 'nicsmyrn@sch.gr'])
| ------ Αυτοματοποιημένο μήνυμα ------             |
| ------------------------------------------------- |
| Νίκος Σμυρναίος - Υπεύθυνος της εφαρμογής ΠΑΣΙΦΑΗ |
@endcomponent

@endcomponent

