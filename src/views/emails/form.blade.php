@component('mail::message')

Από: {{ $fullname }}
@if($phone != '')
Τηλέφωνο: {{ $phone }}
@endif

@component('mail::panel')
<strong>Θέμα:</strong> {{ $subject }}
@endcomponent

@component('mail::promotion')
    <p style="text-align:left">
        {{ $message }}
    </p>
@endcomponent

@component('mail::button', ['url' => ('mailto:'.$email), 'color' => 'green'])
Απάντηση στον εκπαιδευτικό
@endcomponent


@component('mail::subcopy')
    Το μήνυμα αυτό είναι αυτοματοποιημένο. Μην απαντήσετε στο E-mail του μηνύματος.
@endcomponent

@endcomponent