@component('mail::message')

Η Σχολική Μονάδα  <strong> {{ $school_name }} </strong>   σας έστειλε Λίστα Σχολικής Ομάδας

στο αγώνισμα <strong> {{ $sport_name }} - {{ $gender }}</strong>

το μήνυμα περιλαμβάνει συννημμένη τη ΛΙΣΤΑ Αγώνων

@component('mail::signature', ['email' => Config::get('requests.MAIL_OFA')])
| {{Config::get('requests.arxika')}}          |
| -------------------------------- |
| {{Config::get('requests.address')}} |
@endcomponent

@endcomponent