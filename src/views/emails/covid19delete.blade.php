@component('mail::message')

@component('mail::promotion')
    <p style="text-align:left">
        Η ΑΙΤΗΣΗ για εμβολιασμό ΔΙΕΓΡΑΦΗ από τον εκπαιδευτικό
    </p>
@endcomponent

@component('mail::panel')
    <table  cellpadding="5" style="border-collapse: collapse; border: 1px solid #000000">
            <tbody>
                <tr>
                    <td style="border: 1px solid #000000">Ονοματεπώνυμο</td>
                    <td style="text-align:center; border: 1px solid #000000">{{ $fullname }}</td>
                </tr>
                <tr>
                    <td style="border: 1px solid #000000">ΑΜΚΑ</td>
                    <td style="text-align:center; border: 1px solid #000000">{{ $amka }}</td>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="3">
                        Ημερομηνία ΔΙΑΓΡΑΦΗΣ: {!! \Carbon\Carbon::now()->format('d/m/Y h:i:s') !!}
                    </td>
                </tr>
            </tfoot>
        </table>
@endcomponent

@component('mail::button', ['url' => ('mailto:'.$email), 'color' => 'green'])
Απάντηση στον εκπαιδευτικό
@endcomponent


@component('mail::subcopy')
    Το μήνυμα αυτό είναι αυτοματοποιημένο. Μην απαντήσετε στο E-mail του μηνύματος.
@endcomponent

@endcomponent