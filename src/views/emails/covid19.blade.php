@component('mail::message')

@component('mail::panel')
    <table  cellpadding="5" style="border-collapse: collapse; border: 1px solid #000000">
            <tbody>
                <tr>
                    <td style="border: 1px solid #000000">Ονοματεπώνυμο</td>
                    <td style="text-align:center; border: 1px solid #000000">{{ $fullname }}</td>
                </tr>
                <tr>
                    <td style="border: 1px solid #000000">ΑΜΚΑ</td>
                    <td style="text-align:center; border: 1px solid #000000">{{ $amka }}</td>
                </tr>
                <tr>
                    <td style="border: 1px solid #000000">Ημ. Γέννησης</td>
                    <td style="text-align:center; border: 1px solid #000000">{{ \Carbon\Carbon::parse($birth)->format('d/m/Y') }}</td>
                </tr>
                <tr>
                    <td style="border: 1px solid #000000">Τηλέφωνο</td>
                    <td style="text-align:center; border: 1px solid #000000">{{ $phone }}</td>
                </tr>
                <tr>
                    <td style="border: 1px solid #000000">Κινητό</td>
                    <td style="text-align:center; border: 1px solid #000000">{{ $mobile }}</td>
                </tr>
                <tr>
                    <td style="border: 1px solid #000000">Σχολική Μονάδα</td>
                    <td style="text-align:center; border: 1px solid #000000">{{ $school }}</td>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="3">
                        Ημερομηνία καταχώρησης: {!! \Carbon\Carbon::now()->format('d/m/Y h:i:s') !!}
                    </td>
                </tr>
            </tfoot>
        </table>
@endcomponent

@component('mail::button', ['url' => ('mailto:'.$email), 'color' => 'green'])
Απάντηση στον εκπαιδευτικό
@endcomponent


@component('mail::subcopy')
    Το μήνυμα αυτό είναι αυτοματοποιημένο. Μην απαντήσετε στο E-mail του μηνύματος.
@endcomponent

@endcomponent