        @if($lists->isEmpty())
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <hr>
                        <div class="alert alert-info text-center" role="alert">Δεν υπάρχουν Λίστες</div>
                    </div>
                </div>
            </div>
        @else
            <div class="table-responsive">
                <table id="index_lists" class="table table-bordered table-hover" cellspacing="0" width="100%">
                    <thead>
                        <tr class="active">
                            <th class="text-center">Σχολική Μονάδα</th>
                            <th class="text-center">Σχ. Έτος</th>
                            <th class="text-center">Άθλημα</th>
                            <th class="text-center">Φύλο</th>
                            <th class="text-center">Κλειδωμένο</th>
                            <th class="text-center">Αρχείο</th>
                            <th class="text-center">Ημνια Δημιουργίας</th>
                            <th class="text-center">Προβολή</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($lists as $list)
                            <tr>
                                <td>
                                    {{ $list->school->name }}
                                </td>
                                <td class="text-center">{{ $list->year->name }}</td>
                                <td>{{ $list->sport->name }}</td>

                                <td class="text-center">
                                    @if($list->gender == 0)
                                        Κορίτσια
                                    @elseif($list->gender == 1)
                                        Αγόρια
                                    @else
                                        Μικτό
                                    @endif
                                <td class="text-center">
                                    @if($list->locked)
                                        <a href="{{ route('OFA::admin::toggleLock', $list->id) }}" class="btn btn-default btn-xs">
                                            <span style="color: #ff4d4d">
                                                <i class="fa fa-lock" aria-hidden="true"></i>
                                            </span>
                                        </a>
                                    @else
                                        <a href="{{ route('OFA::admin::toggleLock', $list->id) }}" class="btn btn-default btn-xs">
                                            <span style="color: #008000">
                                                <i class="fa fa-unlock" aria-hidden="true"></i>
                                            </span>
                                        </a>
                                    @endif

                                </td>
                                <td class="text-center">
                                    @if($list->filename != null || $list->filename != '')
                                        <a href="{{ route('OFA::admin::downloadPdfListAdmin', [
                                           'year_name' => $list->year->name,
                                           'sport_name' => $list->sport->name,
                                           'gender'     => $list->gender,
                                           'school_name'    => str_replace(' ', '-', $list->school->name),
                                           'school_type'    => $list->school->type
                                        ]) }}">
                                            <img height="30px" width="30px" src="{{ asset('img/download_icon.jpg') }}"/>
                                        </a>
                                    @endif
                                </td>
                                <td class="text-center">
                                    {{ $list->updated_at }}
                                </td>
                                <td class="text-center">
                                    <a href="{{ route('OFA::admin::showListAdmin', Crypt::encrypt($list->id)) }}">
                                        <img height="30px" width="30px" src="{{ asset('img/details-icon.png') }}"/>
                                    </a>
                                </td>
                            </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>

        @endif