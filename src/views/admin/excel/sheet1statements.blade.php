<table>
    <thead>
        <tr>
            <th>Άθλημα</th>
            <th>Αγόρια</th>
            <th>Κορίτσια</th>
        </tr>
    </thead>
    <tbody>
        @foreach($sports as $sport)
            <tr>
                <td>
                    {!! $sport->name !!}
                </td>

                @foreach($gender as $k=>$v)
                    <td>
                        @if($statements->where('sport_id', $sport->id)->where('gender', $v)->count() == 0)
                            ΟΧΙ
                        @else
                            ΝΑΙ
                        @endif  
                    </td>
                @endforeach

            </tr>
        @endforeach
    </tbody>
</table>