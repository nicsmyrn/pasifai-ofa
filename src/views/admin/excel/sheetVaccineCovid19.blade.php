<table>
    <thead>
        <tr>
            <th>ΕΠΩΝΥΜΟ</th>
            <th>ΟΝΟΜΑ</th>
            <th>ΑΜΚΑ</th>
            <th>ΗΜ. ΓΕΝΝΗΣΗΣ</th>
            <th>ΚΙΝΗΤΟ</th>
            <th>ΣΤΑΘΕΡΟ</th>
            <th>ΣΧΟΛ. ΜΟΝΑΔΑ</th>
        </tr>
    </thead>
    <tbody>
        @foreach($teachers as $teacher)
        <tr>
            <td>{{ $teacher['last_name'] }}</td>
            <td>{{ $teacher['first_name'] }}</td>
            <td>{{ $teacher['amka'] }}</td>
            <td>{{ Carbon\Carbon::parse($teacher['birth'])->format('d/m/Y') }}</td>
            <td>{{ $teacher['mobile'] }}</td>
            <td>{{ $teacher['phone'] }}</td>
            <td>{{ $teacher['base_school'] }}</td>
        </tr>
        @endforeach
    </tbody>
</table>