<table>
    <thead>
        <tr>
            <th>ΟΝΟΜΑΤΕΠΩΝΥΜΟ</th>
            <th>ΠΑΤΡΩΝΥΜΟ</th>
            <th>ΕΤΟΣ ΓΕΝΗΣΗΣ</th>
            <th>ΦΥΛΟ</th>
            <th>ΤΑΞΗ</th>
            <th>ΣΧΟΛΙΚΗ ΜΟΝΑΔΑ</th>
            <th>Αρ.</th>
            <th>Παρατηρήσεις</th>
            <th>Κατάταξη</th>
            <th>Χρόνος</th>
        </tr>
    </thead>
    <tbody>
        @foreach($students as $student)
            <tr>
                <td>
                    {{ $student['last_name']}} {{ $student['first_name']}}
                </td>
                <td>
                    {{ $student['middle_name']}}
                </td>
                <td>
                    {{ $student['year_birth']}}
                </td>
                <td>
                    @if($student['sex'] == 0)
                        Θήλυ
                    @else
                        Άρρεν
                    @endif
                </td>
                <td>
                    @if($student['class'] == 1)
                        Α
                    @elseif($student['class'] == 2)
                        Β
                    @elseif($student['class'] == 3)
                        Γ
                    @endif
                </td>
                <td>
                    {{ $student['school']['name']}}
                </td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        @endforeach
    </tbody>
</table>