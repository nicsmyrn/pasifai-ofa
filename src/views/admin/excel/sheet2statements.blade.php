@foreach($sports as $sport)

        @foreach($gender as $k=>$v)
            @if($statements->where('sport_id', $sport->id)->where('gender', $v)->count() > 0)

                <table border="1">
                    <thead>
                        <tr>
                            <th colspan="2">{!! $sport->name !!} - {!! $v == 'male' ? 'ΑΓΟΡΙΑ' : 'ΚΟΡΙΤΣΙΑ' !!}</th>
                        </tr>
                        <tr>
                            <th>α/α</th>
                            <th>Σχολική Μονάδα</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php $counter = 1; ?>
                        @foreach($statements->where('sport_id', $sport->id)->where('gender', $v) as $index=>$record)
                            <tr>
                                <td>{!! $counter !!}</td>
                                <td>
                                    {!! $schools->where('id', $record['school_id'])->first()->name !!}
                                </td>
                            </tr>
                            <?php $counter ++; ?>
                        @endforeach
                    </tbody>
                </table>

            @endif

        @endforeach

@endforeach