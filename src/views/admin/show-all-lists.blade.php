@extends('app')

@section('title')
    Προβολή Λίστας Συμμετοχής
@endsection

@section('header.style')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" integrity="sha256-FdatTf20PQr/rWg+cAKfl6j4/IY3oohFAJ7gVC3M34E=" crossorigin="anonymous" />
@endsection

@section('scripts.footer')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js" integrity="sha256-d/edyIFneUo3SvmaFnf96hRcVBcyaOy96iMkPez1kaU=" crossorigin="anonymous"></script>

	@include('ofa::admin._javascript')
@endsection

@section('content')
    <div class="col-md-12 col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Προβολή Λίστας Αγώνων
            </div>
            <div class="panel-body">
                <h3 class="text-center">

                <select size="30">
                    <option></option>
                    @foreach($schools as $school)
                        <option value="{{ route('OFA::admin::showLists',str_replace(' ', '-', $school->name)) }}"
                        >{{ $school->name }}</option>
                    @endforeach
                </select>
                </h3>
            </div>
        </div>
    </div>
@endsection
