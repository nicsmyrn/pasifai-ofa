@extends('app')

@section('title')
    Στατιστικά
@endsection

@section('header.style')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
    <style>
        .special {
            font-weight: bold !important;
            color: #fff !important;
            background: #bc0000 !important;
            text-transform: uppercase;
        }

    </style>
@endsection

@section('scripts.footer')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
        <script>
            $(document).ready(function() {
                $('.statistic').change(function(){
                    var url = "{{ route('OFA::admin::statistics') }}?first="+$('#first-statistic').val()+"&second="+$('#second-statistic').val();
                    window.location = url;
                })
            });
        </script>
@endsection

@section('content')
    <div class="col-md-12 col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">

            </div>
            <div class="panel-body">
                <div class="col-md-6">
                    <table class="table table-hover table-bordered">
                        <thead>
                            <tr>
                                <th class="text-center" colspan="4">
                                    {{ Form::label('school_type', 'Τύπος Σχολικής Μονάδας:') }}
                                    <select id="first-statistic" class="statistic" name="school_type">
                                        @foreach($school_types as $k=>$v)
                                            <option value="{{ $k }}" {{ Request::get('first') == $k ?'selected':'' }}>{{ $v }}</option>
                                        @endforeach
                                    </select>
                                </th>
                            </tr>
                            <tr>
                                <th class="text-center">Άθλημα</th>
                                <th class="text-center">Αρ. Ομάδων αγοριών</th>
                                <th class="text-center">Αρ. Ομάδων κοριτσιών</th>
                                <th class="text-center">Σύνολο Σχολικών Ομάδων</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($teams as $team)
                                <tr>
                                    <td>
                                        {{ $team['sport_name'] }}
                                    </td>
                                    <td class="text-center">
                                        {{ $team['male'] }}
                                    </td>
                                    <td class="text-center">
                                        {{ $team['female'] }}
                                    </td>
                                    <td class="text-center">
                                        {{ $team['sum'] }}
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12 col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">

            </div>
            <div class="panel-body">
                <div class="col-md-6">
                    <table class="table table-hover table-bordered">
                        <thead>
                            <tr>
                                <th class="text-center" colspan="4">
                                    {{ Form::label('school_type', 'Τύπος Σχολικής Μονάδας:') }}
                                    <select id="second-statistic" class="statistic" name="school_type">
                                        @foreach($school_types as $k=>$v)
                                            <option value="{{ $k }}" {{ Request::get('second') == $k ?'selected':'' }}>{{ $v }}</option>
                                        @endforeach
                                    </select>
                                </th>
                            </tr>
                            <tr>
                                <th class="text-center">Άθλημα</th>
                                <th class="text-center">Αρ. μαθητών (αγόρια)</th>
                                <th class="text-center">Αρ. μαθητριών (κορίτσια)</th>
                                <th class="text-center">Σύνολο μαθητών</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($students as $sportStudent)
                                <tr>
                                    <td>
                                        {{ $sportStudent['sport_name'] }}
                                    </td>
                                    <td class="text-center">
                                        {{ $sportStudent['male_counter'] }}
                                    </td>
                                    <td class="text-center">
                                        {{ $sportStudent['female_counter'] }}
                                    </td>
                                    <td class="text-center">
                                        {{ $sportStudent['sum'] }}
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
@endsection
