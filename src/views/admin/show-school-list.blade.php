@extends('app')

@section('title')
    Λίστα Συμμετοχής για: {{ $name }}
@endsection

@section('header.style')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" integrity="sha256-FdatTf20PQr/rWg+cAKfl6j4/IY3oohFAJ7gVC3M34E=" crossorigin="anonymous" />
    <link href="https://cdn.datatables.net/1.10.9/css/dataTables.bootstrap.min.css" rel="stylesheet">
@endsection

@section('scripts.footer')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js" integrity="sha256-d/edyIFneUo3SvmaFnf96hRcVBcyaOy96iMkPez1kaU=" crossorigin="anonymous"></script>

    <script src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.9/js/dataTables.bootstrap.min.js"></script>
	@include('ofa::admin._javascript')
@endsection

@section('content')
    <div class="col-md-12 col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Λίστα Συμμετοχής για: {{ $name }}
            </div>
            <div class="panel-body">
                <h3 class="text-center">

                <select size="40">
                    <option></option>
                    @foreach($schools as $school)
                        <option value="{{ route('OFA::admin::showLists',str_replace(' ', '-', $school->name)) }}"
                            @if(str_replace(' ', '-', $name) == str_replace(' ', '-', $school->name))
                                selected
                            @endif
                        >{{ $school->name }}</option>
                    @endforeach
                </select>
                </h3>
            </div>
        </div>
    </div>

    <div class="col-md-12 col-lg-12">
        @include('ofa::school.tables._index-lists-table', ['name' => $name, 'school_id' => $school_id])
    </div>
@endsection
