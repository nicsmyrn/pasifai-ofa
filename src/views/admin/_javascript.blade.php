<script>
    $(document).ready(function() {


        $('select').change(function(){
            var url = $(this).val();
            window.location = url;
        })
        .select2({
            placeholder: "Επιλογή Σχολείου",
            theme: "classic",
            language: {
                noResults: function() {
                    return "Δεν υπάρχει αποτέλεσμα αναζήτησης";
                }
            }
        });


        var table = $('#lists').DataTable({
                "order": [[ 2, "asc" ]],
                "aoColumnDefs": [ { "bSortable": false, "aTargets": [ 3, 7, 8 ] } ],
                "language": {
                            "lengthMenu": "Προβολή _MENU_ εγγραφών ανά σελίδα",
                            "zeroRecords": "Δεν βρέθηκε καμία εγγραφή",
                            "info": "Προβολή σελίδας _PAGE_ από _PAGES_",
                            "infoEmpty": "Καμία εγγραφή διαθέσιμη",
                            "infoFiltered": "(φιλτράρισμα  από  _MAX_ συνολικές εγγραφές)",
                            "search": "Αναζήτηση:",
                            "paginate": {
                                  "previous": "Προηγούμενη",
                                  "next" : "Επόμενη"
                                }
                        }
        });

    });
</script>