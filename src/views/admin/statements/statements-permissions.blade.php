@extends('app')

@section('header.style')
    <style>
        table{
            border-collapse: collapse;
            border-spacing: 20px;
        }

        table, th, td {
            border: 1px solid black;
            padding: 20px;
        }
    </style>
@endsection

@section('title')
    Δικαιώματα Συμμετοχής
@endsection

@section('content')
    <div class="col-md-12 col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">

            </div>
            <div class="panel-body">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <table>
                        <thead>
                            <tr>
                                <th align="center">Τύπος Σχολείου</th>
                                <th align="center">Δικαίωμα Πρόσβασης</th>
                                <th align="center">Ενέργεια</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td align="center" valign="middle">Λύκεια - ΕΠΑΛ</td>
                                <td align="center" valign="middle">
                                    @if($data['secondary'])
                                        <h3>
                                            <span style="color: #008000">
                                                <i class="fa fa-unlock" aria-hidden="true"></i>
                                            </span>
                                        </h3>
                                        <span class="label label-success">Ξεκλείδωτο</span>
                                    @else
                                        <h3>
                                            <span style="color: #ff4d4d">
                                                <i class="fa fa-lock" aria-hidden="true"></i>
                                            </span>
                                        </h3>

                                        <span class="label label-danger">κλειδωμένο</span>
                                    @endif
                                </td>
                                <td align="center" valign="middle">
                                    <a href="{!! route('OFA::admin::changeAccessSecondary') !!}" class="btn btn-danger btn-sm">
                                        αλλαγή
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="middle">Γυμνάσια</td>
                                <td align="center" valign="middle">
                                    @if($data['primary'])
                                        <h3>
                                            <span style="color: #008000">
                                                <i class="fa fa-unlock" aria-hidden="true"></i>
                                            </span>
                                        </h3>

                                        <span class="label label-success">Ξεκλείδωτο</span>
                                    @else
                                        <h3>
                                            <span style="color: #ff4d4d">
                                                <i class="fa fa-lock" aria-hidden="true"></i>
                                            </span>
                                        </h3>

                                        <span class="label label-danger">κλειδωμένο</span>
                                    @endif
                                </td>
                                <td align="center" valign="middle">
                                    <a href="{!! route('OFA::admin::changeAccessPrimary') !!}" class="btn btn-danger btn-sm">
                                        αλλαγή
                                    </a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div class="col-md-2"></div>
            </div>
        </div>
    </div>

@endsection
