<html>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

                @foreach($sports as $sport)

                    @foreach($gender as $k=>$v)
                        <table>
                            <thead>
                                <tr>
                                    <th colspan="2">{!! $sport->name !!} - {!! $v == 'male' ? 'ΑΓΟΡΙΑ' : 'ΚΟΡΙΤΣΙΑ' !!}</th>
                                </tr>
                                <tr>
                                    <th>α/α</th>
                                    <th>Σχολική Μονάδα</th>
                                </tr>
                            </thead>

                            <tbody>
                                <?php $counter = 1; ?>
                                @foreach($statements->where('sport_id', $sport->id)->where('gender', $v) as $index=>$record)
                                    <tr>
                                        <td>{!! $counter !!}</td>
                                        <td>
                                            {!! $schools->where('id', $record['school_id'])->first()->name !!}
                                        </td>
                                    </tr>
                                    <?php $counter ++; ?>
                                @endforeach
                            </tbody>
                        </table>
                    @endforeach

                @endforeach

</html>
