<html>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

        <table>
            <thead>
                <tr>
                    <th>Άθλημα</th>
                    <th>Αγόρια</th>
                    <th>Κορίτσια</th>
                </tr>
            </thead>

            <tbody>
                @foreach($sports as $sport)

                    <tr>
                        <td>
                            {!! $sport->name !!}
                        </td>

                        @foreach($gender as $k=>$v)
                            <td>
                                {!! $statements->where('sport_id', $sport->id)->where('gender', $v)->count(); !!}
                            </td>
                        @endforeach

                    </tr>


                @endforeach

            </tbody>
        </table>
</html>
