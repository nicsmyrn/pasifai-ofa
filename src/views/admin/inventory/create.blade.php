@extends('app')

@section('header.style')
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/@mdi/font@4.x/css/materialdesignicons.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/vuetify@2.x/dist/vuetify.min.css" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui">
@endsection 

@section('scripts.footer')
    <script>
        window.api_token = "{{ Auth::user()->api_token }}";
    </script>
    <script src="{{ mix('vendor/ofa/js/ofaInventory.js') }}"></script>

@endsection

@section('title')
    Προσθήκη Υλικού - ΟΦΑ
@endsection

@section('content')
    <new-form
        :token="token"
    >
    </new-form>
@endsection

@section('loader')
    @include('vueloader')
@endsection