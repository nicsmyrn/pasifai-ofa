<?php

namespace Pasifai\Ofa\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ChessRequest extends FormRequest
{


    public function __construct()
    {
        // dd($this->validationData());
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // $amka = $this->all()['amka'];

        // $days = substr($amka, 0,2);

        // $month = substr($amka, 2, 2);

        // $year = substr($amka, 4, 2);

        // $regex = "^\d{2}$year-$month-$days$";

        return [
            // 'amka'              => 'required|digits:11',
            // 'date'              => 'required|date_format:Y-m-d|regex:/'.$regex.'/u',
            // 'date'              => 'required|date_format:Y-m-d',
            'school_name'          => 'required|string|min:3',
            'lichess_username'            => 'required|alpha_dash|min:3|max:255|unique:students,lichess_username',
            'student_class'             => 'required||string',
            // 'school'            => 'required|digits_between:1,100'
        ];
    }

    public function messages()
    {
        return  [
            // 'amka.required'  => 'Ο αριθμός ΑΜΚΑ είναι υποχρεωτικός',
            // 'date.required'  => 'Η ημερομηνία γέννησης είναι υποχρεωτική',
            'school_name.required'  => 'Η Σχολική Μονάδα είναι υποχρεωτική',
            'lichess_username.required'  => 'Το όνομα χρήστη του Lichess είναι υποχρεωτικό',
            'lichess_username.alpha_dash'  => 'Το όνομα χρήστη του Lichess δεν καλύπτει τις προϋποθέσεις',
            'lichess_username.unique'  => 'Το όνομα χρήστη του Lichess υπάρχει ήδη.',
            'student_class.required'  => 'Η τάξη είναι υποχρεωτική',
            // 'school.required'  => 'Η Σχολική Μονάδα είναι υποχρεωτική',

            // 'amka.digits'  => 'Ο αριθμός ΑΜΚΑ είναι αριθμός με 11 ψηφία',
            // 'date.date_format'  => 'Η ημερομηνία πρέπει να έχει μορφή YYYY-mm-dd',
            'school_name.string'  => 'Η Σχολική Μονάδα πρέπει να έχει τουλάχιστον 3 χαρακτήρες',
            'school_name.min'  => 'Η Σχολική Μονάδα πρέπει να έχει τουλάχιστον 3 χαρακτήρες',
            'student_class.string'  => 'Η Τάξη πρέπει να είναι κείμενο',
            // 'school.digits_between'  => 'Η Σχολική Μονάδα πρέπει να είναι μεταξύ 1 - 100',

            // 'date.regex' => 'Η ημερομηνία γέννησης πρέπει να ταιριάζει με το ΑΜΚΑ'
        ];
    }
}
