<?php

namespace Pasifai\Ofa\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Year;

class StudentRequest extends FormRequest
{
    protected  $current_year;
    protected $min_year;
    protected $max_year;

    public function __construct()
    {
        $this->current_year = (int)substr(Year::where('current', true)->first()->name, -4);

        $this->max_year = $this->current_year - 8;
        $this->min_year = $this->current_year - 18;
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'new_student.am'            => 'required|integer|unique:ofa_students,am,NULL,am,school_id,'.\Auth::user()->userable->id,
            'new_student.last_name'     => 'required|min:3',
            'new_student.first_name'    => 'required|min:3',
            'new_student.middle_name'   => 'required|min:3',
            'new_student.mothers_name'  => 'required|min:3',
            'new_student.class'         => 'required|integer|between:1,6',
            'new_student.sex'           => 'required|integer|between:0,1',
            'new_student.year_birth'    => 'required|integer|between:'.$this->min_year.','.$this->max_year,
        ];
    }

    public function messages()
    {
        return  [
            'new_student.am.required' => 'Ο αριθμός Μητρώου είναι υποχρεωτικός',
            'new_student.last_name.required' => 'Το Επώνυμο είναι υποχρεωτικό',
            'new_student.first_name.required' => 'Το Όνομα είναι υποχρεωτικό',
            'new_student.middle_name.required' => 'Το Πατρώνυμο είναι υποχρεωτικό',
            'new_student.mothers_name.required' => 'Το Μητρώνυμο είναι υποχρεωτικό',
            'new_student.class.required' => 'Η Τάξη είναι υποχρεωτική',
            'new_student.sex.required' => 'Το Φύλο είναι  υποχρεωτικό',
            'new_student.year_birth.required' => 'Το έτος γέννησης είναι υποχρεωτικό',

            'new_student.am.integer' => 'Ο αριθμός Μητρώου πρέπει να είναι ακέραιος αριθμός',
            'new_student.class.integer' => 'Η τάξη πρέπει να είναι ακέραιος αριθμός',
            'new_student.sex.integer' => 'Το φύλο πρέπει να είναι ακέραιος αριθμός',
            'new_student.year_birth.integer' => 'Το έτος γέννησης πρέπει να είναι ακέραιος αριθμός',

            'new_student.last_name.min' => 'Το Επώνυμο πρέπει να είναι τουλάχιστον 3 χαρακτήρες',
            'new_student.first_name.min' => 'Το Όνομα πρέπει να είναι τουλάχιστον 3 χαρακτήρες',
            'new_student.middle_name.min' => 'Το Πατρώνυμο πρέπει να είναι τουλάχιστον 3 χαρακτήρες',
            'new_student.mothers_name.min' => 'Το Μητρώνυμο πρέπει να είναι τουλάχιστον 3 χαρακτήρες',

            'new_student.class.between' => 'Η τάξη πρέπει να είναι ή Α ή Β ή Γ',
            'new_student.sex.between' => 'Το φύλο πρέπει να είναι ή κορίτσι ή αγόρι',
            'new_student.year_birth.between' => 'Το έτος γέννησης μπορεί να είναι μεταξύ '. $this->min_year . ' και '. $this->max_year,
            'new_student.am.unique' => 'Ο ΑΜ υπάρχει καταχωρημένος σε άλλον μαθητή',
        ];
    }
}
