<?php

namespace Pasifai\Ofa\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VaccineRequest extends FormRequest
{


    public function __construct()
    {
        // dd($this->validationData());
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // $amka = $this->all()['amka'];

        // $days = substr($amka, 0,2);

        // $month = substr($amka, 2, 2);

        // $year = substr($amka, 4, 2);

        // $regex = "^\d{2}$year-$month-$days$";

        return [
            'amka'              => 'required|digits:11',
            // 'date'              => 'required|date_format:Y-m-d|regex:/'.$regex.'/u',
            'date'              => 'required|date_format:Y-m-d',
            'fullname'          => 'required|string|min:3',
            'mobile'            => 'required|digits:10',
            'phone'             => 'required|digits:10',
            'school'            => 'required|digits_between:1,100'
        ];
    }

    public function messages()
    {
        return  [
            'amka.required'  => 'Ο αριθμός ΑΜΚΑ είναι υποχρεωτικός',
            'date.required'  => 'Η ημερομηνία γέννησης είναι υποχρεωτική',
            'fullname.required'  => 'Το ονοματεπώνυμο είναι υποχρεωτικό',
            'mobile.required'  => 'Το κινητό τηλέφωνο είναι υποχρεωτικό',
            'phone.required'  => 'Το σταθερό τηλέφωνο είναι υποχρεωτικό',
            'school.required'  => 'Η Σχολική Μονάδα είναι υποχρεωτική',

            'amka.digits'  => 'Ο αριθμός ΑΜΚΑ είναι αριθμός με 11 ψηφία',
            'date.date_format'  => 'Η ημερομηνία πρέπει να έχει μορφή YYYY-mm-dd',
            'fullname.string'  => 'Το ονοματεπώνυμο πρέπει να έχει τουλάχιστον 3 χαρακτήρες',
            'fullname.min'  => 'Το ονοματεπώνυμο πρέπει να έχει τουλάχιστον 3 χαρακτήρες',
            'mobile.digits'  => 'Το κινητό τηλέφωνο είναι αριθμος 10 ψηφίων',
            'phone.digits'  => 'Το σταθερό τηλέφωνο είναι αριθμός 10 ψηφίων',
            'school.digits_between'  => 'Η Σχολική Μονάδα πρέπει να είναι μεταξύ 1 - 100',

            // 'date.regex' => 'Η ημερομηνία γέννησης πρέπει να ταιριάζει με το ΑΜΚΑ'
        ];
    }
}
