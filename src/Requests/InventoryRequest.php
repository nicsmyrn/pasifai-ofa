<?php

namespace Pasifai\Ofa\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InventoryRequest extends FormRequest
{


    public function __construct()
    {
        // dd($this->validationData());
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                  => 'required',
            'date_in'               => 'required|date_format:Y-m-d',
            'amount'                => 'required|numeric',
            'quantity'              => 'required|integer',
            'category'              => 'required|integer',
            'donation'              => 'required|boolean'
        ];
    }

    public function messages()
    {
        return  [
            'name.required'  => 'Το όνομα είναι υποχρεωτικό',
            'date_in.required'  => 'Η ημερομηνία είναι υποχρεωτική',
            'amount.required'  => 'Η ποσότητα είναι υποχρεωτική',
            'quantity.required'  => 'Το ποσό είναι υποχρεωτικό',
            'category.required'  => 'Η κατηγορία ειναι υποχρεωτική',
            'donation.required'  => 'Η Δωρεά είναι υποχρεωτική',

            'date_in.date_format' => 'Η ημερομηνία δεν είναι σωστή',
            'amount.numeric'        => 'Το ποσό πρέπει να είναι αριθμός',
            'quantity.integer'        => 'Η ποσότητα πρέπει να είναι ακέραιος',
            'category.integer'          => 'Η κατηγορία πρέπει να είναι ακέραιος',
            'donation.boolean'      => 'Η δωρεά είναι ναι ή όχι',
            
        ];
    }
}
