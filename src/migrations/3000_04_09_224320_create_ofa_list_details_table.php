<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfaListDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ofa_list_details', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('list_id')->unsigned();
            $table->foreign('list_id')->references('id')->on('ofa_lists')->onDelete('cascade');

            $table->integer('student_id')->unsigned();
            $table->foreign('student_id')->references('am')->on('ofa_students')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->boolean('statement');

            $table->smallInteger('sport_id_special')->nullable();

            $table->unique(['list_id', 'student_id']);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ofa_list_details');
    }
}
