<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfaListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ofa_lists', function (Blueprint $table) {
            //Create group_lists table
                $table->increments('id');

                $table->integer('year_id')->unsigned();
                $table->foreign('year_id')->references('id')->on('years_placements')->onDelete('cascade');

                $table->integer('school_id')->unsigned();
                $table->foreign('school_id')->references('id')->on('schools')->onDelete('cascade');

                $table->integer('sport_id')->unsigned();
                $table->foreign('sport_id')->references('id')->on('ofa_sports')->onDelete('cascade');

                $table->smallInteger('gender');

                $table->string('filename')->nullable();
                $table->boolean('locked')->default(false);


                $table->unique(['year_id', 'school_id', 'sport_id', 'gender']);

                $table->string('synodos')->nullable();
                $table->string('teacher_name')->nullable();

                $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ofa_lists');
    }
}
