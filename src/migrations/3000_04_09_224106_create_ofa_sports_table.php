<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfaSportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ofa_sports', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');
            $table->boolean('individual');
            $table->smallInteger('list_limit');

            $table->softDeletes();

            $table->string('identifier')->nullable();

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ofa_sports');
    }
}
