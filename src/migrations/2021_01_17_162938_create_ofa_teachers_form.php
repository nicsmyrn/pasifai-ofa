<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfaTeachersForm extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ofa_teachers_form', function(Blueprint $table){
            $table->uuid('id')->primary();

            $table->unsignedInteger('from_user');
            $table->foreign('from_user')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');

            $table->string('fullname');
            $table->string('email');
            $table->string('phone', 10)->nullable();

            $table->string('subject');
            $table->longText('message');

            $table->timestamp('read_at')->nullable();

            $table->timestamps();
            $table->softDeletes();            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ofa_teachers_form');
    }
}
