<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfaParticipationStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ofa_participation_status', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('list_id')->unsigned();
            $table->foreign('list_id')->references('id')->on('ofa_lists')->onDelete('cascade');


            $table->string('description');
            $table->tinyInteger('phase')->unsigned()->default(0);

            $table->unique(['list_id', 'phase']);

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ofa_participation_status');
    }
}
