<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableOfaInventory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ofa_inventory', function(Blueprint $table){
            $table->uuid('id')->primary();
            $table->string('name');

            $table->integer('quantity');
            $table->date('date_in');
            $table->boolean('donation');
            $table->smallInteger('amount')->default(1);
            $table->longText('remarks');

            $table->unsignedInteger('category_id');
            $table->foreign('category_id')->references('id')->on('ofa_inventory_categories')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ofa_inventory');
    }
}
