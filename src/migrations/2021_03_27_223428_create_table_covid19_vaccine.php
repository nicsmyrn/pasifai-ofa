<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCovid19Vaccine extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ofa_covid19_vaccine', function(Blueprint $table){
            $table->integer('uid')->unsigned();
            $table->foreign('uid')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->boolean('vaccinated')->default(false);
            $table->integer('school_base')->unsigned();
            $table->foreign('school_base')->references('id')->on('schools')->onUpdate('cascade')->onDelete('cascade');

            $table->primary('uid');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ofa_covid19_vaccine');
    }
}
