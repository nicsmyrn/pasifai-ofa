<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfaStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ofa_students', function (Blueprint $table) {
            $table->integer('am')->unsigned();

            $table->string('last_name');
            $table->string('first_name');
            $table->string('middle_name')->nullable();

            $table->string('mothers_name')->nullable();
            $table->integer('year_birth')->unsigned();

            $table->tinyInteger('class');
            $table->boolean('sex');

            $table->integer('school_id')->unsigned();
            $table->foreign('school_id')->references('id')->on('schools')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->boolean('loader')->default(false);

            $table->primary(['am', 'school_id']);

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ofa_students');
    }
}
