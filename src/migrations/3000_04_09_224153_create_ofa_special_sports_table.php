<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfaSpecialSportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ofa_special_sports', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');

            $table->integer('sport_id')->unsigned();
            $table->foreign('sport_id')->references('id')->on('ofa_sports')->onDelete('cascade');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ofa_special_sports');
    }
}
