<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfaPStatusDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ofa_p_status_details', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('p_status_id')->unsigned();
            $table->foreign('p_status_id')->references('id')->on('ofa_participation_status')->onDelete('cascade');

            $table->integer('list_details_id')->unsigned();
            $table->foreign('list_details_id')->references('id')->on('ofa_list_details')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ofa_p_status_details');
    }
}
