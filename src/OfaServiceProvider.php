<?php

namespace Pasifai\Ofa;

use Illuminate\Support\ServiceProvider;
use Pasifai\Ofa\Models\Student;

class OfaServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__.'/migrations');

        $this->loadViewsFrom(__DIR__.'/views', 'ofa');

        $configPath = __DIR__ . '/../config/ofa.php';
        $publishPath = base_path('config/ofa.php');

        $this->publishes([
            __DIR__.'/views' => base_path('resources/views/pasifai/ofa'),
            $configPath => $publishPath
        ]);

        $this->publishes([
            __DIR__.'/assets' => resource_path('assets/vendor/ofa'),
        ], 'vue-components');

        $this->publishes([
            __DIR__.'/published' => public_path('vendor/ofa'),
        ], 'ofa_public');
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
//        $this->app->bind('student', function($student) {
//            return Student::where('school_id', auth()->guard('web')->user()->userable->id)->where('am', $student)->first();
//        });

        $configPath = __DIR__ . '/../config/ofa.php';

        $this->mergeConfigFrom($configPath, 'requests');

        include __DIR__.'/routes/web.php';
        include __DIR__.'/routes/api.php';

        $this->app->make('Pasifai\Ofa\Controllers\OfaController');
    }
}
