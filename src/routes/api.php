<?php
Route::prefix('api')->group(function () {
    
    Route::group(['prefix' => 'ofa', 'as' => 'Ofa::'], function () {

        Route::group(['prefix' => 'inventory', 'as' => 'Inventory::'], function(){
            Route::get('getInventoryCategories', 'Pasifai\Ofa\Controllers\API\AdminInventoryApiController@getCategories');
            Route::post('saveInventory', 'Pasifai\Ofa\Controllers\API\AdminInventoryApiController@saveInventory');
        
        });

        Route::group(['prefix' => 'school', 'as' => 'School::'], function () {
            Route::get('allStudents', 'Pasifai\Ofa\Controllers\API\SchoolGamesApiController@getAllStudents')->name('getStudents');
            Route::get('destroy/{student}', 'Pasifai\Ofa\Controllers\API\SchoolGamesApiController@destroy')->name('delete');

            Route::get('fetchDataForParticipationStatus', 'Pasifai\Ofa\Controllers\API\SchoolGamesApiController@fetchDataForParticipationStatus')->name('fetchDataForParticipationStatus'); 
            Route::post('checkIfListAlreadyExistsForParticipationStatus', 'Pasifai\Ofa\Controllers\API\SchoolGamesApiController@checkIfListAlreadyExistsForParticipationStatus')->name('checkIfListAlreadyExistsForParticipationStatus');
            Route::post('fetchStudentsFromParticipationStatus', 'Pasifai\Ofa\Controllers\API\SchoolGamesApiController@fetchStudentsFromParticipationStatus')->name('fetchStudentsFromParticipationStatus');

            Route::post('removeFromStatus', 'Pasifai\Ofa\Controllers\API\SchoolGamesApiController@removeFromStatus')->name('removeFromStatus');
            Route::post('AddToStudents', 'Pasifai\Ofa\Controllers\API\SchoolGamesApiController@AddToStudents')->name('AddToStudents');


            // Route::get('getTeachersPe11', 'Pasifai\Ofa\Controllers\API\SchoolGamesApiController@getTeachersPe11')->name('getTeachersPe11');    
            Route::post('fetchDataForLists', 'Pasifai\Ofa\Controllers\API\SchoolGamesApiController@fetchDataForLists')->name('fetchDataForLists');  
            // Route::get('fetchDataForListsIndividual', 'Pasifai\Ofa\Controllers\API\SchoolGamesApiController@fetchDataForListsIndividual')->name('fetchDataForListsIndividual'); 
 
            Route::post('fetchDataForSportEducation', 'Pasifai\Ofa\Controllers\API\SchoolGamesApiController@fetchDataForSportEducation')->name('fetchDataForSportEducation');  
            Route::get('fetchDataForSportEducationIndividual', 'Pasifai\Ofa\Controllers\API\SchoolGamesApiController@fetchDataForListsIndividual')->name('aaa'); 
           
            Route::post('checkIfListAlreadyExists', 'Pasifai\Ofa\Controllers\API\SchoolGamesApiController@checkIfListAlreadyExists')->name('checkIfListAlreadyExists');
            Route::post('checkIfListAlreadyExistsForIndividual', 'Pasifai\Ofa\Controllers\API\SchoolGamesApiController@checkIfListAlreadyExistsForIndividual')->name('checkIfListAlreadyExists');
            Route::post('checkIfListAlreadyExistsForIndividualPrimary', 'Pasifai\Ofa\Controllers\API\SchoolGamesApiController@checkIfParticipationPrimaryAlreadyExists')->name('checkIfParticipationPrimaryAlreadyExists');
        
            Route::post('checkIfSportEducationAlreadyExists', 'Pasifai\Ofa\Controllers\API\SchoolGamesApiController@checkIfSportEducationAlreadyExists')->name('checkIfSportEducationAlreadyExists');

            Route::post('insertNewList', 'Pasifai\Ofa\Controllers\API\SchoolGamesApiController@insertNewList')->name('insertNewList');
            Route::post('insertNewListIndividual', 'Pasifai\Ofa\Controllers\API\SchoolGamesApiController@insertNewListIndividual')->name('insertNewListIndividual');
        
            Route::post('insertNewParticipationPrimary', 'Pasifai\Ofa\Controllers\API\SchoolGamesApiController@insertNewParticipationPrimary')->name('insertNewParticipationPrimary');

            Route::post('temporarySaveNewList', 'Pasifai\Ofa\Controllers\API\SchoolGamesApiController@temporarySaveNewList')->name('temporarySaveNewList');

            Route::post('insertNewStudent', 'Pasifai\Ofa\Controllers\API\SchoolGamesApiController@insertNewStudent')->name('insertNewStudent');
            Route::post('insertNewStudentPrimary', 'Pasifai\Ofa\Controllers\API\SchoolGamesApiController@insertNewStudentPrimary')->name('insertNewStudentPrimary');
    
        });

        Route::group(['prefix' => 'teacher', 'as' => 'Teacher::'], function(){
            Route::post('sentFormRequest', 'Pasifai\Ofa\Controllers\API\TeacherApiController@sentFormRequest');
            Route::post('sentVaccineRequest', 'Pasifai\Ofa\Controllers\API\TeacherApiController@sentVaccineRequest');
            Route::post('deleteVaccineRequest', 'Pasifai\Ofa\Controllers\API\TeacherApiController@deleteVaccineRequest');
            Route::get('getTeacherProfile', 'Pasifai\Ofa\Controllers\API\TeacherApiController@getTeacherProfile');
        });

        Route::group(['prefix' => 'student', 'as' => 'Teacher::'], function(){
            Route::post('sentChessRequest', 'Pasifai\Ofa\Controllers\API\StudentApiController@sentChessRequest');
            Route::post('deleteChessRequest', 'Pasifai\Ofa\Controllers\API\StudentApiController@deleteChessRequest');
            Route::get('getStudentProfile', 'Pasifai\Ofa\Controllers\API\StudentApiController@getStudentProfile');
        });
    });
});
