<?php

Route::group(['middleware' => ['web']], function () {

    Route::get('pe11form', 'Pasifai\Ofa\Controllers\TeachersPe11Controller@getForm')->name('getFormPe11');
    Route::get('Εμβολιασμός-Covid19', 'Pasifai\Ofa\Controllers\Covid19Controller@getForm')->name('covid19');

    Route::get('Chess-Tournament', 'Pasifai\Ofa\Controllers\ChessController@getForm')->name('Chess-Tournament');

    Route::get('vaccineCovid19excel', 'Pasifai\Ofa\Controllers\AdminCovid19Controller@vaccineCovid19excel')->name('vaccineCovid19excel');

    Route::group(['middleware' => config()->get('requests.middleware'),'prefix'=>'ΑΘΛΗΤΙΚΟΙ-ΑΓΩΝΕΣ', 'as'=>'OFA::'], function() {

        if(config('requests.ofa_offline')){
            //
        }else{
            Route::get('ΔΗΛΩΣΗ-ΑΘΛΗΜΑΤΩΝ', 'Pasifai\Ofa\Controllers\SchoolOfaController@getStatementIndex')->name('statementSecondary');
            Route::get('ΔΗΛΩΣΗ-ΑΘΛΗΜΑΤΩΝ-ΓΥΜΝΑΣΙΩΝ', 'Pasifai\Ofa\Controllers\SchoolOfaPrimaryController@getStatementIndex')->name('statementPrimary');
    
            Route::post('ΔΗΛΩΣΗ-ΑΘΛΗΜΑΤΩΝ', 'Pasifai\Ofa\Controllers\SchoolOfaController@postSchoolStatement')->name('postSchoolStatement');
            Route::post('ΔΗΛΩΣΗ-ΑΘΛΗΜΑΤΩΝ-ΓΥΜΝΑΣΙΩΝ', 'Pasifai\Ofa\Controllers\SchoolOfaPrimaryController@postSchoolStatement')->name('postSchoolStatementPrimary');
    
            Route::get('Αθλοπαιδεία-Λυκείου', 'Pasifai\Ofa\Controllers\SchoolOfaController@getSportEducationforLyceum')->name('sportEducationForLyceum');
            Route::get('Λίστα-Ομαδικών-Αθλημάτων', 'Pasifai\Ofa\Controllers\SchoolOfaController@getLists')->name('sportList');
            Route::get('Κατάσταση-Συμμετοχής-Ατομικών-Αθλημάτων', 'Pasifai\Ofa\Controllers\SchoolOfaController@getListsIndividual')->name('sportListIndividual');
            Route::get('Αθλοπαιδεία', 'Pasifai\Ofa\Controllers\SchoolOfaPrimaryController@getSportEducation')->name('sportEducation');
            Route::get('Κατάσταση-Συμμετοχής', 'Pasifai\Ofa\Controllers\SchoolOfaParticipationsController@getParticipationStatus')->name('sportParticipationStatus');
    
            Route::get('ΑΡΧΕΙΟ-ΔΗΛΩΣΕΩΝ', 'Pasifai\Ofa\Controllers\SchoolOfaPrimaryController@archivesStatements')->name('archivesStatements');
            Route::get('ΑΡΧΕΙΟ-ΔΗΛΩΣΕΩΝ-ΛΥΚΕΙΩΝ', 'Pasifai\Ofa\Controllers\SchoolOfaController@archivesStatementsHighSchool')->name('archivesStatementsHighSchool');
    
            Route::get('Καταστάσεις-Μαθητών', 'Pasifai\Ofa\Controllers\SchoolOfaController@listOfStudents')->name('listOfStudents');
            Route::get('Καταστάσεις-Μαθητών-Γυμνασίου', 'Pasifai\Ofa\Controllers\SchoolOfaPrimaryController@listOfStudentsPrimary')->name('listOfStudentsPrimary');
    
            Route::get('ΜΑΘΗΤΗΣ/{student}', 'Pasifai\Ofa\Controllers\SchoolOfaController@editStudent')->name('editStudent');
            Route::post('ΜΑΘΗΤΗΣ/{student}', 'Pasifai\Ofa\Controllers\SchoolOfaController@updateStudent')->name('updateStudent');
            Route::post('delete/specificStudent', 'Pasifai\Ofa\Controllers\SchoolOfaController@deleteStudentFromSpecificSchool')->name('deleteStudent');
            Route::post('insertFromMyschool', 'Pasifai\Ofa\Controllers\SchoolOfaController@insertStudentsFromMySchool')->name('insertStudentsFromMySchool');
    
    
            Route::get('ΜΑΘΗΤΗΣ/primary/{student}', 'Pasifai\Ofa\Controllers\SchoolOfaPrimaryController@editStudent')->name('primary.editStudent');
            Route::post('ΜΑΘΗΤΗΣ/primary/{student}', 'Pasifai\Ofa\Controllers\SchoolOfaPrimaryController@updateStudent')->name('primary.updateStudent');
            Route::post('delete/primary/specificStudent', 'Pasifai\Ofa\Controllers\SchoolOfaPrimaryController@deleteStudentFromSpecificSchool')->name('primary.deleteStudent');
            Route::post('primary/insertFromMyschool', 'Pasifai\Ofa\Controllers\SchoolOfaPrimaryController@insertStudentsFromMySchool')->name('primary.insertStudentsFromMySchool');
        }

        Route::group(['prefix'=>'ΔΙΑΧΕΙΡΙΣΗ', 'as'=>'admin::'], function() {

            Route::group(['prefix'=>'ΥΛΙΚΟ-ΟΦΑ', 'as'=>'Inventory::'], function() {
                Route::get('/', 'Pasifai\Ofa\Controllers\InventoryOfaController@index')->name('inventoryIndex');
            });

            Route::get('Games-to-Excel', 'Pasifai\Ofa\Controllers\AdminOfaController@gamesToExcel')->name('getGamesToExcel');
            Route::get('Δηλώσεις-Συμμετοχής-Σε-Excel', ['as' => 'statementsToExcel', 'uses' => 'Pasifai\Ofa\Controllers\AdminOfaController@statementsToExcel']);
            Route::get('Δηλώσεις-Συμμετοχής-Σε-Excel-Γυμνάσια', ['as' => 'statementsToExcelPrimary', 'uses' => 'Pasifai\Ofa\Controllers\AdminOfaController@statementsToExcelPrimary']);
            Route::get('Προβολή-Δηλώσεων-Συμμετοχής', ['as' => 'statementsView', 'uses' => 'Pasifai\Ofa\Controllers\AdminOfaController@statementsView']);//statementsPermissions
            Route::get('Διαχείριση-Δηλώσεων-Συμμετοχής', ['as' => 'statementsPermissions', 'uses' => 'Pasifai\Ofa\Controllers\AdminOfaController@statementsPermissions']);//statementsPermissions
            Route::get('change-access-to-secondary-schools', ['as' => 'changeAccessSecondary', 'uses' => 'Pasifai\Ofa\Controllers\AdminOfaController@changeAccessSecondary']);//statementsPermissions
            Route::get('change-access-to-primary-schools', ['as' => 'changeAccessPrimary', 'uses' => 'Pasifai\Ofa\Controllers\AdminOfaController@changeAccessPrimary']);//statementsPermissions

            Route::get('Διαχείριση-Λίστας', ['as' => 'listsPermissions', 'uses' => 'Pasifai\Ofa\Controllers\AdminOfaController@listsPermissions']);//statementsPermissions
            Route::get('change-access-to-secondary-schools-lists', ['as' => 'changeAccessSecondaryLists', 'uses' => 'Pasifai\Ofa\Controllers\AdminOfaController@changeAccessSecondaryLists']);//statementsPermissions
            Route::get('change-access-to-primary-schools-lists', ['as' => 'changeAccessPrimaryLists', 'uses' => 'Pasifai\Ofa\Controllers\AdminOfaController@changeAccessPrimaryLists']);//statementsPermissions

            Route::get('Statistics', 'Pasifai\Ofa\Controllers\AdminOfaController@statistics')->name('statistics');
            Route::get('Λίστες-Συμμετοχής', 'Pasifai\Ofa\Controllers\AdminOfaController@getLists')->name('adminShowLists');
            Route::get('Λίστες-Συμμετοχής-Συνολικά', 'Pasifai\Ofa\Controllers\AdminOfaController@getGlobalLists')->name('adminShowGlobalLists');
            Route::get('Λίστα-Αγώνων/{token}', 'Pasifai\Ofa\Controllers\AdminOfaController@showList')->name('showListAdmin');
            Route::get('Κατάσταση-Συμμετοχής/{token}', 'Pasifai\Ofa\Controllers\AdminOfaController@showParticipation')->name('showParticipationAdmin');
            Route::get('downloadList/{year_name}/{sport_name}/{gender}/{school_name}/{school_type}', 'Pasifai\Ofa\Controllers\AdminOfaController@downloadOfaPdfList')->name('downloadPdfListAdmin');
            Route::post('aj/ofa/ParticipationStatus/pdfStream/{school_id}', 'Pasifai\Ofa\Controllers\AdminOfaController@pdfParticipationStatus')->name('pdfParticipationStatusAdmin');


            Route::get('toggleLock/{list_id}', 'Pasifai\Ofa\Controllers\AdminOfaController@toggleLock')->name('toggleLock');

            Route::get('for/{forSchool}', 'Pasifai\Ofa\Controllers\AdminOfaController@showLists')->name('showLists');
        });

        Route::group(['prefix'=>'primary', 'as'=>'Primary::'], function() {
            Route::get('downloadList/{year_name}/{sport_name}/{gender}', 'Pasifai\Ofa\Controllers\SchoolOfaPrimaryController@downloadOfaPdfList')->name('downloadPdfList');
        });

        Route::get('Λίστα-Αγώνων/{token}', 'Pasifai\Ofa\Controllers\SchoolOfaController@showList')->name('showList');
        Route::get('Αρχείο-Αθλοπαιδεία/{token}', 'Pasifai\Ofa\Controllers\SchoolOfaPrimaryController@showListPrimary')->name('showListPrimary');
        Route::get('Κατάσταση-Συμμετοχής/{token}', 'Pasifai\Ofa\Controllers\SchoolOfaController@showParticipation')->name('showParticipation');

        Route::get('downloadList/{year_name}/{sport_name}/{gender}', 'Pasifai\Ofa\Controllers\SchoolOfaController@downloadOfaPdfList')->name('downloadPdfList');

    });

    Route::post('aj/ofa/insertNewList', 'Pasifai\Ofa\Controllers\SchoolOfaController@insertNewList')->name('insertNewList');

    Route::post('aj/ofa/insertNewListIndividual', 'Pasifai\Ofa\Controllers\SchoolOfaController@insertNewListIndividual')->name('insertNewListIndividual');
    Route::post('aj/ofa/insertNewListIndividualPrimary', 'Pasifai\Ofa\Controllers\SchoolOfaPrimaryController@insertNewListIndividual')->name('insertNewListIndividualPrimary');

    Route::post('aj/ofa/temporarySaveNewList', 'Pasifai\Ofa\Controllers\SchoolOfaController@temporarySaveNewList')->name('temporarySaveNewList');

    Route::post('aj/ofa/temporarySaveNewListIndividual', 'Pasifai\Ofa\Controllers\SchoolOfaController@temporarySaveNewListIndividual')->name('temporarySaveNewListIndividual');
    Route::post('aj/ofa/temporarySaveNewListIndividualPrimary', 'Pasifai\Ofa\Controllers\SchoolOfaPrimaryController@temporarySaveNewListIndividual')->name('temporarySaveNewListIndividualPrimary');


    Route::get('aj/ofa/fetchDataForParticipationStatus', 'Pasifai\Ofa\Controllers\SchoolOfaParticipationsController@fetchDataForParticipationStatus')->name('fetchDataForParticipationStatus');
    Route::post('aj/ofa/fetchStudentsFromParticipationStatus', 'Pasifai\Ofa\Controllers\SchoolOfaParticipationsController@fetchStudentsFromParticipationStatus')->name('fetchStudentsFromParticipationStatus');
    Route::post('aj/ofa/checkIfListAlreadyExistsForParticipationStatus', 'Pasifai\Ofa\Controllers\SchoolOfaParticipationsController@checkIfListAlreadyExistsForParticipationStatus')->name('checkIfListAlreadyExistsForParticipationStatus');
    Route::post('aj/ofa/RemoveFromStatus', 'Pasifai\Ofa\Controllers\SchoolOfaParticipationsController@RemoveFromStatus')->name('RemoveFromStatus');
    Route::post('aj/ofa/AddToStudents', 'Pasifai\Ofa\Controllers\SchoolOfaParticipationsController@AddToStudents')->name('AddToStudents');
    Route::post('aj/ofa/ParticipationStatus/pdfStream/{school_id}', 'Pasifai\Ofa\Controllers\SchoolOfaParticipationsController@pdfParticipationStatus')->name('pdfParticipationStatus');

//Primary
    Route::get('aj/ofa/primary/fetchDataForSportEducation', 'Pasifai\Ofa\Controllers\SchoolOfaPrimaryController@fetchDataForSportEducation')->name('fetchDataForSportEducation');
    Route::post('aj/ofa/primary/checkIfSportEducationAlreadyExists', 'Pasifai\Ofa\Controllers\SchoolOfaPrimaryController@checkIfSportEducationAlreadyExists')->name('checkIfSportEducationAlreadyExists');
    Route::post('aj/ofa/primary/insertNewStudent', 'Pasifai\Ofa\Controllers\SchoolOfaPrimaryController@insertNewStudent')->name('insertNewStudent');
    Route::post('aj/ofa/primary/insertNewList', 'Pasifai\Ofa\Controllers\SchoolOfaPrimaryController@insertNewList')->name('insertNewList');
    Route::post('aj/ofa/primary/temporarySaveNewList', 'Pasifai\Ofa\Controllers\SchoolOfaPrimaryController@temporarySaveNewList')->name('temporarySaveNewList');
    Route::get('downloadOfaPdfSportEducation/primary/{year_name}/{sport_name}/{gender}', 'Pasifai\Ofa\Controllers\SchoolOfaPrimaryController@downloadOfaPdfSportEducation')->name('downloadPdfSportEducation');

});


