<?php

namespace Pasifai\Ofa\Seeders;

use Illuminate\Database\Seeder;
class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        // $this->call(YearsSeeder::class);
        // $this->call(OfaSportsSeeder::class);
        $this->call(OfaSpecialSportsSeeder::class);
    }
}
