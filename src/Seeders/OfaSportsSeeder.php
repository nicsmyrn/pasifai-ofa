<?php

namespace Pasifai\Ofa\Seeders;

use Illuminate\Database\Seeder;


class OfaSportsSeeder extends Seeder
{
    private $sports = array (
                0 =>
                    array (
                        'id' => 1,
                        'name' => 'ΚΑΛΑΘΟΣΦΑΙΡΙΣΗ',
                        'individual' => 0,
                        'list_limit' => 18,
                        'deleted_at' => NULL,
                        'identifier' => '1α',
                        'created_at' => '2017-12-09 20:18:29',
                        'updated_at' => '2017-12-09 20:18:29',
                    ),
                1 =>
                    array (
                        'id' => 2,
                        'name' => 'ΠΟΔΟΣΦΑΙΡΟ',
                        'individual' => 0,
                        'list_limit' => 24,
                        'deleted_at' => NULL,
                        'identifier' => '1β',
                        'created_at' => '2017-12-09 20:18:57',
                        'updated_at' => '2017-12-09 20:18:57',
                    ),
                2 =>
                    array (
                        'id' => 3,
                        'name' => 'ΠΕΤΟΣΦΑΙΡΙΣΗ',
                        'individual' => 0,
                        'list_limit' => 20,
                        'deleted_at' => NULL,
                        'identifier' => '1γ',
                        'created_at' => '2017-12-09 20:19:33',
                        'updated_at' => '2017-12-09 20:19:33',
                    ),
                3 =>
                    array (
                        'id' => 4,
                        'name' => 'ΣΤΙΒΟΣ',
                        'individual' => 1,
                        'list_limit' => 50,
                        'deleted_at' => NULL,
                        'identifier' => '2',
                        'created_at' => '2017-12-12 00:00:00',
                        'updated_at' => '2017-12-12 00:00:00',
                    ),
                4 =>
                    array (
                        'id' => 5,
                        'name' => 'ΧΕΙΡΟΣΦΑΙΡΙΣΗ',
                        'individual' => 0,
                        'list_limit' => 22,
                        'deleted_at' => NULL,
                        'identifier' => '1δ',
                        'created_at' => '2017-12-12 00:00:00',
                        'updated_at' => '2017-12-12 00:00:00',
                    )
    );


    public function run()
    {
        \DB::table('ofa_sports')->insert($this->sports);
    }
}
