<?php

namespace Pasifai\Ofa\Seeders;

use Illuminate\Database\Seeder;


class OfaSpecialSportsSeeder extends Seeder
{
    private $specialSports = array (
                0 =>
                    array (
                        'id' => 1,
                        'name' => '100 μ.',
                        'sport_id' => 4,
                        'deleted_at' => NULL,
                        'created_at' => '2017-12-09 20:20:28',
                        'updated_at' => '2017-12-09 20:20:28',
                    ),
                1 =>
                    array (
                        'id' => 2,
                        'name' => '200 μ.',
                        'sport_id' => 4,
                        'deleted_at' => NULL,
                        'created_at' => '2017-12-09 20:20:43',
                        'updated_at' => '2017-12-09 20:20:43',
                    ),
                2 =>
                    array (
                        'id' => 3,
                        'name' => '400 μ.',
                        'sport_id' => 4,
                        'deleted_at' => NULL,
                        'created_at' => '2017-12-09 20:20:47',
                        'updated_at' => '2017-12-09 20:20:47',
                    ),
                3 =>
                    array (
                        'id' => 4,
                        'name' => '800 μ.',
                        'sport_id' => 4,
                        'deleted_at' => NULL,
                        'created_at' => '2017-12-09 20:20:51',
                        'updated_at' => '2017-12-09 20:20:51',
                    ),
                4 =>
                    array (
                        'id' => 5,
                        'name' => '1.500 μ.',
                        'sport_id' => 4,
                        'deleted_at' => NULL,
                        'created_at' => '2017-12-09 20:21:01',
                        'updated_at' => '2017-12-09 20:21:01',
                    ),
                5 =>
                    array (
                        'id' => 6,
                        'name' => '3.000 μ.',
                        'sport_id' => 4,
                        'deleted_at' => NULL,
                        'created_at' => '2018-01-06 00:00:00',
                        'updated_at' => '2018-01-06 00:00:00',
                    ),
                6 =>
                    array (
                        'id' => 7,
                        'name' => '100 μ. εμπόδια',
                        'sport_id' => 4,
                        'deleted_at' => NULL,
                        'created_at' => '2018-01-06 00:00:00',
                        'updated_at' => '2018-01-06 00:00:00',
                    ),
                7 =>
                    array (
                        'id' => 8,
                        'name' => '110 μ. εμπόδια',
                        'sport_id' => 4,
                        'deleted_at' => NULL,
                        'created_at' => '2018-01-06 00:00:00',
                        'updated_at' => '2018-01-06 00:00:00',
                    ),
                8 =>
                    array (
                        'id' => 9,
                        'name' => '400 μ. εμπόδια',
                        'sport_id' => 4,
                        'deleted_at' => NULL,
                        'created_at' => '2018-01-06 00:00:00',
                        'updated_at' => '2018-01-06 00:00:00',
                    ),
                9 =>
                    array (
                        'id' => 10,
                        'name' => '2.000 μ. φυσ. εμπόδια',
                        'sport_id' => 4,
                        'deleted_at' => NULL,
                        'created_at' => '2018-01-06 00:00:00',
                        'updated_at' => '2018-01-06 00:00:00',
                    ),
                10 =>
                    array (
                        'id' => 11,
                        'name' => '5.000 μ. βάδην',
                        'sport_id' => 4,
                        'deleted_at' => NULL,
                        'created_at' => '2018-01-06 00:00:00',
                        'updated_at' => '2018-01-06 00:00:00',
                    ),
                11 =>
                    array (
                        'id' => 12,
                        'name' => '10.000 μ. βάδης',
                        'sport_id' => 4,
                        'deleted_at' => NULL,
                        'created_at' => '2018-01-06 00:00:00',
                        'updated_at' => '2018-01-06 00:00:00',
                    ),
                12 =>
                    array (
                        'id' => 13,
                        'name' => 'Μήκος',
                        'sport_id' => 4,
                        'deleted_at' => NULL,
                        'created_at' => '2018-01-06 00:00:00',
                        'updated_at' => '2018-01-06 00:00:00',
                    ),
                13 =>
                    array (
                        'id' => 14,
                        'name' => 'Τριπλούν',
                        'sport_id' => 4,
                        'deleted_at' => NULL,
                        'created_at' => '2018-01-06 00:00:00',
                        'updated_at' => '2018-01-06 00:00:00',
                    ),
                14 =>
                    array (
                        'id' => 15,
                        'name' => 'Ύψος',
                        'sport_id' => 4,
                        'deleted_at' => NULL,
                        'created_at' => '2018-01-06 00:00:00',
                        'updated_at' => '2018-01-06 00:00:00',
                    ),
                15 =>
                    array (
                        'id' => 16,
                        'name' => 'Επί κοντώ',
                        'sport_id' => 4,
                        'deleted_at' => NULL,
                        'created_at' => '2018-01-06 00:00:00',
                        'updated_at' => '2018-01-06 00:00:00',
                    ),
                16 =>
                    array (
                        'id' => 17,
                        'name' => 'Σφαίρα',
                        'sport_id' => 4,
                        'deleted_at' => NULL,
                        'created_at' => '2018-01-06 00:00:00',
                        'updated_at' => '2018-01-06 00:00:00',
                    ),
                17 =>
                    array (
                        'id' => 18,
                        'name' => 'Δίσκος',
                        'sport_id' => 4,
                        'deleted_at' => NULL,
                        'created_at' => '2018-01-06 00:00:00',
                        'updated_at' => '2018-01-06 00:00:00',
                    ),
                18 =>
                    array (
                        'id' => 19,
                        'name' => 'Ακόντιο',
                        'sport_id' => 4,
                        'deleted_at' => NULL,
                        'created_at' => '2018-01-06 00:00:00',
                        'updated_at' => '2018-01-06 00:00:00',
                    ),
                19 =>
                    array (
                        'id' => 20,
                        'name' => 'Σφύρα',
                        'sport_id' => 4,
                        'deleted_at' => NULL,
                        'created_at' => '2018-01-06 00:00:00',
                        'updated_at' => '2018-01-06 00:00:00',
                    ),
                20 =>
                    array (
                        'id' => 21,
                        'name' => 'Έπταθλο',
                        'sport_id' => 4,
                        'deleted_at' => NULL,
                        'created_at' => '2018-01-06 00:00:00',
                        'updated_at' => '2018-01-06 00:00:00',
                    ),
                21 =>
                    array (
                        'id' => 22,
                        'name' => 'Δέκαθλο',
                        'sport_id' => 4,
                        'deleted_at' => NULL,
                        'created_at' => '2018-01-06 00:00:00',
                        'updated_at' => '2018-01-06 00:00:00',
                    ),
                22 =>
                    array (
                        'id' => 23,
                        'name' => 'Όκταθλο Γ/σιο',
                        'sport_id' => 4,
                        'deleted_at' => NULL,
                        'created_at' => '2018-03-07 00:00:00',
                        'updated_at' => '2018-03-07 00:00:00',
                    ),
                23 =>
                    array (
                        'id' => 24,
                        'name' => '80 μ. Γ/σιο',
                        'sport_id' => 4,
                        'deleted_at' => NULL,
                        'created_at' => '2018-03-07 00:00:00',
                        'updated_at' => '2018-03-07 00:00:00',
                    ),
                24 =>
                    array (
                        'id' => 25,
                        'name' => '150 μ. Γ/σιο',
                        'sport_id' => 4,
                        'deleted_at' => NULL,
                        'created_at' => '2018-03-07 00:00:00',
                        'updated_at' => '2018-03-07 00:00:00',
                    ),
                25 =>
                    array (
                        'id' => 26,
                        'name' => '300 μ. Γ/σιο',
                        'sport_id' => 4,
                        'deleted_at' => NULL,
                        'created_at' => '2018-03-07 00:00:00',
                        'updated_at' => '2018-03-07 00:00:00',
                    ),
                26 =>
                    array (
                        'id' => 27,
                        'name' => '600 μ. Γ/σιο',
                        'sport_id' => 4,
                        'deleted_at' => NULL,
                        'created_at' => '2018-03-07 00:00:00',
                        'updated_at' => '2018-03-07 00:00:00',
                    ),
                27 =>
                    array (
                        'id' => 28,
                        'name' => '1000 μ.  Γ/σιο',
                        'sport_id' => 4,
                        'deleted_at' => NULL,
                        'created_at' => '2018-03-07 00:00:00',
                        'updated_at' => '2018-03-07 00:00:00',
                    ),
                28 =>
                    array (
                        'id' => 29,
                        'name' => '2000 μ. Γ/σιο',
                        'sport_id' => 4,
                        'deleted_at' => NULL,
                        'created_at' => '2018-03-07 00:00:00',
                        'updated_at' => '2018-03-07 00:00:00',
                    ),
                29 =>
                    array (
                        'id' => 30,
                        'name' => '3000 μ. βάδην Γ/σιο',
                        'sport_id' => 4,
                        'deleted_at' => NULL,
                        'created_at' => '2018-03-07 00:00:00',
                        'updated_at' => '2018-03-07 00:00:00',
                    ),
                30 =>
                    array (
                        'id' => 31,
                        'name' => '80 μ. εμπόδια κορίτσια Γ/σιου',
                        'sport_id' => 4,
                        'deleted_at' => NULL,
                        'created_at' => '2018-03-07 00:00:00',
                        'updated_at' => '2018-03-07 00:00:00',
                    ),
                31 =>
                    array (
                        'id' => 32,
                        'name' => '100 μ. εμπόδια αγόρια Γ/σιου',
                        'sport_id' => 4,
                        'deleted_at' => NULL,
                        'created_at' => '2018-03-07 00:00:00',
                        'updated_at' => '2018-03-07 00:00:00',
                    ),
                32 =>
                    array (
                        'id' => 33,
                        'name' => '300 μ. εμπόδια Γ/σιο',
                        'sport_id' => 4,
                        'deleted_at' => NULL,
                        'created_at' => '2018-03-07 00:00:00',
                        'updated_at' => '2018-03-07 00:00:00',
                    ),
                33 =>
                    array (
                        'id' => 34,
                        'name' => 'Πένταθλο κορίτσια Γ/σιου',
                        'sport_id' => 4,
                        'deleted_at' => NULL,
                        'created_at' => '2018-03-07 00:00:00',
                        'updated_at' => '2018-03-07 00:00:00',
                    ),
                34 =>
                    array (
                        'id' => 35,
                        'name' => 'Έξαθλο αγόρια Γ/σιου',
                        'sport_id' => 4,
                        'deleted_at' => NULL,
                        'created_at' => '2018-03-07 00:00:00',
                        'updated_at' => '2018-03-07 00:00:00',
                    ),
    );


    public function run()
    {
        \DB::table('ofa_special_sports')->insert($this->specialSports);
    }
}
