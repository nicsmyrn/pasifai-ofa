<?php

namespace Pasifai\Ofa\Seeders;

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\Models\Year;

class YearsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $current_year = Carbon::now()->year;

        $next_year = $current_year + 1;

        $name = $current_year . '-' . $next_year;

        Year::create([
            'name'  => $name,
            'current'   => true
        ]);
    }
}
