<?php

namespace Pasifai\Ofa\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\User;

class Covid19VaccineDelete extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    protected $user;
    protected $data;

    public function __construct(User $user, $data = [])
    {
        $this->user = $user;
        $this->data = $data;
    }

    public function build()
    {
        return $this->to(config()->get('requests.MAIL_PYSDE'), config()->get('requests.NAME_PYSDE'))
            ->cc($this->data['email'])
            ->subject('[Εμβολιασμός ΔΙΑΓΡΑΦΗ Covid19] από '. $this->data['fullname'])
            ->markdown('ofa::emails.covid19delete')
            ->with([
                'fullname' => $this->data['fullname'],
                'email' => $this->data['email'],
                'amka'  => $this->data['amka']
            ]);
    }
}
