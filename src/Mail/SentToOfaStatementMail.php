<?php

namespace Pasifai\Ofa\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SentToOfaStatementMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $user;
    protected $sports;
    protected $gender;
    protected $records;

    public function __construct(User $user, $sports, $gender, $request)
    {
        $this->user = $user;
        $this->sports = $sports;
        $this->gender = $gender;
        $this->records = $request->input('sport');
    }


    public function build()
    {
        return $this->from($this->user->sch_mail, 'Πασιφάη')
            ->to(config()->get('requests.MAIL_OFA'), config()->get('requests.NAME_OFA'))
            ->cc($this->user->sch_mail)
            ->subject('['.$this->user->userable->name.'] Δήλωση Συμμετοχής σε Αγώνες')
            ->markdown('ofa::emails.statements')
            ->with([
                'sports'    => $this->sports,
                'gender'    => $this->gender,
                'records'   => $this->records,
                'school'    => $this->user->userable->name
            ]);
    }
}
