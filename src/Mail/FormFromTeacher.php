<?php

namespace Pasifai\Ofa\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\User;

class FormFromTeacher extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    protected $attachment;
    protected $user;
    protected $data;

    public function __construct(User $user, $data = [])
    {
        $this->user = $user;
        $this->data = $data;
    }

    public function build()
    {
        return $this->to(config()->get('requests.MAIL_OFA'), config()->get('requests.NAME_OFA'))
            ->cc($this->data['email'])
            ->subject('[ΔΩΣΕ ΠΑΣΑ] από '. $this->data['fullname'])
            ->markdown('ofa::emails.form')
            ->with([
                'fullname' => $this->data['fullname'],
                'email' => $this->data['email'],
                'phone' => $this->data['phone'],
                'subject' => $this->data['subject'],
                'message' => $this->data['message']
            ]);
    }
}
