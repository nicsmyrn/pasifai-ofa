<?php

namespace Pasifai\Ofa\Mail;

use App\School;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\User;

class SportEducationFromSchool extends Mailable
{
    use Queueable, SerializesModels;

    protected $attachment;
    protected $user;
    protected $data;

    public function __construct(User $user, $attachement, $data)
    {
        $this->user = $user;
        $this->attachment = $attachement;
        $this->data = $data;
    }

    public function build()
    {
        if($this->data['gender'] == 0){
            $gender = 'ΚΟΡΙΤΣΙΩΝ';
        }elseif($this->data['gender'] == 1){
            $gender = 'ΑΓΟΡΙΩΝ';
        }else{
            $gender = 'ΜΙΚΤΗ';
        }

        return $this->attach($this->attachment)
            ->from($this->user->email, 'ΠΑΣΙΦΑΗ')
            ->to(config()->get('requests.MAIL_OFA'), config()->get('requests.NAME_OFA'))
            ->subject('Αθλοπαιδεία - '.$this->user->userable->name)
            ->markdown('ofa::emails.sport-education-created')
            ->with([
                'school_name' => $this->user->userable->name,
                'sport_name'  => $this->data['sport']['name'],
                'gender'        => $gender
            ]);
    }
}
