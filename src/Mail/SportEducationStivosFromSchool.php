<?php

namespace Pasifai\Ofa\Mail;

use App\School;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\User;

class SportEducationStivosFromSchool extends Mailable
{
    use Queueable, SerializesModels;

    protected $attachment;
    protected $user;
    protected $data;

    public function __construct(User $user, $attachements, $data)
    {
        $this->user = $user;
        $this->file_attachments = $attachements;
        $this->data = $data;
    }

    public function build()
    {
        if($this->data['gender'] == 0){
            $gender = 'ΚΟΡΙΤΣΙΩΝ';
        }elseif($this->data['gender'] == 1){
            $gender = 'ΑΓΟΡΙΩΝ';
        }else{
            $gender = 'ΜΙΚΤΗ';
        }

        $message = $this->markdown('ofa::emails.sport-education-created')
            ->from($this->user->email, 'ΠΑΣΙΦΑΗ')
            ->to(config()->get('requests.MAIL_OFA'), config()->get('requests.NAME_OFA'))
            ->with([
                'school_name' => $this->user->userable->name,
                'sport_name'  => $this->data['sport']['name'],
                'gender'        => $gender
            ])
            ->subject('Αθλοπαιδεία Στίβος');


        if(count($this->file_attachments) > 0){
            foreach($this->file_attachments as $file){
                $message->attach($file);
            }
        }

        return $message;
    }
}
