window.axios = require('axios');

window.Vue = require('vue');

window.Event = new Vue();

import Swalify from './Classes/Swalify';

window.Swalify = Swalify;

new Vue({
    el: '#app',

    data : {
        token : window.api_token,
        base_url : window.base_url,
        student_url : window.student_url,
        students : [],
        loading : true,
        dataDelete : new Swalify({
            title : 'Προσοχή!!',
            type : 'error',
            text : 'Ο μαθητής θα διαγραφεί οριστικά',
            buttonText : 'Ναι, διαγράψτε τον',
            success_title : 'Συγχαρητήρια!',
            success_text  : 'ο μαθητής διεγράφη'
        })
    },

    mounted ()  {
        this.fetchStudents();
    },

    methods : {
        fetchStudents (){
            axios.get('/api/ofa/school/allStudents?api_token='+this.token)
                .then(r => {
                    this.students = r.data;
                    this.loading = false;
                })
                .catch(e => console.log(e));
        },

        destroy(am){
            this.dataDelete.submit(`${this.base_url}/api/ofa/school/destroy/${am}?api_token=${this.token}`);
        }
    }
});