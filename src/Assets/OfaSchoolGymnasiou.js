require('./../../../../../resources/assets/js/bootstrap');

window.Vue = require('vue');

window.Event = new Vue();

import ofaMixin from './../assets/Mixins/ofaMixin.js';

new Vue ({
    el : '#app',

    mixins : [ofaMixin],

    data : {

        nullValue : null,
        sex : [
            'Κορίτσια', 'Αγόρια', 'Μικτή Ομάδα'
        ]
    },

    computed : {
        computedRoute () {
            return this.current_sport.individual ? 'insertNewListIndividualPrimary' : 'insertNewParticipationPrimary';
        }
    },

    mounted(){    
        let route = 'fetchDataForSportEducation';

        this.fetchDataFromDatabase(false, route);
    },

    methods : {

        temporarySaveIndividual(){
            this.hideLoader = false;

            this.$http.post('/aj/ofa/temporarySaveNewListIndividualPrimary',{
                current_sport : this.current_sport,
                current_sex : 3,                       //individual sex not exists
                studentsListIndividual : this.studentsListIndividual,
                year : this.dataSet.year,
                synodos : this.synodos,
                teacher_name : this.teacher_name,
                existedListId : this.existedListId
            })
                .then(r => {
                    this.existedListId = r.data;
                    this.displayAlert('Συγχαρητήρια!', 'Η Κατάσταση Συμμετοχής για τα ατομικά αθλήματα αποθηκεύτηκε με επιτυχία', 'success');
                    this.hideLoader = true;
                })
                .catch(r => {
                    this.hideLoader = true;
                    this.displayAlert('Προσοχή ΣΦΑΛΜΑ - 150.245', 'Παρακαλώ επικοινωνήστε με τον διαχειριστή του συστήματος (Σμυρναίο Νικόλαο) και αναφέρετε τον κωδικό Σφάλματος', 'danger',true)
                })
        },

        fetchDataFromDatabaseIndividual (){
            this.$http.get('/aj/ofa/fetchDataForListsIndividual')
                .then(r => {
                    this.$set('dataSet', r.data);
                    this.loading = false;
                })
                .catch(r => {
                    this.loading = false;
                    this.displayAlert('Προσοχή ΣΦΑΛΜΑ - 150.303', 'Παρακαλώ επικοινωνήστε με τον διαχειριστή του συστήματος (Σμυρναίο Νικόλαο) και αναφέρετε τον κωδικό Σφάλματος', 'danger',true)
                })
        },

        checkIfListAlreadyExistsIndividual(){
            this.existedListId = 0;

            this.$http.post('/aj/ofa/checkIfListAlreadyExistsForIndividual', {
                sport : this.current_sport,
                year_id : this.dataSet.year.id
            })
                .then(r => {
                    this.individualSports = r.data["special_sports"].sort(this.sortSports);

                    this.individualSports = r.data["special_sports"];

                    if(r.data["response"] == 'canMakeSchoolListIndividual'){
                        if(this.synodos == ''){
                            this.displayAlert('Σημείωση:', 'συμπληρώστε παρακαλώ τον συνοδό του Σχολείου σας', 'warning');
                        }
                        this.notExistsPdfFileIndividual = true;

                    }else if(r.data["response"] == 'isLockedIndividual'){
                        this.synodos = r.data["synodos"];
                        this.current_sex = 3;
                        this.notExistsPdfFileIndividual = false;
                    }else{
                        if(r.data["response"] == 'temporaryListExistsIndividual'){
                            this.synodos = r.data["synodos"];
                            let existedStudents = r.data["list"];
                            this.notExistsPdfFileIndividual = true;
                            this.existedListId = r.data["list_id"];
                            let list_details = r.data['studentListIndividual'];
                            var i;
                            for (i = this.allStudentsIndividual.length - 1; i >= 0; i -= 1) {
                                if (existedStudents.includes(this.allStudentsIndividual[i]["am"])) {
                                    this.studentsListIndividual.push({
                                        am : this.allStudentsIndividual[i]["am"],
                                        birth : this.allStudentsIndividual[i]["birth"],
                                        class : this.allStudentsIndividual[i]["class"],
                                        first_name : this.allStudentsIndividual[i]["first_name"],
                                        last_name : this.allStudentsIndividual[i]["last_name"],
                                        loader : 0,
                                        middle_name : this.allStudentsIndividual[i]["middle_name"],
                                        mothers_name : this.allStudentsIndividual[i]["mothers_name"],
                                        school_id : this.allStudentsIndividual[i]["school_id"],
                                        sex : this.allStudentsIndividual[i]["sex"],
                                        year_birth : this.allStudentsIndividual[i]["year_birth"],
                                        sport_id_special : list_details.find(function(obj){
                                            return obj.student_id === this.allStudentsIndividual[i]["am"]
                                        })["sport_id_special"],
                                        sport_name_special : list_details.find(function(obj){
                                            return obj.student_id === this.allStudentsIndividual[i]["am"]
                                        })["special_name"]
                                    });
                                    this.allStudentsIndividual.splice(i, 1);
                                }
                            }

                            this.studentsListIndividual.sort(this.sortStudentsBySpecialName);

                        }
                    }
                    this.loadingForExistenceIndividual = false;
                    this.loadingForExistence = false;
                })
                .catch(r => this.displayAlert('Προσοχή ΣΦΑΛΜΑ - 150.782', 'Παρακαλώ επικοινωνήστε με τον διαχειριστή του συστήματος (Σμυρναίο Νικόλαο) και αναφέρετε τον κωδικό Σφάλματος', 'danger',true))

        },

        // sentRequestIndividual (){

        //     this.hideLoader = false;

        //     this.studentsListIndividual.sort(this.sortStudentsBySpecialSport);

        //     this.$http.post('/aj/ofa/insertNewListIndividualPrimary',{
        //         current_sport : this.current_sport,
        //         current_sex : 3,
        //         studentsList : this.studentsListIndividual,
        //         year : this.dataSet.year,
        //         teacher_name : this.teacher_name,
        //         synodos      : this.synodos,
        //         existedListId : this.existedListId
        //     })
        //         .then(r => {
        //             location.href = r.data;
        //             //console.log(r.data);
        //         })
        //         .catch(r => this.displayAlert('Προσοχή ΣΦΑΛΜΑ - 150.999', 'Παρακαλώ επικοινωνήστε με τον διαχειριστή του συστήματος (Σμυρναίο Νικόλαο) και αναφέρετε τον κωδικό Σφάλματος', 'danger',true))
        // },

        // temporarySave (data){
        //     this.hideLoader = false;

        //     let api = '';
        //     let message = [];
        //     if(!data.individual){
        //         api = '/api/ofa/school/temporarySaveNewList?api_token='+this.token;
        //         message = ['Η Κατάσταση Συμμετοχής αποθηκεύτηκε με επιτυχία'];
        //     }else{
        //         api = '/api/ofa/school/temporarySaveNewList?api_token='+this.token;
        //         message = ['Η Κατάσταση Συμμετοχής για ατομικά αθλήματα αποθηκεύτηκε με επιτυχία'];
        //     }

        //     axios.post(api, {
        //         current_sport : this.current_sport,
        //         current_sex : this.current_sex,
        //         studentsList : this.studentsList,
        //         year : this.dataSet.year,
        //         teacher_name : this.teacher_name,
        //         synodos : this.synodos,
        //         existedListId : this.existedListId
        //     })
        //     .then(r => {
        //         this.existedListId = r.data;
        //         this.displayAlert('Συγχαρητήρια!', message, 'success');
        //         this.hideLoader = true;
        //     })
        //     .catch(r => {
        //         this.hideLoader = true;
        //         this.displayAlert('Προσοχή ΣΦΑΛΜΑ - 150.133', 'Παρακαλώ επικοινωνήστε με τον διαχειριστή του συστήματος (Σμυρναίο Νικόλαο) και αναφέρετε τον κωδικό Σφάλματος', 'danger',true)
        //     })
        // },

        // fetchDataFromDatabase (){
        //     axios.get('/api/ofa/school/fetchDataForSportEducation?api_token='+this.token)
        //         .then(r => {
        //             this.dataSet = r.data;
        //             this.loading = false;
        //         })
        //         .catch(e => console.log(e.response)) 
        // },

        fetchAllStudentsByGender (){
            this.loadingForExistence = true;
            this.studentsList = [];
            if(this.current_sex !== 2){                                                  //case current sex is mixed
                this.allStudentsByGender = this.dataSet.students.filter(student => {
                    if (student.sex === this.current_sex) return student;
                });
            }else{
                this.allStudentsByGender = this.dataSet.students;
            }

            this.checkIfSportEducationAlreadyExists();
        
        },

        // fetchAllStudentsForIndividualSport(){
        //     this.loadingForExistenceIndividual = true;
        //     this.studentsListIndividual = [];
        //     this.studentsList = [];
        //     this.allStudentsIndividual = this.dataSet.students;

        //     console.log('run stivos game');

        //     this.checkIfListAlreadyExistsIndividualPrimary();

        // },

        // checkIfListAlreadyExistsIndividualPrimary(){
        //     this.existedListId = 0;

        //     this.$http.post('/aj/ofa/checkIfListAlreadyExistsForIndividualPrimary', {
        //         sport : this.current_sport,
        //         year_id : this.dataSet.year.id
        //     })
        //         .then(r => {
        //             console.log(r.data);
        //             this.individualSports = r.data["special_sports"];

        //             if(r.data["response"] == 'canMakeSchoolPrimaryIndividual'){
        //                 if(this.synodos == ''){
        //                     this.displayAlert('Σημείωση:', 'συμπληρώστε παρακαλώ τον συνοδό και τον γυμναστή του Σχολείου', 'warning');
        //                 }
        //                 this.notExistsPdfFileIndividual = true;

        //             }else if(r.data["response"] == 'isLockedPrimaryIndividual'){
        //                 console.log('synodos loaded');
        //                 this.synodos = r.data["synodos"];
        //                 this.current_sex = 3;
        //                 this.notExistsPdfFileIndividual = false;
        //             }else{
        //                 if(r.data["response"] == 'temporaryPrimaryExistsIndividual'){
        //                     this.synodos = r.data["synodos"];
        //                     this.teacher_name = r.data["teacher_name"];
        //                     let existedStudents = r.data["list"];
        //                     this.notExistsPdfFileIndividual = true;
        //                     this.existedListId = r.data["list_id"];
        //                     let list_details = r.data['studentListIndividual'];
        //                     var i;
        //                     for (i = this.allStudentsIndividual.length - 1; i >= 0; i -= 1) {
        //                         if (existedStudents.includes(this.allStudentsIndividual[i]["am"])) {
        //                             this.studentsListIndividual.push({
        //                                 am : this.allStudentsIndividual[i]["am"],
        //                                 birth : this.allStudentsIndividual[i]["birth"],
        //                                 class : this.allStudentsIndividual[i]["class"],
        //                                 first_name : this.allStudentsIndividual[i]["first_name"],
        //                                 last_name : this.allStudentsIndividual[i]["last_name"],
        //                                 loader : 0,
        //                                 middle_name : this.allStudentsIndividual[i]["middle_name"],
        //                                 mothers_name : this.allStudentsIndividual[i]["mothers_name"],
        //                                 school_id : this.allStudentsIndividual[i]["school_id"],
        //                                 sex : this.allStudentsIndividual[i]["sex"],
        //                                 year_birth : this.allStudentsIndividual[i]["year_birth"],
        //                                 sport_id_special : list_details.find(function(obj){
        //                                     return obj.student_id === this.allStudentsIndividual[i]["am"]
        //                                 })["sport_id_special"],
        //                                 sport_name_special : list_details.find(function(obj){
        //                                     return obj.student_id === this.allStudentsIndividual[i]["am"]
        //                                 })["special_name"]
        //                             });
        //                             this.allStudentsIndividual.splice(i, 1);
        //                         }
        //                     }
        //                 }
        //             }
        //             this.loadingForExistenceIndividual = false;
        //             this.loadingForExistence = false;
        //         })
        //         .catch(r => this.displayAlert('Προσοχή ΣΦΑΛΜΑ - 150.782221', 'Παρακαλώ επικοινωνήστε με τον διαχειριστή του συστήματος (Σμυρναίο Νικόλαο) και αναφέρετε τον κωδικό Σφάλματος', 'danger',true))

        // },

        checkIfSportEducationAlreadyExists(){
            this.existedListId = 0;

            axios.post('/api/ofa/school/checkIfSportEducationAlreadyExists?api_token='+this.token, {
                sport : this.current_sport,
                year_id : this.dataSet.year.id,
                gender : this.current_sex,
                year_name : this.dataSet.year.name           
            })
            .then(r => {
                if(r.data["response"] === 'canMakeSchoolSportEducation'){
                    if(this.teacher_name === ''){
                        this.displayAlert('Σημείωση:', 'συμπληρώστε παρακαλώ τον υπεύθυνο Φυσικής Αγωγής και τον Συνοδό του αγωνίσματος.', 'warning');
                    }
                    this.notExistsPdfFile = true;
                }else if(r.data["response"] === 'isLocked'){
                    this.teacher_name = r.data["teacher_name"];
                    this.synodos = r.data["synodos"];
                    this.notExistsPdfFile = false;
                    this.downloadUrl = r.data["downloadUrl"];
                }else{
                    if(r.data["response"] === 'temporarySportEducationExists'){
                        this.teacher_name = r.data["teacher_name"];
                        this.synodos = r.data["synodos"];
                        let existedStudents = r.data["list"];
                        this.notExistsPdfFile = true;
                        this.existedListId = r.data["list_id"];

                        var i;
                        for (i = this.allStudentsByGender.length - 1; i >= 0; i -= 1) {
                            if (existedStudents.includes(this.allStudentsByGender[i]["am"])) {
                                this.studentsList.push(this.allStudentsByGender[i]);
                                this.allStudentsByGender.splice(i, 1);
                            }
                        }
                    }
                }
                this.loadingForExistence = false;
            })
            .catch(e => console.log(e.response));
        },

        // sentRequest (){

        //     this.showModalAgreement = false;
        //     this.hideLoader = false;

        //     axios.post('/api/ofa/school/insertNewParticipationPrimary?api_token='+this.token, {
        //         current_sport : this.current_sport,
        //         current_sex : this.current_sex,
        //         studentsList : this.studentsList,
        //         year : this.dataSet.year,
        //         teacher_name : this.teacher_name,
        //         synodos : this.synodos,
        //         existedListId : this.existedListId
        //     })
        //         .then(r => {
        //             this.downloadUrl = r.data;
        //             this.hideLoader = true;
        //             this.notExistsPdfFile = false;
        //             this.displayAlert('Συγχαρητήρια!', ['Η Κατάσταση Συμμετοχής στάλθηκε στην Ομάδα Φυσικής Αγωγής'], 'success');
        //         })
        //         .catch(r => this.displayAlert('Προσοχή ΣΦΑΛΜΑ - 150.136', 'Παρακαλώ επικοινωνήστε με τον διαχειριστή του συστήματος (Σμυρναίο Νικόλαο) και αναφέρετε τον κωδικό Σφάλματος', 'danger',true))
        // },


        // SHARED FUNTIONS ##########################################
        // deleteStudent(data){
        //     this.allStudentsByGender.push(data.student);
        //     this.allStudentsByGender.sort(this.sortStudents);
        // },

        // deleteStudentIndividual(student){
        //     this.allStudentsIndividual.push(student);
        //     this.allStudentsIndividual.sort(this.sortStudents);
        // },

        // sortSports(a,b){
        //     if (a.name < b.name){
        //         return -1;
        //     }
        //     if (a.name > b.name){
        //         return 1;
        //     }
        // },

        // sortStudentsBySpecialName(a, b) {
        //     if (a.sport_id_special < b.sport_id_special) {
        //         return -1;
        //     }
        //     if (a.sport_id_special > b.sport_id_special) {
        //         return 1;
        //     }
        // },

        // openModal(){
        //     this.showModalAgreement = true;
        // },

        // sortStudentsBySpecialSport(a, b){
        //     if (a.sport_name_special < b.sport_name_special){
        //         return -1;
        //     }
        //     if (a.sport_name_special > b.sport_name_special){
        //         return 1;
        //     }
        // },

        // hideModalAgreement(){
        //     this.showModalAgreement = false;
        //     this.displayAlert('Ειδοποίηση', 'Η Αποστολή πραγματοποιείται μόνο συμφωνώντας με τους όρους','warning');
        // },

        // displayAlert(header, body, type, important = false){
        //     this.$refs.alert.broadcast(header, body, type, important);
        // },

        // sortStudents (a,b){
        //     if (a.last_name < b.last_name){
        //         return 1;
        //     }
        //     if (a.last_name > b.last_name){
        //         return -1;
        //     }
        // },

        // showModalNewStudentIndividual(){
        //     this.showModalStudent = true;
        //     this.current_sex = 3;
        //     console.log('must open... for Individual Sport');
        // },

        // displayGender(sport){
        //     this.current_sex = 3;
        //     if(sport.individual === 1){
        //         this.showIndividualForm = true;
        //         console.log('inside');
        //         // this.fetchAllStudentsForIndividualSport();
        //     }else{
        //         this.showIndividualForm = false;
        //     }
        // },

        // displayMessage(message){
        //     this.displayAlert(message.title, message.body, message.type);
        // },

        // downloadList(){
        //     this.displayAlert('Παρακαλώ περιμένετε...', 'Το αρχείο είναι σε διαδικασία κατεβάσματος στον υπολογιστή σας', 'danger');
        // },
    }
});
