require('./../../../../../resources/assets/js/bootstrap');

window.Vue = require('vue');

window.Event = new Vue();

import ofaMixin from './../assets/Mixins/ofaMixin.js';

new Vue ({

    el : '#app',
    
    mixins : [ofaMixin],

    data : {
        showTeacherInput : false
    },

    mounted(){      
        let route = 'fetchDataForLists';

        if(window.location.pathname === '/%CE%91%CE%98%CE%9B%CE%97%CE%A4%CE%99%CE%9A%CE%9F%CE%99-%CE%91%CE%93%CE%A9%CE%9D%CE%95%CE%A3/%CE%9B%CE%AF%CF%83%CF%84%CE%B1-%CE%9F%CE%BC%CE%B1%CE%B4%CE%B9%CE%BA%CF%8E%CE%BD-%CE%91%CE%B8%CE%BB%CE%B7%CE%BC%CE%AC%CF%84%CF%89%CE%BD'){
            this.fetchDataFromDatabase(false, route);

        }else if(window.location.pathname === '/ΑΘΛΗΤΙΚΟΙ-ΑΓΩΝΕΣ/Λίστα-Ομαδικών-Αθλημάτων'){
            this.fetchDataFromDatabase(false, route);
        }
        else{
            this.fetchDataFromDatabase(true, route);
        }    
    },

    methods: {
        fetchAllStudentsByGender (){            
            this.loadingForExistence = true;
            this.studentsList = [];
            this.allStudentsByGender = this.dataSet.students.filter(student => {
                if (student.sex === this.current_sex) return student;
            });

            this.checkIfListAlreadyExists();
        }
    }
});