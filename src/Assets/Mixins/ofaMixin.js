import Alert from './../../../../../../resources/assets/js/components/Alert.vue';//components/Alert.vue';
import ListStudentsTable from './../components/ListStudentsTable.vue';
import ModalAgreement from './../components/ModalOfaAgreement.vue';
import CreateOrSelectStudent from './../components/CreateOrSelectStudent.vue';

export default {
    components : {
        Alert, ListStudentsTable, CreateOrSelectStudent, ModalAgreement
    },

    data () {
        return {
            dataSet : {
                sports : null,
                students : null,
                year : {
                    name : null,
                    id : null
                }
            },
            current_sport : null,
            current_sex : 3,

            teacher_name : '',
            synodos : '',
            sex : [
                'Κορίτσια', 'Αγόρια'
            ],
            sport_name : null,
            selectedStudent : null,
            allStudentsByGender : [],
            studentsList : [],
            loading : true,
            showModalStudent : false,
            showModalAgreement : false,
            hideLoader : true,
            loadingForExistence : true,
            notExistsPdfFile: true,
            existedListId : 0,
            showIndividualForm : false,
            notExistsPdfFileIndividual : true,
            allStudentsIndividual : [],
            individualSports : [],
            loadingForExistenceIndividual : true,
            hideLoaderAgreement : true,
            current_url : window.current_url,
            base_url    : window.base_url,
            token   : window.api_token,
            downloadUrl : ''
        }
    },

    computed :{
        showListForm (){
            return !!((this.current_sport !== null) && (this.current_sex !== 3) && (this.dataSet.year.id !== null));
        }
    },

    methods : {
        fetchDataFromDatabase (individual, route){
            axios.post(`/api/ofa/school/${route}?api_token=${this.token}`, {
                individual : individual
            })
                .then(r => {
                    this.dataSet = r.data;
                    this.loading = false;
                })
                .catch(e => console.log(e.response)) 
        },

        temporarySave (data){
            this.hideLoader = false;

            let api = '';
            let message = [];
            let currentList = null;

            if(!data.individual){
                api = '/api/ofa/school/temporarySaveNewList?api_token='+this.token;
                message = ['Η Λίστα αποθηκεύτηκε με επιτυχία'];
                currentList = this.studentsList;
            }else{
                api = '/api/ofa/school/temporarySaveNewList?api_token='+this.token;
                message = ['Η Λίστα για ατομικά αθλήματα αποθηκεύτηκε με επιτυχία'];
                currentList = this.studentsList;
            }
            
            axios.post(api, {
                current_sport : this.current_sport,
                current_sex : this.current_sex,
                studentsList : currentList,
                year : this.dataSet.year,
                teacher_name : this.teacher_name,
                synodos : this.synodos,
                existedListId : this.existedListId
            })
                .then(r => {
                    this.existedListId = r.data;
                    this.displayAlert('Συγχαρητήρια!', message, 'success');
                    this.hideLoader = true;
                })
                .catch(r => {
                    this.hideLoader = true;
                    this.displayAlert('Προσοχή ΣΦΑΛΜΑ - 150.133', 'Παρακαλώ επικοινωνήστε με τον διαχειριστή του συστήματος (Σμυρναίο Νικόλαο) και αναφέρετε τον κωδικό Σφάλματος', 'danger',true)
                })
        },

        fetchAllStudentsForIndividualSport(){
            this.loadingForExistenceIndividual = true;
            this.studentsList = [];
            this.allStudentsIndividual = this.dataSet.students; // this goes sorting for individual students
            this.allStudentsIndividual.sort(this.sortStudents);
            this.checkIfListAlreadyExists();
        },

        checkIfListAlreadyExists(){
            this.existedListId = 0;

            axios.post('/api/ofa/school/checkIfListAlreadyExists?api_token='+this.token, {
                sport : this.current_sport,
                year_id : this.dataSet.year.id,
                gender : this.current_sex,
                year_name : this.dataSet.year.name
            })
                .then(r => {
                    this.caseListSituation(r.data);

                    this.hideLoading();
                })
                .catch(r => this.displayAlert('Προσοχή ΣΦΑΛΜΑ - 150.135', 'Παρακαλώ επικοινωνήστε με τον διαχειριστή του συστήματος (Σμυρναίο Νικόλαο) και αναφέρετε τον κωδικό Σφάλματος', 'danger',true))
        },

        caseListSituation(data){
            if(data['response'] === 'canMakeSchoolList'){
                this.notExistsPdfFile = true;
                if(!this.current_sport.individual){
                    this.checkTeacherName('');
                }else{
                    this.loadIndividualSports(data['special_sports']);
                    this.checkSynodos('');
                }
            }else if(data['response'] === 'isLocked'){
                this.showTeacherInput = false;
                this.downloadUrl = data["downloadUrl"];
                this.notExistsPdfFile = false;
            }else{
                if(data['response'] === 'temporaryListExists'){
                    let existedStudents = data["list"];
                    this.notExistsPdfFile = true;
                    this.existedListId = data["list_id"];

                    if(!this.current_sport.individual){
                        this.displayStudentList(existedStudents);
                        this.checkTeacherName(data['teacher_name']);
                    }else{
                        this.loadIndividualSports(data['special_sports']);
                        this.checkSynodos(data['synodos']);
                        this.teacher_name = data['teacher_name'];
                        let list_details = data['studentListIndividual'];
                        this.displayStudentListIndividual(existedStudents, list_details);
                    }
                }
            }
        },

        loadIndividualSports(sports){
            this.individualSports = sports.sort(this.sortSports);
        },

        displayStudentList(existedStudents){
            var i;

            for (i = this.allStudentsByGender.length - 1; i >= 0; i -= 1) {
                if (existedStudents.includes(this.allStudentsByGender[i]["am"])) {
                    this.studentsList.push(this.allStudentsByGender[i]);
                    this.allStudentsByGender.splice(i, 1);
                }
            }
        },

        displayStudentListIndividual(existedStudents, list_details){
            var i;

            for (i = this.allStudentsIndividual.length - 1; i >= 0; i -= 1) {
                let currentStudent = this.allStudentsIndividual[i];

                if (existedStudents.includes(this.allStudentsIndividual[i]["am"])) {
                    this.studentsList.push({
                        am : currentStudent["am"],
                        class : currentStudent["class"],
                        first_name : currentStudent["first_name"],
                        last_name : currentStudent["last_name"],
                        loader : 0,
                        middle_name : currentStudent["middle_name"],
                        mothers_name : currentStudent["mothers_name"],
                        school_id : currentStudent["school_id"],
                        sex : currentStudent["sex"],
                        year_birth : currentStudent["year_birth"],
                        sport_id_special : list_details.find(function(obj){
                            return obj.student_id === currentStudent["am"]
                        })["sport_id_special"],
                        sport_name_special : list_details.find(function(obj){
                            return obj.student_id === currentStudent["am"]
                        })["special_name"]
                    });
                    this.allStudentsIndividual.splice(i, 1);
                }
            }

            this.studentsList.sort(this.sortStudentsBySpecialName);
        },

        hideLoading(){
            this.loadingForExistence = false;
            this.loadingForExistenceIndividual = false;
        },

        checkTeacherName(teacher_name){
            if(teacher_name === ''){
                this.displayAlert('Σημείωση:', 'συμπληρώστε παρακαλώ τον υπεύθυνο Φυσικής Αγωγής του Σχολείου σας', 'warning');
            }else{
                this.teacher_name = teacher_name
            }
        },

        checkSynodos(synodos){
            if(synodos === '' || synodos === null){
                this.displayAlert('Σημείωση:', 'συμπληρώστε παρακαλώ τον συνοδό του Σχολείου σας', 'warning');
            }else{
                this.synodos = synodos
            }
        },

        deleteStudent(data){
            this.allStudentsByGender.push(data.student);
            this.allStudentsByGender.sort(this.sortStudents);
        },

        deleteStudentIndividual(data){
            this.pushAndSortListIndividual(data.student);
            this.displayAlert('Προσοχή!', data.message,'danger');            
        },

        pushAndSortListIndividual(student){
            this.allStudentsIndividual.push({
                am : this.parseToInt(student.am),
                class : this.parseToInt(student.class),
                first_name : student.first_name,
                last_name : student.last_name,
                loader : 0,
                middle_name : student.middle_name,
                mothers_name : student.mothers_name,
                school_id : student.school_id,
                sex : this.parseToInt(student.sex),
                year_birth : this.parseToInt(student.year_birth)
            });

            this.allStudentsIndividual.sort(this.sortStudents);
        },

        parseToInt(value){
            let parsed = parseInt(value);
            if(isNaN(parsed)) { return 0;}
            return parsed;
        },

        sortSports(a,b){
            if (a.name < b.name){
                return -1;
            }
            if (a.name > b.name){
                return 1;
            }
        },

        sortStudentsBySpecialName(a, b){
            if (a.sport_id_special < b.sport_id_special){
                return -1;
            }
            if (a.sport_id_special > b.sport_id_special){
                return 1;
            }
        },

        openModal(){
            this.showModalAgreement = true;
        },

        hideModalAgreement(){
            this.showModalAgreement = false;
            this.displayAlert('Ειδοποίηση', 'Η Αποστολή πραγματοποιείται μόνο συμφωνώντας με τους όρους','warning');
        },

        displayAlert(header, body, type, important = false){
            this.$refs.alert.broadcast(header, body, type, important);
        },

        sortStudents (a,b){
            if (a.last_name > b.last_name){
                return 1;
            }
            if (a.last_name < b.last_name){
                return -1;
            }
        },

        showModalNewStudentIndividual(){
            this.showModalStudent = true;
            this.current_sex = 3;
        },

        displayGender(sport){
            this.current_sex = 3;

            if(sport.individual === 1){
                this.showTeacherInput = true;
                this.showIndividualForm = true;
                this.fetchAllStudentsForIndividualSport();
            }else{
                this.showTeacherInput = false;
                this.showIndividualForm = false;
            }
        },

        displayMessage(message){
            this.displayAlert(message.title, message.body, message.type, message.important);
        },

        downloadList(){
            this.displayAlert('Παρακαλώ περιμένετε...', 'Το αρχείο είναι σε διαδικασία κατεβάσματος στον υπολογιστή σας', 'danger');
        },

        sentRequest (data){
            this.showModalAgreement = false;
            this.hideLoader = false;
            let tempStudentList;

            tempStudentList = this.studentsList;

            axios.post(`/api/ofa/school/insertNewList?api_token=${this.token}`, {
                current_sport : this.current_sport,
                current_sex : this.current_sex,
                studentsList : tempStudentList,
                year : this.dataSet.year,
                teacher_name : this.teacher_name,
                synodos : this.synodos,
                existedListId : this.existedListId,
                isIndividual : data.isIndividual
            })
                .then(r => {
                    location.href = r.data;
                    this.downloadUrl = r.data;
                    this.hideLoader = true;
                    this.notExistsPdfFile = false;
                    this.displayAlert('Συγχαρητήρια!', ['Η Κατάσταση Συμμετοχής στάλθηκε στην Ομάδα Φυσικής Αγωγής'], 'success');
                })
                .catch(r => this.displayAlert('Προσοχή ΣΦΑΛΜΑ - 150.136', 'Παρακαλώ επικοινωνήστε με τον διαχειριστή του συστήματος (Σμυρναίο Νικόλαο) και αναφέρετε τον κωδικό Σφάλματος', 'danger',true))
        },
    }
}