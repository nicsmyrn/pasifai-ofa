require('./../../../../../resources/assets/js/bootstrap');

window.Vue = require('vue');

window.Event = new Vue();

import Alert from './../../../../../resources/assets/js/components/Alert.vue';

new Vue({
    el : "#pe11",

    components : {
        Alert
    },

    data : {
        hideLoader : false,
    
        flag : true,
        loading : true,

        school_type : null,

        token : window.api_token,
        base_url : window.base_url,

        teachers : [],

        all_types : [],
        editable_types : [],
        editable_types_list : [],

        datePickerClass : new DatePickerCustomize(),

        updated : false,

        loading_button : false
    },

    mounted(){
        this.fetchTeachersPe11();
    },

    methods : {    
        fetchTeachersPe11(){
            axios.get('/api/school/games/getTeachersPe11?api_token='+this.token)
                .then(r => {
                    this.teachers = r.data['topothetisis'];
                    this.all_types = r.data['all_types'];
                    this.editable_types = r.data['editable_types'];
                    this.editable_types_list = r.data['editable_types_list'];
                    this.school_type = r.data['school_type'];

                    this.loading = false;
                })
                .catch(e => console.log(e.response))  
        },

        saveTeachers(){
            this.loading_button = true;
            axios.post('/api/school/leitourgika/saveTeachers?api_token='+this.token,{
                teachers : this.teachers
            })
                .then(r => {
                    this.updated = false;
                    this.loading_button = false;
                    this.displayAlert(r.data['header'], r.data['message'], r.data['type']);
                })
                .catch(e => console.log(e.response))  

        },

        displayAlert(header, body, type, important = false){
            this.$refs.alert.broadcast(header, body, type, important);
        }
    }

});