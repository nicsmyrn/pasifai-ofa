
class Swalify {
    constructor(data){
        this.originalData = data;

        for(let field in data){
            this[field] = data[field];
        }
    }

    data(){
        let data = {};

        for(let property in this.originalData){
            data[property] = this[property];
        }

        return data;
    }

    submit(ipAPI){
        Swal.fire({
            title: this.title,
            type: this.type,
            text: this.text,
            showCancelButton: true,
            cancelButtonText: 'Ακύρωση',
            confirmButtonText: this.buttonText,
            confirmButtonColor: "#DD6B55",
            showLoaderOnConfirm: true,
            preConfirm: () => {
                return fetch(ipAPI)
                .then(response => {      
                    if (!response.ok) {        
                        throw new Error(response.statusText)
                    }                    
                    return response.json()
                })
                .catch(error => {
                    Swal.showValidationMessage(
                        `Σφάλμα 432: ${error}`
                    )
                })
            },
            allowOutsideClick: () => !Swal.isLoading()
            }).then((result) => {
                if (result.value) {
                    Swal.fire({
                        title : this.success_title,
                        text :  this.success_text,
                        type : 'success'
                    });
                    setTimeout(function(){location.reload(true);}, 2000);
                    
                }
            });
    }


}

export default Swalify