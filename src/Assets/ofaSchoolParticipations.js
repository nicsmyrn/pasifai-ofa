
require('./../../../../../resources/assets/js/bootstrap');

window.Vue = require('vue');

window.Event = new Vue();

import Alert from './../../../../../resources/assets/js/components/Alert.vue';
import ModalAgreement from './components/ModalOfaAgreement.vue';

var parent = new Vue ({
    el : '#app',

    components : {
        Alert, ModalAgreement
    },

    data : {
        synodos : '',

        nullValue : null,
        sex : [
            'Κορίτσια', 'Αγόρια'
        ],
        alert: {},
        dataSet : {
            sports : null,
            students : null,
            years : null
        },

        current_phase : null,
        current_sport : null,
        current_sex : null,
        current_year : null,

        current_list_id : null,

        phases : [
            '1ος αγώνας',
            '2ος αγώνας',
            '3ος αγώνας'
        ],

        listExists : false,


        allStudentsByGender : [],
        studentsInList : [],
        studentsInParticipationStatus : [],
        checkedStudents : [],
        p_status_id : null,

        loading : true,

        showModalAgreement : false,
        hideLoader : true,
        addRemoveLoader : false,

        canChangeParticipationStatus: true,
        loadingPhaseDiv : true,
        loadingParticipationStatusTable : true,

        current_url : window.current_url,
        base_url    : window.base_url,
        token   : window.api_token,
    },

    computed :{
        showParticipationStatusDiv (){
            if(this.current_sport != null && this.current_sex != null && this.current_year != null && this.current_phase != null){
                return true;
            }
            return false;
        },

        showPhase(){
            if(this.current_sport !== null && this.current_sex !== null && this.current_year !== null){
                return true;
            }
            return false;
        }
    },

    created (){
        this.fetchDataFromDatabase();
    },

    ready(){
        this.hideLoader = true;
    },

    methods : {

        openPdfStream (){
            console.log('open method openPdfStream');
            //var newWindow = window.open(); //Open URL in new tab
            //
            //
            //this.$http.post('/aj/ofa/ParticipationStatus/pdfStream', {
            //    checkedStudents : this.checkedStudents,
            //    phase : this.current_phase,
            //    synodos : this.synodos
            //})
            //    .then(r => {
            //        newWindow.html(r.data);
            //    })
            //    .catch(r => {
            //        console.log('Error 999');
            //    });
        },

        toggleStudentInStatus(am, checked, index){
            this.studentsInList[index].loader = true;
            console.log('am:'+ am + ' checked: '+checked+ ' indexed: ' + index);

            // console.log(document.querySelector('#token').attributes['content'].nodeValue);
            let message = [];

            if(checked){
                console.log('remove');
                axios.post('/api/ofa/school/removeFromStatus?api_token='+this.token, {
                    am : am,
                    p_status_id : this.p_status_id,
                    list_id : this.current_list_id,
                    phase : this.current_phase
                })
                .then(r => {
                    this.checkedStudents.splice(this.checkedStudents.indexOf(r.data),1);

                    this.studentsInList[index].loader = false;

                    this.displayAlert('Προσοχή','Ο μαθητής ' + this.studentsInList[index].last_name + " " + this.studentsInList[index].first_name  +  '  με Α.Μ. :' + am + ' αφαιρέθηκε από την κατάσταση συμμετοχής!', 'danger');
                })
                .catch(e => console.log(e.response));
            }else{
                axios.post('/api/ofa/school/AddToStudents?api_token='+this.token, {
                    am : am,
                    p_status_id : this.p_status_id,
                    list_id : this.current_list_id,
                    phase : this.current_phase
                })
                .then(r => {
                    this.checkedStudents.push(am);
                    this.p_status_id = r.data;
                    message = ['Ο μαθητής ' + this.studentsInList[index].last_name + " " + this.studentsInList[index].first_name  + ' με Α.Μ. :' + am + ' προστέθηκε με επιτυχία!'];
                    this.displayAlert('Συγχαρητήρια', message, 'success');
                    this.studentsInList[index].loader = false;
                })
                .catch(r => {
                    this.studentsInList[index].loader = false;
                    this.displayAlert('Προσοχή!','[Ο μαθητής '+ this.studentsInList[index].last_name + " " + this.studentsInList[index].first_name +' με Α.Μ. :' + am + ' δεν ενημερώθηκε λόγω τεχνικού προβλήματος β, ξαναπροσπαθήστε!]', 'danger', true)
                });
            }
        },

        fetchStudentsFromList (){
            this.loadingParticipationStatusTable = true;
            this.canChangeParticipationStatus = false;

            axios.post('/api/ofa/school/fetchStudentsFromParticipationStatus?api_token='+this.token, {
                list_id : this.current_list_id,
                phase : this.current_phase
            })
            .then(r => {
                    this.checkedStudents = r.data['p_status_students_ids'];
                    this.p_status_id = r.data['p_status_id'];

                    this.loadingParticipationStatusTable = false;

                    this.canChangeParticipationStatus = true;
            })
            .catch(e => console.log(e.response))

        },

        fetchDataFromDatabase (){
                axios.get('/api/ofa/school/fetchDataForParticipationStatus?api_token='+this.token)
                .then(r => {
                    this.dataSet = r.data;
                    this.loading = false;
                })
                .catch(e => console.log(e.response)) 
        },

        initializeData(){
            this.checkedStudents = [];
            this.current_sex = null;
            this.synodos = '';
            this.current_phase = null;
        },

        fetchAllStudentsByGender (){
            if(this.showPhase) {
                // console.log('fetchAllStudentsByGender');
                this.loadingForExistence = true;
                this.studentsList = [];
                
                this.allStudentsByGender = this.dataSet.students.filter(student => {
                    if (student.sex === this.current_sex) return student;
                });

                this.checkIfListAlreadyExists();
            }
        },

        checkIfListAlreadyExists(){
            this.loadingPhaseDiv = true;

            axios.post('/api/ofa/school/checkIfListAlreadyExistsForParticipationStatus?api_token='+this.token, {
                sport : this.current_sport,
                year_id : this.current_year,
                gender : this.current_sex
            })
            .then(r => {
                this.loadingPhaseDiv = false;

                if(r.data['message'] === 'listExistsAndCanMakeParticipationStatus'){
                    this.studentsInList = this.allStudentsByGender.filter(student => {
                        if (r.data['list'].includes(student.am)){
                            return student;
                        }
                    });

                    this.current_list_id = r.data['list_id'];
                    this.listExists = true;
                }else{
                    this.listExists = false;
                }
                this.loadingForExistence = false;
            })
            .catch(r => this.displayAlert('Προσοχή ΣΦΑΛΜΑ - 150.135', 'Παρακαλώ επικοινωνήστε με τον διαχειριστή του συστήματος (Σμυρναίο Νικόλαο) και αναφέρετε τον κωδικό Σφάλματος', 'danger',true))
        },

        displayGender(sport){
            this.initializeData();
            this.fetchAllStudentsByGender();
        },

        addStudentToList (selected){
            this.studentsList.push(selected);
            this.studentsList.sort(this.sortStudents);
            this.allStudentsByGender.$remove(selected);
            this.selectedStudent = null;
            console.log('add student & remove ');
        },

        deleteStudent(student){
            this.studentsList.$remove(student);
            this.allStudentsByGender.push(student);
            this.allStudentsByGender.sort(this.sortStudents);
        },

        showModalNewStudent(){
            this.showModalStudent = true;
            console.log('must open...');
        },

        displayAlert(header, body, type, important = false){
            this.$refs.alert.broadcast(header, body, type, important);
        },

        displayMessage(message){
            this.displayAlert(message.title, message.body, message.type);
        },

        sortStudents (a,b){
            if (a.last_name < b.last_name){
                return 1;
            }
            if (a.last_name > b.last_name){
                return -1;
            }
        },

        sentRequest (){

            parent.hideLoader = false;

            parent.$http.post('/aj/ofa/insertNewList',{
                current_sport : parent.current_sport,
                current_sex : parent.current_sex,
                studentsList : parent.studentsList,
                year : parent.dataSet.year,
                synodos : parent.synodos
            })
                //.then(r => location.href = r.data)
                .then(r => console.log(r.data))
                .catch(r => console.log('Error'));
        },

        openModal(){
            this.showModalAgreement = true;
        },

        remove(array, element) {
            const index = array.indexOf(element);

            if (index !== -1) {
                array.splice(index, 1);
            }
        },

        add(array, element){

        }
    }

});
