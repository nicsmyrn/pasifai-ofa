require('./../../../../../../resources/assets/js/bootstrap');

import Vuetify from 'vuetify'
import CovidVaccine from '../components/CovidVaccine.vue'
import CovidRegistered from '../components/CovidRegistered.vue'
import Loader from '../components/Loader.vue'
import axios from 'axios'

window.Vue = require('vue');

Vue.use(Vuetify);

new Vue({
    el : "#app",

    vuetify: new Vuetify(),

    data : {
        token : window.api_token,
        hideLoader: false,
        notRegistered: false,
        profile: {}
    },

    components:{
        CovidVaccine, Loader, CovidRegistered
    },

    mounted(){
        axios.get('/api/ofa/teacher/getTeacherProfile?api_token='+this.token)
            .then(r => {
                this.profile = r.data.profile
                this.notRegistered = r.data.notRegistered
                this.hideLoader = true
            })
            .catch(e => console.log(console.log(e.response))) 
    }

});