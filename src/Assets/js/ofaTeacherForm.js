require('./../../../../../../resources/assets/js/bootstrap');

import Vuetify from 'vuetify'
import FormRequest from '../components/FormRequest.vue'

window.Vue = require('vue');

Vue.use(Vuetify);

new Vue({
    el : "#ofa",

    vuetify: new Vuetify(),

    data : {
        fullname: window.fullname,
        token : window.api_token,
        base_url : window.base_url,
        email: window.email
    },

    components:{
        FormRequest
    }
});