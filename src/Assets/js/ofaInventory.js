require('./../../../../../../resources/assets/js/bootstrap');

import Vuetify from 'vuetify'
import Loader from '../components/Loader.vue'
import NewForm from '../components/Inventory/NewForm.vue'

window.Vue = require('vue');

Vue.use(Vuetify);

new Vue({
    el : "#app",

    vuetify: new Vuetify(),

    data : {
        token : window.api_token,
        hideLoader: false
    },

    components:{
        Loader, NewForm
    },

    mounted(){
        console.log('Hello World!')
        this.hideLoader = true
        // axios.get('/api/ofa/teacher/getTeacherProfile?api_token='+this.token)
        //     .then(r => {
        //         this.profile = r.data.profile
        //         this.notRegistered = r.data.notRegistered
        //         this.hideLoader = true
        //     })
        //     .catch(e => console.log(console.log(e.response))) 
    }

});