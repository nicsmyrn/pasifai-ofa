<?php

namespace Pasifai\Ofa\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Pasifai\Ofa\Models\SportList;

class Sport extends Model
{
    use SoftDeletes;

    protected $table = 'ofa_sports';

    protected $fillable = [
        'name',
        'individual',
        'list_limit',
        'identifier'
    ];

    protected $dates = ['deleted_at'];

    public function special()
    {
        return $this->hasMany(SpecialSport::class, 'sport_id', 'id');
    }

    public function sportlists()
    {
        return $this->hasMany(SportList::class, 'sport_id', 'id');
    }
}
