<?php

namespace Pasifai\Ofa\Models;

use App\School;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Pasifai\Ofa\Models\SportList;

class Vaccine extends Model
{
    protected $table = 'ofa_covid19_vaccine';

    protected $primaryKey = 'uid';

    protected $fillable = [
        'uid',
        'school_base',
        'vaccinated'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'uid', 'id');
    }

    public function school()
    {
        return $this->belongsTo(School::class, 'school_base', 'id');
    }

}
