<?php

namespace Pasifai\Ofa\Models;

use Illuminate\Database\Eloquent\Model;

class InventoryCategory extends Model
{
    protected $table = 'ofa_inventory_categories';

    protected $fillable = [
        'name'
    ];

    public function inventories()
    {
        return $this->hasMany(Inventory::class, 'category_id', 'id');
    }
}
