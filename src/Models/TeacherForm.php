<?php

namespace Pasifai\Ofa\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;

class TeacherForm extends Model
{
    use SoftDeletes;

    protected $table = 'ofa_teachers_form';

    protected $casts = [
        'id' => 'string'
    ];
    
    protected $fillable = [
        'id',
        'fullname',
        'from_user',
        'email',
        'phone',
        'subject',
        'message',
        'read_at'
    ];

    protected $dates = ['deleted_at', 'read_at'];

    public function user()
    {
        return $this->belongsTo(User::class, 'from_user', 'id');
    }

}
