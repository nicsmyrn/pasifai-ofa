<?php

namespace Pasifai\Ofa\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Inventory extends Model
{

    protected $table = 'ofa_inventory';

    public function getIncrementing()
    {
        return false;
    }
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($inventory) {
            $inventory->{$inventory->getKeyName()} = (string) Str::uuid();
        });
    }

    public function getKeyType()
    {
        return 'string';
    }

    protected $fillable = [
        'name',
        'quantity',
        'date_in',
        'donation',
        'amount',
        'remarks',
        'category_id'
    ];

    public function category()
    {
        return $this->belongsTo(InventoryCategory::class, 'category_id', 'id');
    }
}
