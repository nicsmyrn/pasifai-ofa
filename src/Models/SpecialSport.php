<?php

namespace Pasifai\Ofa\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SpecialSport extends Model
{
    use SoftDeletes;

    protected $table = 'ofa_special_sports';

    protected $fillable = [
        'name',
        'sport_id'
    ];

    protected $dates = ['deleted_at'];

    public function sport()
    {
        return $this->belongsTo(Sport::class, 'sport_id', 'id');
    }
}
