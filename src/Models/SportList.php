<?php

namespace Pasifai\Ofa\Models;


use App\School;
use App\Models\Year;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class SportList extends Model
{
    protected $table = 'ofa_lists';

    protected $fillable = [
        'year_id',
        'school_id',
        'sport_id',
        'gender',
        'teacher_name',
        'synodos',
        'filename',
        'locked'
    ];

    public function year()
    {
        return $this->belongsTo(Year::class, 'year_id', 'id');
    }

    public function school()
    {
        return $this->belongsTo(School::class, 'school_id', 'id');
    }

    public function sport()
    {
        return $this->belongsTo(Sport::class, 'sport_id', 'id');
    }

    public function details()
    {
        return $this->hasMany(SportListDetails::class, 'list_id', 'id');
    }

    public function participations()
    {
        return $this->hasMany(SportParticipation::class, 'list_id', 'id');
    }


    public function getUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('d/m/Y H:i:s');
    }
}
