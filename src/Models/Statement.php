<?php

namespace Pasifai\Ofa\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Statement extends Model
{
    protected $table = 'school_sports';

    protected $fillable = [
        'school_id',
        'sport_id',
        'gender'
    ];

//    protected $primaryKey = ['school_id', 'sport_id', 'gender'];

    protected function setKeysForSaveQuery(Builder $query)
    {
        $query
            ->where('school_id', '=', $this->getAttribute('school_id'))
            ->where('sport_id', '=', $this->getAttribute('sport_id'))
            ->where('gender', '=', $this->getAttribute('gender'));
        return $query;
    }
}
