<?php

namespace Pasifai\Ofa\Models;


use App\School;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;


class Student extends Model
{
    use SoftDeletes;

    protected $table = 'ofa_students';

    // protected $primaryKey = ['am' , 'school_id'];

    // public $incrementing = false;

    protected $fillable = [
        'am',
        'last_name',
        'first_name',
        'middle_name',
        'class',
        'sex',
        'school_id',
        'year_birth',
        'mothers_name'

    ];

    protected $dates = ['deleted_at'];

    public function school()
    {
        return $this->belongsTo(School::class, 'school_id', 'id');
    }

    protected function setKeysForSaveQuery(Builder $query)
    {
        $query
            ->where('am', '=', $this->getAttribute('am'))
            ->where('school_id', '=', $this->getAttribute('school_id'));
        return $query;
    }
}
