<?php

namespace Pasifai\Ofa\Controllers;

use App\Http\Controllers\Controller;
use Carbon\Carbon;

class OfaController extends Controller
{

    public function index($timezone = null)
    {
        $current_time = ($timezone)
            ? Carbon::now(str_replace('-', '/', $timezone))
            : Carbon::now();

        return view('ofa::time', compact('current_time'));
    }

}