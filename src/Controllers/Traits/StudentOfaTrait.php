<?php

namespace Pasifai\Ofa\Controllers\Traits;

use Pasifai\Ofa\Requests\StudentRequest;
use Pasifai\Ofa\Models\Student;
use Pasifai\Ofa\Repositories\StudentRepository;

use Illuminate\Http\Request;
use Storage;


trait StudentOfaTrait {
//    protected $repo;

    private $sort = [
        'school_id'         => 'asc',
        'last_name'         => 'asc'
    ];


//    public function __construct(StudentRepository $repository)
//    {
//        $this->repo = $repository;
//    }

    private function compareCollection()
    {
        return function ($a, $b) {
            foreach($this->sort as $sortField=>$orderType){
                if($a[$sortField] < $b[$sortField]){
                    return $orderType === 'asc' ? -1 : 1;
                }elseif($a[$sortField] > $b[$sortField]){
                    return $orderType === 'asc' ? 1 : -1;
                };
            }
            return 0;
        };
    }


    protected function getStudents()
    {
        $students = Student::where('school_id', $this->repo->school_id)->get();

        return view('ofa::school.tables.index-students', compact('students'));
    }

    protected function edit(Student $student)
    {
        $type = \Auth::user()->userable->type;
        
        return view('ofa::school.edit-student', compact('student', 'type'));
        
    }

    protected function deleteStudent(Request $request)
    {
        if($request->ajax()){
            $am = $request->get('am');

            \DB::table('ofa_students')
                ->where('school_id', \Auth::user()->userable->id)
                ->where('am', $am)
                ->delete();

            flash()->success('', 'ο μαθητής με Αριθμό Μητρώου: '. $am . ' ΔΙΕΓΡΑΦΗ');
        }
    }

    protected function insertFromMySchool(StudentRepository $repository, Request $request)
    {
        if($request->hasFile('csv_file')){
            $real_path =  $request->file('csv_file')->getRealPath();
            $mime =  $request->file('csv_file')->getMimeType();

            Storage::put('schools/'.str_slug(\Auth::user()->userable->name).'/myschool.csv', file_get_contents($real_path));

            $repository->execute();
        }else{
            flash()->error('Δεν έχετε επιλέξει το σωστό csv αρχείο');
        }
        return redirect()->back();
    }

    protected function updateS($student, $request)
    {
        \DB::table('ofa_students')
            ->where('school_id', \Auth::user()->userable->id)
            ->where('am', $student->am)
            ->update([
                'am'    => $request->get('am'),
                'last_name'    => $request->get('last_name'),
                'first_name'    => $request->get('first_name'),
                'middle_name'    => $request->get('middle_name'),
                'mothers_name'    => $request->get('mothers_name'),
                'class'    => $request->get('class'),
                'sex'    => $request->get('sex'),
                'year_birth'    => $request->get('year_birth'),
            ]);

        flash()->success('Συγχαρητήρια', 'η αποθήκευση των στοιχεών του μαθητή: ['. $student->last_name . ' '. $student->first_name . '] έγινε με επιτυχία');
    }

    protected  function insertNewStudent(StudentRequest $request)
    {
        if(\Request::ajax()){
            $new_student = $request->get('new_student');

            $student = Student::create($new_student + [
                    'school_id'     => \Auth::user()->userable->id
                ]);
            return $student;
        }else{
            abort(404);
        }
    }
}