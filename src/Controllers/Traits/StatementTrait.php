<?php

namespace Pasifai\Ofa\Controllers\Traits;

use Pasifai\Ofa\Models\Statement;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Mail;
use Pasifai\Ofa\Mail\SentToOfaStatementMail;
use Pasifai\Ofa\Models\Sport;

trait StatementTrait {
    protected function getStatementForm()
    {
        if(\Auth::user()->userable->type == 'Γυμνάσιο'){
            $sports = Sport::pluck('name', 'id');
        }else{
            $sports = Sport::where('identifier', '<>','1st')->pluck('name', 'id');
        }

        $school_id = \Auth::user()->userable->id;

        $statements = Statement::where('school_id', $school_id)->get();

        return view('ofa::school.statement', compact('sports', 'statements', 'school_id'));
    }

    protected function postStatementForm(Request $request, $school)
    {        
        if($school->type == 'Γυμνάσιο'){
            $sports = Sport::all();
        }else{
            $sports = Sport::where('identifier', '<>','1st')->get();
        }

        $now = Carbon::now();
        $gender = ['male', 'female'];

        Statement::where('school_id', $school->id)->delete();

        foreach($sports as $sport){
            foreach($gender as $k=>$v){
                if($request->input('sport.'.$sport->id. '.'. $v) != null) {
                    $school->statements()->attach($sport->id, [
                        'gender' => $v,
                        'created_at' => $now,
                        'updated_at' => $now
                    ]);
                }
            }
        }

        $user = \Auth::user();

        // if(\Config::get('requests.sent_mail')) {
            \Log::alert('statement to OFA sent by E-mail');
            Mail::send(new SentToOfaStatementMail($user, $sports, $gender, $request));
            // Mail::send(new ListFromSchool(storage_path('app/schools/'. $file_path)));
        // }

        flash()->success('Συγχαρητήρια!', 'Η Δήλωση Αθλημάτων στάλθηκε με επιτυχία στην Ομάδα Φυσικής Αγωγής');

        return redirect()->back();
    }

}