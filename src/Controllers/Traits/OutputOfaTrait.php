<?php

namespace Pasifai\Ofa\Controllers\Traits;


//use App\Events\SchoolToOfaTeamNotification;

use Illuminate\Support\Facades\Mail;
use Pasifai\Ofa\Mail\ListFromSchool;
use Pasifai\Ofa\Mail\SportEducationFromSchool;
use Pasifai\Ofa\Mail\SportEducationStivosFromSchool;
use Pasifai\Ofa\Models\SportList;
use Pasifai\Ofa\Models\SportListDetails;
use Pasifai\Ofa\Models\SportParticipation;
use Pasifai\Ofa\Models\SportParticipationDetails;
use Pasifai\Ofa\Models\Student;
use App\School;
use App\Models\Year;
use Barryvdh\DomPDF\Facade as PDF;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

trait OutputOfaTrait {

    protected $list_of_athlopaideies = [
        'ΟΡΕΙΝΗ ΠΟΔΗΛΑΣΙΑ',
        'ΔΡΟΜΟΣ ΣΕ ΑΝΩΜΑΛΟ ΕΔΑΦΟΣ'
    ];

    public function temporarySaveNewListIndividual(Request $request)
    {
        if($request->ajax()){            
            $list_id = $request->get('existedListId');
            $attr = [
                'studentsList'  => $request->get('studentsListIndividual'),
                'gender'   => $request->get('current_sex'),
                'sport' => $request->get('current_sport'),
                'year'       => $request->get('year'),
                'school'        => \Auth::user()->userable,
                'teacher_name'  => $request->get('teacher_name'),
                'synodos'       => $request->get('synodos')
            ];

            if($list_id == 0){
                $newList = $this->createTablesInDatabase($attr);
            }else{
                $newList = $this->updateTablesInDatabase($attr, $list_id);
            }
            return $newList->id;
        }
    }

    public function insertNewListIndividual(Request $request)
    {
        if($request->ajax()){
            $list_id = $request->get('existedListId');

            $attr = [
                'studentsList'  => $request->get('studentsList'),
                'gender'   => $request->get('current_sex'),
                'sport' => $request->get('current_sport'),
                'year'       => $request->get('year'),
                'school'        => \Auth::user()->userable,
                'teacher_name'  => $request->get('teacher_name'),
                'synodos'       => $request->get('synodos')
            ];

            $school_folder = str_slug($attr['school']->name). '/' . $attr['year']['name'] . '/OfaLists/';
            $sport_name = $attr['sport']['name'];

            $gender = $attr['gender'] == 0 ? 'ΚΟΡΙΤΣΙΩΝ' : 'ΑΓΟΡΙΩΝ';

            $file_path = $school_folder .str_slug($sport_name)  . '.pdf';


            if($list_id == 0){
                $newList = $this->createTablesInDatabase($attr, $file_path);
            }else{
                $newList = $this->updateTablesInDatabase($attr, $list_id, $file_path);
            }


            $this->sportListToPDF($attr, $school_folder,  $file_path, 'ofa::school.PDF.sport-list-individual');

            flash()->overlayS('Συγχαρητήρια!', 'Η κατάσταση συμμετοχής αγώνων δημιουργήθηκε με επιτυχία και το έγγραφο PDF στάλθηκε στο γραφείο Φυσικής Αγωγής. Για να δείτε το έγγραφο από το μενού Σχολικά Πρωταθλήματα -> Αρχείο Δηλώσεων του Σχολείου');

//            event(new SchoolToOfaTeamNotification($attr['school'],[
//                'action' => 'OFA\AdminOfaController@showLists',
//                'title' => 'Δημιουργία Κατάστασης Συμμετοχής',
//                'description' => "Η Σχολική Μονάδα <span class='notification_full_name'>&laquo;{$attr['school']['name']}&raquo;</span> δημιούργησε με επιτυχία κατάσταση συμμετοχής στο άθλημα <span class='notification_full_name'>{$sport_name}</span>",
//                'type' => 'danger'
//            ]));

            return route('OFA::sportListIndividual');
        }else{
            abort(401);
        }
    }

    private function createTablesInDatabase($attr, $file_name = null)
    {
        \DB::beginTransaction();

        $newList = SportList::create([
            'year_id' => $attr['year']['id'],
            'school_id' => $attr['school']['id'],
            'sport_id' => $attr['sport']['id'],
            'gender' => $attr['gender'],
            'teacher_name' => $attr['teacher_name'],
            'synodos'       => $attr['synodos'],
            'filename' => $file_name,
            'locked'    => $file_name != null ? true : false
        ]);

        foreach ($attr['studentsList'] as $student) {
            SportListDetails::create([
                'list_id' => $newList->id,
                'student_id' => $student['am'],
                'statement' => true,
                'sport_id_special' => isset($student['sport_id_special']) ? $student['sport_id_special'] : 0
            ]);
        }

        \DB::commit();

        return $newList;
    }

    private function updateTablesInDatabase($attr, $list_id, $file_path = null)
    {
        \DB::beginTransaction();
        $list = SportList::find($list_id);

        $list->update([
            'gender'    => $attr['gender'],
            'teacher_name'  => $attr['teacher_name'],
            'synodos'       => $attr['synodos'],
            'filename'     => $file_path,
            'locked'        => $file_path != null ? true : false
        ]);

        $list->details()->delete();

        foreach ($attr['studentsList'] as $student) {
            SportListDetails::create([
                'list_id' => $list->id,
                'student_id' => $student['am'],
                'statement' => true,
                'sport_id_special' => isset($student['sport_id_special']) ? $student['sport_id_special'] : 0
            ]);
        }

        \DB::commit();
        return $list;
    }

    public function downloadOfaPdfList($year_name, $sport_name, $gender, $school_name = null, $school_type = null)
    {                        
        if($school_name == null){            
            $school_name = \Auth::user()->userable->name;
            $school_type = Auth::user()->userable->type;            
        }
                
        if($school_type == 'Λύκειο' || $school_type == 'ΕΠΑΛ'){
            if(in_array($sport_name, $this->list_of_athlopaideies)){
                $link_type = '/OfaSportEducation/';
            }else{
                $link_type = '/OfaLists/';
            }
            $school_folder = storage_path('app/schools/'. str_slug($school_name) . '/' . $year_name . $link_type);
        }elseif($school_type == 'Γυμνάσιο' || $school_type == 'Δημοτικό'){
            $school_folder = storage_path('app/schools/'. str_slug($school_name) . '/' . $year_name . '/OfaSportEducation/');
        }else{
            $school_folder = storage_path('app/schools/'. str_slug($school_name) . '/' . $year_name . '/OfaLists/');
        }
        
        if($gender == 0){
            $gender_name = 'ΚΟΡΙΤΣΙΩΝ';
        }elseif($gender == 1){
            $gender_name = 'ΑΓΟΡΙΩΝ';
        }elseif($gender == 2){
            // if($school_type == 'Λύκειο' || $school_type == 'ΕΠΑΛ') {
                // $gender_name = '';
            // }elseif($school_type == 'Γυμνάσιο' || $school_type == 'Δημοτικό'){
                $gender_name = 'ΜΙΚΤΗ';
            // }else{
                // $gender_name = '';
            // }
        }else{
            $gender_name = 'ΜΙΚΤΗ';
        }
        
        if($gender_name == ''){
            $file = str_slug($sport_name) . '.pdf';
        }else{
            $file = str_slug($sport_name).'-'.str_slug($gender_name)  . '.pdf';
        }        

        $file_path = $school_folder . $file;

        if (file_exists($file_path)){
            return response()->download($file_path, $file, [
                'Content-Length: '. filesize($file_path)
            ]);
        }else{
            exit('Το συγκεκριμένο αρχείο ΔΕΝ υπάρχει!');
        }

    }

    public function downloadOfaPdf($school_name, $year_name, $sport_name, $gender)
    {

        $school_folder = storage_path('app/schools/'. str_slug($school_name) . '/' . $year_name . '/OfaLists/');

        $gender_name = $gender == 0 ? 'ΚΟΡΙΤΣΙΩΝ' : 'ΑΓΟΡΙΩΝ';

        $file = str_slug($sport_name).'-'.str_slug($gender_name)  . '.pdf';
        $file_path = $school_folder . $file;


        if (file_exists($file_path)){
            return response()->download($file_path, $file, [
                'Content-Length: '. filesize($file_path)
            ]);
        }else{
            exit('Το συγκεκριμένο αρχείο ΔΕΝ υπάρχει!');
        }
    }


    public function sportEducationToPDF($attr, $school_folder, $file_path, $view, $forOfaFile = null, $forOfaView = null)
    {
        \Storage::makeDirectory('schools/'.$school_folder);

        $pdf = PDF::loadView($view,compact('attr'));
        $pdf->setPaper('a4', 'landscape')->save(storage_path('app/schools/'. $file_path));

        if($forOfaFile != null && $forOfaView !=null){
            $pdf2 = PDF::loadView($forOfaView,compact('attr'));
            $pdf2->setPaper('a4', 'landscape')->save(storage_path('app/schools/'. $forOfaFile));      //sentSchoolToOfaSportEducationTwoAttachments
            if(\Config::get('requests.sent_mail')) {
                \Log::alert('file to OFA sent by E-mail');                
                Mail::send(new SportEducationStivosFromSchool(Auth::user(), [storage_path('app/schools/'. $file_path), storage_path('app/schools/'. $forOfaFile)], $attr));
            }

        }else{
            if(\Config::get('requests.sent_mail')) {
                \Log::alert('file to OFA sent by E-mail');
                Mail::send(new SportEducationFromSchool(Auth::user(), storage_path('app/schools/'. $file_path), $attr));
            }
        }

    }

    public function sportListToPDF($attr, $school_folder, $file_path, $view)
    {
        \Storage::makeDirectory('schools/'.$school_folder);

        $pdf = PDF::loadView($view,compact('attr'));

        $pdf->setPaper('a4', 'landscape')->save(storage_path('app/schools/'. $file_path));

        if(\Config::get('requests.sent_mail')) {
            \Log::alert('file to Lyceum sent by E-mail');
            Mail::send(new ListFromSchool(Auth::user(), storage_path('app/schools/'. $file_path), $attr));
        }
        // if(\Config::get('requests.sent_mail')) {
        //     Mail::send(new ListFromSchool(storage_path('app/schools/'. $file_path)));
        //     \Log::alert('file to OFA sent by E-mail');
        // }
    }

    public function showList($token)
    {
        $decoded = \Crypt::decrypt($token);

        $list = SportList::with('details')->find($decoded);

        return view('ofa::school.info-list', compact('list'));
    }

    public function showParticipation($token)
    {
        $decoded = \Crypt::decrypt($token);

        $participation =  SportParticipation::with(['sport_list', 'details'])->find($decoded);

        return view('ofa::school.info-participation', compact('participation'));

    }

    public function pdfParticipationStatus(Request $request, $school_id = null)
    {
        if($school_id == null){
            dd('it is null');
        }


        $school = School::find($school_id);

        $sex = $school->user->sex;
        $full_name = $school->user->full_name;


        $students = Student::whereIn('am', $request->get('checkedStudents'))->where('school_id', $school->id)->get();

        $school_name = $school->name;

        $school_type = $school->type;

        $year_name = Year::find($request->get('year_id'))->name;


        $sport_name = $request->get('sport_name');
        $gender = $request->get('gender');
        $phase = $request->get('phase');
        $individual = $request->get('individual');

        $synodos = $request->get('synodos');

//        if(!\Auth::user()->isRole('ofa')){
//            event(new SchoolToOfaTeamNotification($school,[
//                'action' => 'OFA\AdminOfaController@showLists',
//                'title' => 'Αλλαγή κατάστασης συμμετοχής',
//                'description' => "Η Σχολική Μονάδα <span class='notification_full_name'>&laquo;{$school->name}&raquo;</span> τροποποίησε την κατάσταση συμμετοχής στο άθλημα ...{$sport_name}",
//                'type' => 'warning'
//            ]));
//        }

        $pdf = PDF::loadView('ofa::school.PDF.sport-participation-status',compact('synodos','individual','year_name', 'sport_name', 'gender', 'phase', 'school_name', 'school_type', 'students', 'sex', 'full_name'));
        return $pdf->setPaper('a4', 'landscape')->stream();
//        }else{
//            abort(401);
//        }
    }

}