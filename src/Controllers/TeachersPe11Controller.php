<?php

namespace Pasifai\Ofa\Controllers;

use App\Http\Controllers\Controller;
use Carbon\Carbon;

class TeachersPe11Controller extends Controller
{
    public function __construct()
    {
        $this->middleware('isPe11');
    }

    public function getForm()
    {
        return view('ofa::teachers.form');
    }

}