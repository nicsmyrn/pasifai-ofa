<?php

namespace Pasifai\Ofa\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Year;
use App\School;
use Carbon\Carbon;
use Pasifai\Ofa\Models\Vaccine;
use Pasifai\Pysde\Models\Praxi;

class Covid19Controller extends Controller
{
    public function __construct()
    {
        $this->middleware('isTeacher');
    }

    public function getForm()
    {
        if(config('requests.covid_available')){
            return view('ofa::teachers.covid19');
        }
        abort('403');
    }
}