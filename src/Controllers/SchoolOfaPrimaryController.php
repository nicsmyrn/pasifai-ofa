<?php

namespace Pasifai\Ofa\Controllers;

//use App\Events\SchoolToOfaTeamNotification;
use App\Http\Controllers\Controller;

use Pasifai\Ofa\Requests\StudentRequest;
use Pasifai\Ofa\Models\SportList;
use Pasifai\Ofa\Models\SportListDetails;
use Pasifai\Ofa\Models\SportParticipation;
use Pasifai\Ofa\Models\SportParticipationDetails;
use Pasifai\Ofa\Models\Student;
use Pasifai\Ofa\Repositories\StudentRepository;

use Pasifai\Ofa\Models\Sport;
use App\Models\Year;

use Pasifai\Ofa\Controllers\Traits\OutputOfaTrait;
use Pasifai\Ofa\Controllers\Traits\StudentOfaTrait;
use Illuminate\Http\Request;

use Pasifai\Ofa\Requests\StudentFormRequest;
use Storage;
use Gate;
use Pasifai\Ofa\Controllers\Traits\StatementTrait;

class SchoolOfaPrimaryController extends Controller
{

    use OutputOfaTrait, StudentOfaTrait, StatementTrait;

    protected $repo;

    public function __construct()
    {
        $this->middleware('isPrimary');
    }

    public function getStatementIndex()
    {
        $this->denies('ofa_view_statements_primary');

        return $this->getStatementForm();
    }

    public function postSchoolStatement(Request $request)
    {
        $school =  \Auth::user()->userable;

        return $this->postStatementForm($request, $school);
    }

    // public function checkIfParticipationPrimaryAlreadyExists(Request $request)
    // {
    //     if($request->ajax()){
    //         $data = array();

    //         $sport = $request->get('sport');
    //         $year_id = $request->get('year_id');
    //         $school_id = \Auth::user()->userable->id;

    //         $data['special_sports'] = Sport::find($sport['id'])->special;

    //         $list = SportList::where('year_id', $year_id)
    //             ->where('school_id', $school_id)
    //             ->where('sport_id', $sport['id'])
    //             ->first();

    //         if($list == null){
    //             $data['response'] = 'canMakeSchoolPrimaryIndividual';
    //         }else{
    //             if($list->locked){
    //                 $data['response'] = 'isLockedPrimaryIndividual';
    //                 $data['synodos'] = $list->synodos;
    //                 $data['teacher_name'] = $list->teacher_name;
    //             }else{
    //                 $data['response'] = 'temporaryPrimaryExistsIndividual';
    //                 $data['list'] = $list->details->pluck('student_id')->all();
    //                 $data['studentListIndividual'] = $list->details;
    //                 $data['list_id'] = $list->id;
    //                 $data['synodos'] = $list->synodos;
    //                 $data['teacher_name'] = $list->teacher_name;
    //             }
    //         }
    //         return $data;
    //     }else{
    //         abort(401);
    //     }
    // }

    public function listOfStudentsPrimary(StudentRepository $repository)
    {        
        $this->repo = $repository;

        return $this->getStudents();
    }

    public function insertStudentsFromMySchool(StudentRepository $repository, Request $request)
    {
        return $this->insertFromMySchool($repository, $request);
    }

    public function editStudent(Student $student)
    {
        return $this->edit($student);
    }

    public function updateStudent(Student $student, StudentFormRequest $request)
    {
        $this->updateS($student, $request);

        return redirect()->route('OFA::listOfStudentsPrimary');
    }

    public function getSportEducation()
    {
        $this->denies('ofa_edit_lists_gymnasium');

        return view('ofa::school.create-sport-education');
    }

    public function archivesStatements()
    {
        $lists = SportList::where('school_id', \Auth::user()->userable->id)
            ->get();

        return view('ofa::school.tables.index-sport-education', compact('lists'));
    }

    public function showListPrimary($token)
    {
        $decoded = \Crypt::decrypt($token);

        $list = SportList::find($decoded);

        return view('ofa::school.info-list-primary', compact('list'));
    }

    public function downloadOfaPdfSportEducation($year_name, $sport_name, $gender)
    {

        $school_name = \Auth::user()->userable->name;

        $school_folder = storage_path('app/schools/'. str_slug($school_name) . '/' . $year_name . '/OfaSportEducation/');

        if($gender == 0){
            $gender_name = 'ΚΟΡΙΤΣΙΩΝ';
        }elseif($gender == 1){
            $gender_name = 'ΑΓΟΡΙΩΝ';
        }elseif($gender == 2){
            $gender_name = 'ΜΙΚΤΗ';
        }elseif($gender == 3){
            $gender_name = 'ΜΙΚΤΗ';
        }else{
            $gender_name = '';
        }

        if($gender_name == ''){
            $file = str_slug($sport_name) . '.pdf';
        }else{
            $file = str_slug($sport_name).'-'.str_slug($gender_name)  . '.pdf';
        }

        $file_path = $school_folder . $file;


        if (file_exists($file_path)){
            return response()->download($file_path, $file, [
                'Content-Length: '. filesize($file_path)
            ]);
        }else{
            flash()->error('Το αρχείο ΔΕΝ υπάρχει!','', 'warning');

            return redirect()->back();
        }

    }


    // public function temporarySaveNewList(Request $request)
    // {
    //     if($request->ajax()){
    //         $list_id = $request->get('existedListId');
    //         $attr = [
    //             'studentsList'  => $request->get('studentsList'),
    //             'gender'   => $request->get('current_sex'),
    //             'sport' => $request->get('current_sport'),
    //             'year'       => $request->get('year'),
    //             'school'        => \Auth::user()->userable,
    //             'teacher_name'  => $request->get('teacher_name'),
    //             'synodos'       => $request->get('synodos')
    //         ];

    //         if($list_id == 0){
    //             $newList = $this->createTablesInDatabase($attr);
    //         }else{
    //             $newList = $this->updateTablesInDatabase($attr, $list_id);
    //         }
    //         return $newList->id;

    //     }else{
    //         abort(401);
    //     }
    // }

    // public function temporarySaveNewListIndividual(Request $request)
    // {
    //     if($request->ajax()){
    //         $list_id = $request->get('existedListId');
    //         $attr = [
    //             'studentsList'  => $request->get('studentsListIndividual'),
    //             'gender'   => $request->get('current_sex'),
    //             'sport' => $request->get('current_sport'),
    //             'year'       => $request->get('year'),
    //             'school'        => \Auth::user()->userable,
    //             'teacher_name'  => $request->get('teacher_name'),
    //             'synodos'       => $request->get('synodos')
    //         ];

    //         if($list_id == 0){
    //             $newList = $this->createTablesInDatabase($attr);
    //         }else{
    //             $newList = $this->updateTablesInDatabase($attr, $list_id);
    //         }
    //         return $newList->id;
    //     }else{
    //         abort(401);
    //     }
    // }



    // public function insertNewListIndividual(Request $request)
    // {
    //     if($request->ajax()){
    //         $list_id = $request->get('existedListId');

    //         $attr = [
    //             'studentsList'  => $request->get('studentsList'),
    //             'gender'   => $request->get('current_sex'),
    //             'sport' => $request->get('current_sport'),
    //             'year'       => $request->get('year'),
    //             'school'        => \Auth::user()->userable,
    //             'teacher_name'  => $request->get('teacher_name'),
    //             'synodos'       => $request->get('synodos')
    //         ];

    //         $school_folder = str_slug($attr['school']->name). '/' . $attr['year']['name'] . '/OfaSportEducation/';
    //         $sport_name = $attr['sport']['name'];

    //         $gender = '';

    //         if($attr['gender'] == 0){
    //             $gender = 'ΚΟΡΙΤΣΙΩΝ';
    //         }elseif($attr['gender'] == 1){
    //             $gender = 'ΑΓΟΡΙΩΝ';
    //         }else{
    //             $gender = 'ΜΙΚΤΗ';
    //         }

    //         $file_path = $school_folder .str_slug($sport_name).'-'.str_slug($gender)  . '.pdf';
    //         $file_path_for_ofa = $school_folder . 'for_OFA_only-' .str_slug($sport_name).'-'.str_slug($gender)  . '.pdf';

    //         if($list_id == 0){
    //             $newList = $this->createTablesInDatabase($attr, $file_path);
    //         }else{
    //             $newList = $this->updateTablesInDatabase($attr, $list_id, $file_path);
    //         }

    //         $this->sportEducationToPDF($attr, $school_folder,  $file_path, 'OFA.school.PDF.sport-education-individual', $file_path_for_ofa, 'OFA.school.PDF.sport-education-for-ofa-only');


    //         flash()->overlayS('Συγχαρητήρια!', 'Η Κατάσταση Συμμετοχής αγώνων ΑθλοΠαιδεία δημιουργήθηκε με επιτυχία και το έγγραφο PDF στάλθηκε στο γραφείο Φυσικής Αγωγής. Για να δείτε το έγγραφο από το μενού Σχολικά Πρωταθλήματα -> Αρχείο Δηλώσεων του Σχολείου');

    //         return route('OFA::sportEducation');
    //     }else{
    //         abort(401);
    //     }
    // }

    // public function checkIfSportEducationAlreadyExists(Request $request)
    // {
    //     if($request->ajax()){
    //         $data = array();

    //         $sport = $request->get('sport');
    //         $year_id = $request->get('year_id');
    //         $gender = $request->get('gender');
    //         $school_id = \Auth::user()->userable->id;

    //         if($gender != 2){
    //             $list = SportList::where('year_id', $year_id)
    //                 ->where('school_id', $school_id)
    //                 ->where('sport_id', $sport['id'])
    //                 ->where('gender', $gender)
    //                 ->first();
    //         }else{
    //             $list = SportList::where('year_id', $year_id)
    //                 ->where('school_id', $school_id)
    //                 ->where('sport_id', $sport['id'])
    //                 ->first();
    //         }


    //         if($list == null){
    //             $data['response'] = 'canMakeSchoolSportEducation';
    //         }else{
    //             if($list->locked){
    //                 $data['response'] = 'isLocked';
    //                 $data['teacher_name'] = $list->teacher_name;
    //                 $data['synodos'] = $list->synodos;
    //             }else{
    //                 $data['response'] = 'temporarySportEducationExists';
    //                 $data['list'] = $list->details->pluck('student_id')->all();
    //                 $data['list_id'] = $list->id;
    //                 $data['teacher_name'] = $list->teacher_name;
    //                 $data['synodos'] = $list->synodos;
    //             }
    //         }
    //         return $data;
    //     }else{
    //         abort(401);
    //     }
    // }

    // public function checkIfSportEducationAlreadyExistsForIndividual(Request $request)
    // {
    //     if($request->ajax()){
    //         $data = array();

    //         $sport = $request->get('sport');
    //         $year_id = $request->get('year_id');
    //         $school_id = \Auth::user()->userable->id;

    //         $data['special_sports'] = Sport::find($sport['id'])->special;

    //         $list = SportList::where('year_id', $year_id)
    //             ->where('school_id', $school_id)
    //             ->where('sport_id', $sport['id'])
    //             ->first();

    //         if($list == null){
    //             $data['response'] = 'canMakeSchoolSportEducationIndividual';
    //         }else{
    //             if($list->locked){
    //                 $data['response'] = 'isLockedIndividual';
    //                 $data['teacher_name'] = $list->teacher_name;
    //                 $data['synodos'] = $list->synodos;
    //             }else{
    //                 $data['response'] = 'temporarySportEducationExistsIndividual';
    //                 $data['list'] = $list->details->pluck('student_id')->all();
    //                 $data['studentListIndividual'] = $list->details;
    //                 $data['list_id'] = $list->id;
    //                 $data['teacher_name'] = $list->teacher_name;
    //                 $data['synodos'] = $list->synodos;
    //             }
    //         }
    //         return $data;
    //     }else{
    //         abort(401);
    //     }
    // }




    public function deleteStudentFromSpecificSchool(Request $request)
    {
        $this->deleteStudent($request);

        return 'ok2';
    }

    private function updateTablesInDatabase($attr, $list_id, $file_path = null)
    {
        \DB::beginTransaction();
        $list = SportList::find($list_id);
        $list->update([
            'gender'    => $attr['gender'],
            'teacher_name'  => $attr['teacher_name'],
            'synodos' => $attr['synodos'],
            'filename'     => $file_path,
            'locked'        => false
//            'locked'        => $file_path != null ? true : false
        ]);

        $list->details()->delete();

        foreach ($attr['studentsList'] as $student) {
            SportListDetails::create([
                'list_id' => $list->id,
                'student_id' => $student['am'],
                'statement' => true,
                'sport_id_special' => isset($student['sport_id_special']) ? $student['sport_id_special'] : 0
            ]);
        }

        \DB::commit();
        return $list;
    }

    private function denies($permision)
    {
        if (Gate::denies($permision)){
            abort(403);
        }
    }
}