<?php

namespace Pasifai\Ofa\Controllers;

use App\Http\Controllers\Controller;


use App\Models\Role;
use Carbon\Carbon;
use Pasifai\Ofa\Models\Statement;
use Pasifai\Ofa\Requests\StudentRequest;
use Pasifai\Ofa\Models\SportList;
use Pasifai\Ofa\Models\SportListDetails;
use Pasifai\Ofa\Models\SportParticipation;
use Pasifai\Ofa\Models\SportParticipationDetails;
use Pasifai\Ofa\Models\Student;
//use App\Notification;
use App\School;
use Illuminate\Http\Request;

use Pasifai\Ofa\Models\Sport;
use App\Models\Year;
use Pasifai\Ofa\Controllers\Traits\OutputOfaTrait;
use Maatwebsite\Excel\Facades\Excel;
use Pasifai\Ofa\Controllers\ExcelExports\StatementsExport;
use Pasifai\Ofa\Controllers\ExcelExports\AthlopaideiesExport;
use Pasifai\Ofa\Controllers\ExcelExports\VaccineExport;
use Pasifai\Ofa\Controllers\Traits\StudentOfaTrait;
class AdminOfaController extends Controller
{

    use OutputOfaTrait, StudentOfaTrait;

    protected $style_Array = array(
        'borders' => array(
            'allborders' => array(
                // 'style' => \PHPExcel_Style_Border::BORDER_THIN
            )
        ),
        'alignment' => array(
            // 'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            // 'vertical'   => \PHPExcel_Style_Alignment::VERTICAL_CENTER
        )
    );

    public function __construct()
    {
        $this->middleware('ofaAdmin');

//        \Auth::loginUsingId(1109); //16 1ο Γυμνάσιο, 57 1 ΕΠΑΛ ΧΑΝΙΩΝ  | 23 4 Γυνάσιο | 42  1 ΓΕΛ
    }

    public function statementsPermissions()
    {
        $data = [];
        $role = Role::where('slug', 'school')->first();

        if($role->hasPermission('ofa_view_statements_primary')){
            $data['primary'] = true;
        }else{
            $data['primary'] = false;
        }

        if($role->hasPermission('ofa_view_statements_lykeia')){
            $data['secondary'] = true;
        }else{
            $data['secondary'] = false;
        }

        return view('ofa::admin.statements.statements-permissions', compact('data'));

    }

    public function listsPermissions()
    {
        $data = [];
        $role = Role::where('slug', 'school')->first();

        if($role->hasPermission('ofa_edit_lists_gymnasium')){
            $data['primary'] = true;
        }else{
            $data['primary'] = false;
        }

        if($role->hasPermission('ofa_edit_lists_lykeia')){
            $data['secondary'] = true;
        }else{
            $data['secondary'] = false;
        }

        return view('ofa::admin.lists.lists-permissions', compact('data'));

    }

    public function changeAccessPrimaryLists()
    {
        $p = [
            'ofa_edit_lists_gymnasium'
        ];

        $permissionRemoved = false;

        $role_school = Role::where('slug', 'school')->first();

        foreach($p as $key=>$permission){
            if($role_school->hasPermission($permission)){
                $role_school->takePermission($permission);
                $permissionRemoved = true;
            }else{
                $role_school->givePermission($permission);
            }
        }
        if($permissionRemoved){
            flash()->overlayE('Προσοχή!', 'Αφαιρέσατε το δικαίωμα Επεξεργασίας Λίστας από τα Σχολεία');
        }else{
            flash()->overlayS('Συγχαρητήρια!', 'Δώσατε το δικαίωμα Επεξεργασίας Λίστας από τα Σχολεία');
        }
        return redirect()->back();
    }

    public function changeAccessSecondaryLists()
    {
        $p = [
            'ofa_edit_lists_lykeia'
        ];

        $permissionRemoved = false;

        $role_school = Role::where('slug', 'school')->first();

        foreach($p as $key=>$permission){
            if($role_school->hasPermission($permission)){
                $role_school->takePermission($permission);
                $permissionRemoved = true;
            }else{
                $role_school->givePermission($permission);
            }
        }
        if($permissionRemoved){
            flash()->overlayE('Προσοχή!', 'Αφαιρέσατε το δικαίωμα Επεξεργασίας Λίστας από τα Σχολεία');
        }else{
            flash()->overlayS('Συγχαρητήρια!', 'Δώσατε το δικαίωμα Επεξεργασίας Λίστας από τα Σχολεία');
        }
        return redirect()->back();
    }

    public function changeAccessPrimary()
    {
        $p = [
            'ofa_view_statements_primary',
            'ofa_post_statements_primary'
        ];

        $permissionRemoved = false;

        $role_school = Role::where('slug', 'school')->first();

        foreach($p as $key=>$permission){
            if($role_school->hasPermission($permission)){
                $role_school->takePermission($permission);
                $permissionRemoved = true;
            }else{
                $role_school->givePermission($permission);
            }
        }
        if($permissionRemoved){
            flash()->overlayE('Προσοχή!', 'Αφαιρέσατε το δικαίωμα Επεξεργασίας Δηλώσεων Συμμετοχής από τα Σχολεία');
        }else{
            flash()->overlayS('Συγχαρητήρια!', 'Δώσατε το δικαίωμα Επεξεργασίας Δηλώσεων Συμμετοχής από τα Σχολεία');
        }
        return redirect()->back();
    }

    public function changeAccessSecondary()
    {
        $p = [
            'ofa_view_statements_lykeia',
            'ofa_post_statements_lykeia'
        ];

        $permissionRemoved = false;

        $role_school = Role::where('slug', 'school')->first();

        foreach($p as $key=>$permission){
            if($role_school->hasPermission($permission)){
                $role_school->takePermission($permission);
                $permissionRemoved = true;
            }else{
                $role_school->givePermission($permission);
            }
        }
        if($permissionRemoved){
            flash()->overlayE('Προσοχή!', 'Αφαιρέσατε το δικαίωμα Επεξεργασίας Δηλώσεων Συμμετοχής από τα Σχολεία');
        }else{
            flash()->overlayS('Συγχαρητήρια!', 'Δώσατε το δικαίωμα Επεξεργασίας Δηλώσεων Συμμετοχής από τα Σχολεία');
        }
        return redirect()->back();
    }

    public function gamesToExcel()
    {        
        $sheets = collect();

        $sports = Sport::with(['sportlists' => function($q){
            $q->where('locked', true);
        }])->has('sportlists')->get();
        
        foreach($sports as $sport){

            $students = collect();

            $lists =  $sport->sportlists;

            foreach($lists as $list){
                $details =  $list->details;
                foreach($details as $d){
                    $student =  Student::with('school')->where('am', $d->student_id)->where('school_id', $list->school_id)->first();

                    $students->push($student);
                }
            }
            
            $ordered_students = $students->sort($this->compareCollection())->values();

            $sheets->put($sport->name, $ordered_students);

        }      
                
        return $this->createExcelStudentsForAthlopaideies($sheets);
    }

    public function statementsToExcel()
    {        
        $schools = School::where('type', 'epal')->orWhere('type', 'lyk')->get();

        $school_list = $schools->pluck('id');
        
        $statements =  Statement::whereIn('school_id', $school_list)->get();
        
        $gender = ['male', 'female'];

        $sports = Sport::all();

        return $this->createExcelStatements($statements, $gender, $sports, $schools, "ΛΥΚΕΙΑ");
    }

    public function statementsToExcelPrimary()
    {
        $schools = School::where('type', 'gym')->get();

        $school_list = $schools->pluck('id');

        $statements =  Statement::whereIn('school_id', $school_list)->get();

        $gender = ['male', 'female'];

        $sports = Sport::all();

        return $this->createExcelStatementsPrimary($statements, $gender, $sports, $schools, "ΓΥΜΝΑΣΙΑ");
    }

    public function statementsView()
    {
        return 'ok2';
    }

    private function createExcelStudentsForAthlopaideies($sheets)
    {
        $now = Carbon::now()->format('d_m_Y_h_i_s');

        return Excel::download(new AthlopaideiesExport($sheets), "{$now}_ΑΘΛΗΤΕΣ_ΑΘΛΟΠΑΙΔΕΙΑΣ.xlsx");
    }

    private function createExcelStatements($statements, $gender, $sports, $schools, $file_name)
    {
        $now = Carbon::now()->format('d_m_Y_h_i_s');

        return Excel::download(new StatementsExport(compact('statements', 'gender', 'sports', 'schools', 'file_name')), "{$file_name}_{$now}_ΔΗΛΩΣΕΙΣ_ΑΓΩΝΙΣΜΑΤΩΝ.xlsx");

        \Excel::create(Carbon::now()->format('d_m_Y_h_i_s').'_ΛΥΚΕΙΑ_ΕΠΑΛ_ΔΗΛΩΣΕΙΣ_ΣΥΜΜΕΤΟΧΗΣ', function($excel) use($statements, $gender, $sports, $schools){

            $excel->sheet('Συγκεντρωτικά', function($sheet) use($statements, $gender, $sports){

                $sheet->loadView('ofa::admin.statements.sheet1excel')
                    ->setFontFamily('Arial')
                    ->setFontSize(19)
                    ->setFontBold(false)
                    ->with(compact('statements', 'gender', 'sports'));

                $sheet->setPageMargin(array(
                    0.55, 0.25, 0.25, 0.25
                ));

                $sheet->getStyle('A1:C'.($sports->count()+1))->applyFromArray($this->style_Array);

                $sheet->cells('A1:C1', function($cells) {
                    $cells->setFontSize(20);
                    $cells->setFontWeight('bold');
                    $cells->setBackground('#B0B0B0');
                });

                $height_array = array();
                for($i=1; $i<=($sports->count()+1); $i++){
                    $height_array[$i] = 50;
                }
                $sheet->setHeight($height_array);

                $sheet->getHeaderFooter()->setOddHeader('&C&H&25 ΣΥΓΚΕΝΤΡΩΤΙΚΑ ΔΗΛΩΣΕΩΝ ΣΥΜΜΕΤΟΧΗΣ ΓΙΑ ΛΥΚΕΙΑ - ΕΠΑΛ');
                $sheet->getHeaderFooter()->setOddFooter(Carbon::now()->format('d/m/Y h:i:s'));


                $sheet->setOrientation('landscape');
            });

            $excel->sheet('Λεπτομέρειες', function($sheet) use($statements, $gender, $sports, $schools){

                $sheet->loadView('ofa::admin.statements.sheet2excel')
                    ->setFontFamily('Arial')
                    ->setFontSize(19)
                    ->setFontBold(false)
                    ->with(compact('statements', 'gender', 'sports', 'schools'));

                $sheet->setPageMargin(array(
                    0.55, 0.25, 0.25, 0.25
                ));

                $sheet->getHeaderFooter()->setOddHeader('&C&H&25 ΛΕΠΤΟΜΕΡΕΙΕΣ ΑΘΛΗΜΑΤΩΝ ΓΙΑ ΛΥΚΕΙΑ');
                $sheet->getHeaderFooter()->setOddFooter(Carbon::now()->format('d/m/Y h:i:s'));


                $sheet->setOrientation('landscape');
            });

            $excel->setTitle('School Sport Statements');
            $excel->setCreator('OFA')
                ->setCompany('DDE Chanion');

            $excel->setDescription('Nick Smyrnaios');

        })->download('xls');
    }

    private function createExcelStatementsPrimary($statements, $gender, $sports, $schools, $file_name)
    {
        $now = Carbon::now()->format('d_m_Y_h_i_s');

        return Excel::download(new StatementsExport(compact('statements', 'gender', 'sports', 'schools', 'file_name')), "{$file_name}_{$now}_ΔΗΛΩΣΕΙΣ_ΑΓΩΝΙΣΜΑΤΩΝ.xlsx");
    }

    public function statistics(Request $request)
    {

        $gym = School::where('type', 'gym')->pluck('id');
        $lykeia = School::where('type', 'lyk')->orWhere('type', 'epal')->pluck('id');
        $dimotika = School::where('type', 'dimotika')->pluck('id');

        if($request->get('first') == 'high-school'){
            $first_statistic_array = SportList::with('details')->whereIn('school_id', $lykeia)->where('locked', true)->get()->groupBy('sport_id');
        }elseif($request->get('first') == 'secondary-school'){
            $first_statistic_array = SportList::with('details')->whereIn('school_id', $gym)->where('locked', true)->get()->groupBy('sport_id');
        }elseif($request->get('first') == 'primary-school'){
            $first_statistic_array = SportList::with('details')->whereIn('school_id', $dimotika)->where('locked', true)->get()->groupBy('sport_id');
        }else{
            $first_statistic_array = SportList::with('details')->whereIn('school_id', $lykeia)->where('locked', true)->get()->groupBy('sport_id');
        }

        if($request->get('second') == 'high-school'){
            $second_statistic_array = SportList::with('details')->whereIn('school_id', $lykeia)->where('locked', true)->get()->groupBy('sport_id');
        }elseif($request->get('second') == 'secondary-school'){
            $second_statistic_array = SportList::with('details')->whereIn('school_id', $gym)->where('locked', true)->get()->groupBy('sport_id');
        }elseif($request->get('second') == 'primary-school'){
            $second_statistic_array = SportList::with('details')->whereIn('school_id', $dimotika)->where('locked', true)->get()->groupBy('sport_id');
        }else{
            $second_statistic_array = SportList::with('details')->whereIn('school_id', $lykeia)->where('locked', true)->get()->groupBy('sport_id');
        }

        $teams = collect();
        $students = collect();


        foreach($first_statistic_array as $key=>$sportLists){
            $teams->push([
                'sport_name'    => Sport::find($key)->name,
                'female'        => $sportLists->where('gender', 0)->count(),
                'male'          => $sportLists->where('gender', 1)->count(),
                'sum'           => $sportLists->count()
            ]);

        }


        foreach($second_statistic_array as $key=>$sportLists){
            $sumMale = 0;
            $sumFemale = 0;

            foreach($sportLists as $sportList){
                if($sportList->gender == 0){
                    $sumFemale += $sportList->details->count();
                }else{
                    $sumMale += $sportList->details->count();
                }
            }

            $students->push([
                'sport_name'    => Sport::find($key)->name,
                'female_counter'        => $sumFemale,
                'male_counter'          => $sumMale,
                'sum'           => ($sumFemale + $sumMale)
            ]);
        }

        $school_types = [
            'high-school' => 'ΛΥΚΕΙΑ - ΕΠΑΛ',
            'secondary-school' => 'ΓΥΜΝΑΣΙΑ',
            'primary-school'    => 'ΔΗΜΟΤΙΚΑ'
        ];

        return view('ofa::admin.statistics', compact('teams', 'students', 'school_types'));
    }

    public function getLists()
    {
        $schools = School::where('type', 'lyk')->orWhere('type', 'epal')->get();

        return view('ofa::admin.show-all-lists', compact('schools'));
    }

    public function getGlobalLists()
    {
        $lists = SportList::with(['year', 'school'])->whereNotNull('filename')->get();

        return view('ofa::admin.all-lists-by-filter', compact('lists'));
    }

    public function toggleLock($list_id)
    {
        $list = SportList::find($list_id);

        $list->locked = $list->locked ? false : true;

        $list->save();

        flash()->success('Εντάξει...');

        return redirect()->back();
    }

    public function showLists(School $school, Request $request)
    {
        $action = $request->get('action');
        if($action != null){
//            Notification::where('uniqueAction', $action)->update([
//                'unread' => 'is_false'
//            ]);
        }

        $name = $school->name;

        $schools = School::where('type', 'lyk')->orWhere('type', 'epal')->orWhere('type', 'gym')->get();

        $lists = SportList::where('school_id', $school->id)->get();

        $parts = collect();


        foreach($lists as $list){
            foreach($list->participations as $participation){
                $parts->push([
                    'year_name' => $list->year->name,
                    'year_id'   => $list->year_id,
                    'individual'   => $list->sport->individual,
                    'sport_name'    => $list->sport->name,
                    'gender'        => $list->gender,
                    'synodos'       => $list->synodos,
                    'phase'         => $participation->phase,
                    'part_id'       => $participation->id,
                    'checkedStudents'  => $participation->details->pluck('student_id')->all()
                ]);
            }
        }

        $school_id = $school->id;

        return view('ofa::admin.show-school-list', compact('schools', 'name', 'lists', 'parts', 'school_id'));

    }
}