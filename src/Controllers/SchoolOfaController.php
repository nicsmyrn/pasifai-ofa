<?php

namespace Pasifai\Ofa\Controllers;

//use App\Events\SchoolToOfaTeamNotification;
use App\Http\Controllers\Controller;


use Illuminate\Auth\Authenticatable;
use Illuminate\Support\Facades\Auth;
use Pasifai\Ofa\Requests\StudentFormRequest;
use Pasifai\Ofa\Requests\StudentRequest;
use Pasifai\Ofa\Models\SportList;
use Pasifai\Ofa\Models\SportListDetails;
use Pasifai\Ofa\Models\SportParticipation;
use Pasifai\Ofa\Models\SportParticipationDetails;
use Pasifai\Ofa\Models\Student;
use Pasifai\Ofa\Models\Sport;
use App\Models\Year;

use Pasifai\Ofa\Controllers\Traits\OutputOfaTrait;
use Pasifai\Ofa\Controllers\Traits\StudentOfaTrait;
use Pasifai\Ofa\Controllers\Traits\StudentOfaAbstract;
use Illuminate\Http\Request;

use Pasifai\Ofa\Repositories\StudentRepository;
use Pasifai\Ofa\Controllers\Traits\StatementTrait;
use Gate;


class SchoolOfaController extends Controller
{

    use OutputOfaTrait, StudentOfaTrait, StatementTrait;

    protected $repo;
    protected $user;

    public function __construct()
    {
        $this->middleware('isLykeio');
    }

    public function getSportEducationforLyceum()
    {
        return view('ofa::school.create-sport-education');
    }

    public function getStatementIndex()
    {
        $this->denies('ofa_view_statements_lykeia');

        return $this->getStatementForm();
    }

    public function postSchoolStatement(Request $request)
    {
        $school =  \Auth::user()->userable;

        return $this->postStatementForm($request, $school);
    }

    public function insertStudentsFromMySchool(StudentRepository $repository, Request $request)
    {
        return $this->insertFromMySchool($repository, $request);
    }

    public function listOfStudents(StudentRepository $repository)
    {
        $this->repo = $repository;        

        return $this->getStudents();
    }

    public function editStudent(Student $student)
    {        
        return $this->edit($student);
    }

    public function deleteStudentFromSpecificSchool(Request $request)
    {
        $this->deleteStudent($request);

        return 'ok2';
    }

    public function updateStudent(Student $student, StudentFormRequest $request)
    {
        $this->updateS($student, $request);

        return redirect()->route('OFA::listOfStudents');
    }

    public function getLists()
    {
        $this->denies('ofa_edit_lists_lykeia');

        return view('ofa::school.create-list-high-school');
    }
    
    public function getListsIndividual()
    {
        $this->denies('ofa_edit_lists_lykeia');

        return view('ofa::school.create-list-high-school-individual');
    }

    public function archivesStatementsHighSchool()
    {
        $lists = SportList::where('school_id', \Auth::user()->userable->id)
                            ->get();
        
        $parts = collect();

        foreach($lists as $list){
            foreach($list->participations as $participation){                
                $parts->push([
                    'year_name' => $list->year->name,
                    'year_id'   => $list->year_id,
                    'individual'   => $list->sport->individual,
                    'sport_name'    => $list->sport->name,
                    'gender'        => $list->gender,
                    'synodos'       => $list->synodos,
                    'phase'         => $participation->phase,
                    'part_id'       => $participation->id,
                    'checkedStudents'  => $participation->details->pluck('student_id')->all()
                ]);
            }
        }

        return view('ofa::school.tables.index-lists', compact('lists', 'parts'));
    }

    public function temporarySaveNewList(Request $request)
    {
        if($request->ajax()){
            $list_id = $request->get('existedListId');
            $attr = [
                'studentsList'  => $request->get('studentsList'),
                'gender'   => $request->get('current_sex'),
                'sport' => $request->get('current_sport'),
                'year'       => $request->get('year'),
                'school'        => \Auth::user()->userable,
                'teacher_name'  => $request->get('teacher_name'),
                'synodos'       => $request->get('synodos')
            ];

            if($list_id == 0){
                $newList = $this->createTablesInDatabase($attr);
            }else{
                $newList = $this->updateTablesInDatabase($attr, $list_id);
            }
            return $newList->id;

        }else{
            abort(401);
        }
    }


    public function checkIfListAlreadyExists(Request $request)
    {
        if($request->ajax()){
            $data = array();

            $sport = $request->get('sport');
            $year_id = $request->get('year_id');
            $gender = $request->get('gender');
            $school_id = \Auth::user()->userable->id;

            $list = SportList::where('year_id', $year_id)
                        ->where('school_id', $school_id)
                        ->where('sport_id', $sport['id'])
                        ->where('gender', $gender)
                        ->first();

            if($list == null){
                $data['response'] = 'canMakeSchoolList';
            }else{
                if($list->locked){
                    $data['response'] = 'isLocked';
                    $data['teacher_name'] = $list->teacher_name;
                }else{
                    $data['response'] = 'temporaryListExists';
                    $data['list'] = $list->details->pluck('student_id')->all();
                    $data['list_id'] = $list->id;
                    $data['teacher_name'] = $list->teacher_name;
                }
            }
            return $data;
        }else{
            abort(401);
        }
    }

    public function checkIfListAlreadyExistsForIndividual(Request $request)
    {
        if($request->ajax()){
            $data = array();

            $sport = $request->get('sport');
            $year_id = $request->get('year_id');
            $school_id = \Auth::user()->userable->id;

            $data['special_sports'] = Sport::find($sport['id'])->special;

            $list = SportList::where('year_id', $year_id)
                ->where('school_id', $school_id)
                ->where('sport_id', $sport['id'])
                ->first();

            if($list == null){
                $data['response'] = 'canMakeSchoolListIndividual';
            }else{
                if($list->locked){
                    $data['response'] = 'isLockedIndividual';
                    $data['synodos'] = $list->synodos;
                }else{
                    $data['response'] = 'temporaryListExistsIndividual';
                    $data['list'] = $list->details->pluck('student_id')->all();
                    $data['studentListIndividual'] = $list->details;
                    $data['list_id'] = $list->id;
                    $data['synodos'] = $list->synodos;
                }
            }
            return $data;
        }else{
            abort(401);
        }
    }

    private function denies($permision)
    {
        if (Gate::denies($permision)){
            abort(403);
        }
    }

}