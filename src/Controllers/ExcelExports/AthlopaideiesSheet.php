<?php

namespace Pasifai\Ofa\Controllers\ExcelExports;

use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\WithTitle;

class AthlopaideiesSheet implements FromView, ShouldAutoSize, WithEvents, WithTitle
{
    protected $students;
    protected $title;
    protected $last_cell_row;

    public function __construct($collection, $key)
    {
        $this->students = $collection;    
        $this->title = $key;        
    }

    public function title(): string
    {
        return $this->title;
    }

    public function view(): View
    {
        $this->getLastCellNumber($this->students->count());

        return view('ofa::admin.excel.sheetAthlopaideias', ['students' => $this->students]);

    }

    protected function getLastCellNumber($number_of_students)
    {
        $this->last_cell_row = $number_of_students + 1;
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $date = Carbon::now();
                                
                $headerRange = 'A1:I1'; 
                $bodyRange = 'A2:I'.$this->last_cell_row;

                $event->sheet->getDelegate()->getStyle($headerRange)->applyFromArray($this->getHeaderStyle());

                $event->sheet->getDelegate()->getRowDimension(1)->setRowHeight(31);
                for($i=2; $i<=$this->last_cell_row; $i++){
                    $event->sheet->getDelegate()->getRowDimension($i)->setRowHeight(30);
                }
                $event->sheet->getDelegate()->getColumnDimension('A')->setAutoSize(true);

                $event->sheet->getDelegate()->getStyle($bodyRange)->applyFromArray($this->getBodyStyle());

                $event->sheet->getDelegate()->getHeaderFooter()->setOddHeader('&C&H&25 ΦΥΛΛΟ ΑΓΩΝΑ ΓΙΑ '.$this->title);

                $event->sheet->getDelegate()->getHeaderFooter()->setOddFooter('&L Συντάχθηκε από την Ομάδα Φυσικής Αγωγής στις '. $date->format('d/m/Y και ώρα H:i:s'));
                $event->sheet->getDelegate()->getPageSetup()->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_PORTRAIT);
                $event->sheet->getDelegate()->getPageSetup()->setPaperSize(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::PAPERSIZE_A4);
                $event->sheet->getDelegate()->freezePane('B2');

                $event->sheet->getDelegate()->getPageSetup()->setFitToWidth(1);
                $event->sheet->getDelegate()->getPageSetup()->setFitToHeight(0);
            },
        ];
    }

    protected function getHeaderStyle() : array
    {
        return [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    // 'color' => ['argb' => 'FFFF0000'],
                ]
            ],
            'font' =>[
                'name' => 'Arial',
                'size'  => 16, 
                // 'bold' => TRUE, 
                'color' => [ 'rgb' => '000000' ]   
            ],
            'alignment' => [
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER, 
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER, 
                // 'wrapText'  => true
            ]          
        ];
    }

    protected function getBodyStyle() : array
    {
        return [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    // 'color' => ['argb' => 'FFFF0000'],
                ]
            ],
            'alignment' => [
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER, 
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER, 
            ],
            'font' =>[
                'name' => 'Arial',
                'size'  => 12, 
                // 'bold' => TRUE, 
                // 'color' => [ 'rgb' => '000000' ]   
            ],            
        ];
    }
}