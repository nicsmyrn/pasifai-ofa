<?php

namespace Pasifai\Ofa\Controllers\ExcelExports;

use App\NewEidikotita;
use App\School;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Style\Conditional;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\WithTitle;
// use Pasifai\Pysde\Controllers\Traits\ExcelTrait;

class StatementsSheetGroup2 implements FromView , WithEvents, WithTitle
{
    // use ExcelTrait;

    protected $last_cell_row;

    protected $data;

    public function title(): string
    {
        return "Ανά Αγώνισμα";
    }


    public function __construct($collection)
    {
        $this->data = $collection;    
    }

    public function view(): View
    {        
        return view('ofa::admin.excel.sheet2statements', $this->data);
    }
    


    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {

            },
        ];
    }

    protected function getMasterHeaderStyle() : array
    {
        return [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    // 'color' => ['argb' => 'FFFF0000'],
                ]
            ],
            'font' =>[
                'name' => 'Arial',
                'size'  => 20, 
                'bold' => TRUE, 
                'color' => [ 'rgb' => '000000' ]   
            ],
            'alignment' => [
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER, 
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER, 
            ]          
        ];       
    }

    protected function getHeaderStyle() : array
    {
        return [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    // 'color' => ['argb' => 'FFFF0000'],
                ]
            ],
            'font' =>[
                'name' => 'Arial',
                'size'  => 16, 
                // 'bold' => TRUE, 
                'color' => [ 'rgb' => '000000' ]   
            ],
            'alignment' => [
                'wrapText'  => true,
                'textRotation' => 90, 
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_BOTTOM, 
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER, 
            ]          
        ];
    }

    protected function getBodyStyle() : array
    {
        return [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    // 'color' => ['argb' => 'FFFF0000'],
                ]
            ],
            'alignment' => [
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER, 
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER, 
            ],
            'font' =>[
                'name' => 'Arial',
                'size'  => 18, 
                // 'bold' => TRUE, 
                // 'color' => [ 'rgb' => '000000' ]   
            ],            
        ];
    }

    protected function getRowSlugStyle() : array
    {
        return [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    // 'color' => ['argb' => 'FFFF0000'],
                ]
            ],
            'font' =>[
                'name' => 'Arial',
                'size'  => 14, 
                // 'bold' => TRUE, 
                // 'color' => [ 'rgb' => '000000' ]   
            ],  
        ];
    }

    protected function getRowNameStyle() : array
    {
        return [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    // 'color' => ['argb' => 'FFFF0000'],
                ]
            ],
            'font' =>[
                'name' => 'Arial',
                'size'  => 9, 
                // 'bold' => TRUE, 
                // 'color' => [ 'rgb' => '000000' ]   
            ],  
        ];
    }

    protected function setConditionalStylesArray() : array
    {
        $conditionalStyles = array();

        $conditional1 = new Conditional();

        $conditional1->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
        $conditional1->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_LESSTHAN);
        $conditional1->addCondition('0');
        $conditional1->getStyle()->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_DARKGREEN);
        $conditional1->getStyle()->getFont()->setBold(true);

        $conditional2 = new Conditional();
        $conditional2->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
        $conditional2->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_GREATERTHANOREQUAL);
        $conditional2->addCondition('0');
        $conditional2->getStyle()->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_RED);
        $conditional2->getStyle()->getFont()->setBold(true);

        $conditionalStyles[] = $conditional1;
        $conditionalStyles[] = $conditional2;

        return $conditionalStyles;
    }

}

/*

