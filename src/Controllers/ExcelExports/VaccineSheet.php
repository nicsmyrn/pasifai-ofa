<?php

namespace Pasifai\Ofa\Controllers\ExcelExports;

use App\NewEidikotita;
use App\School;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Style\Conditional;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\WithTitle;
// use Pasifai\Pysde\Controllers\Traits\ExcelTrait;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;


class VaccineSheet implements FromView, WithEvents, WithTitle, ShouldAutoSize
{
    // use ExcelTrait;
    
    protected $last_cell_row;

    protected $data;

    public function title(): string
    {
        return "Εκπαιδευτικοί";
    }


    public function __construct($collection)
    {
        $this->data = $collection;    
    }

    public function view(): View
    {                
        $this->getLastCellNumber();

        return view('ofa::admin.excel.sheetVaccineCovid19', $this->data);
    }
    

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {       
                $date = Carbon::now();

                $headerRange = 'A1:G1'; 
                $bodyRange = 'A2:G'.$this->last_cell_row;

                $event->sheet->getDelegate()->getStyle($headerRange)->applyFromArray($this->getHeaderStyle());

                // $event->sheet->getDelegate()->getRowDimension(1)->setRowHeight(31);
                // for($i=2; $i<=$this->last_cell_row; $i++){
                //     $event->sheet->getDelegate()->getRowDimension($i)->setRowHeight(30);
                // }
                // $event->sheet->getDelegate()->getColumnDimension('A')->setAutoSize(true);

                $event->sheet->getDelegate()->getStyle($bodyRange)->applyFromArray($this->getBodyStyle());
                $event->sheet->getDelegate()->getStyle('C1:F'.$this->last_cell_row)->applyFromArray($this->onlyCenter());


                $event->sheet->getDelegate()->getStyle('D2:D'.$this->last_cell_row)->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DMYSLASH);

                $event->sheet->getDelegate()->getHeaderFooter()->setOddHeader('&C&H&25 ΚΑΤΑΛΟΓΟΣ ΕΚΠΑΙΔΕΥΤΙΚΩΝ ΓΙΑ ΕΜΒΟΛΙΑΣΜΟ');

                $event->sheet->getDelegate()->getHeaderFooter()->setOddFooter('&L Συντάχθηκε από την ΠΑΣΙΦΑΗ στις '. $date->format('d/m/Y και ώρα H:i:s'));
                // $event->sheet->getDelegate()->getPageSetup()->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_PORTRAIT);
                $event->sheet->getDelegate()->getPageSetup()->setPaperSize(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::PAPERSIZE_A4);
                $event->sheet->getDelegate()->freezePane('B1');

                $event->sheet->getDelegate()->getPageSetup()->setFitToWidth(1);
                $event->sheet->getDelegate()->getPageSetup()->setFitToHeight(0);
            },
        ];
    }

    protected function getHeaderStyle() : array
    {
        return [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    // 'color' => ['argb' => 'FFFF0000'],
                ]
            ],
            'font' =>[
                'name' => 'Arial',
                'size'  => 16, 
                // 'bold' => TRUE, 
                'color' => [ 'rgb' => '000000' ]   
            ],
            'alignment' => [
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER, 
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER, 
                // 'wrapText'  => true
            ]          
        ];
    }

    protected function getLastCellNumber()
    {
        $this->last_cell_row = $this->data['teachers']->count() + 1;
    }

    protected function onlyCenter()
    {
        return [
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER, 
            ]          
        ];        
    }

    protected function getBodyStyle() : array
    {
        return [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    // 'color' => ['argb' => 'FFFF0000'],
                ]
            ],
            'alignment' => [
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER, 
                // 'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER, 
                'indent'     => 2,
            ],
            'font' =>[
                'name' => 'Arial',
                'size'  => 12, 
                // 'bold' => TRUE, 
                // 'color' => [ 'rgb' => '000000' ]   
            ],            
        ];
    }

}