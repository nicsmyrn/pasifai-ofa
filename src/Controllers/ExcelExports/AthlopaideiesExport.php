<?php

namespace Pasifai\Ofa\Controllers\ExcelExports;

use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Pasifai\Ofa\Controllers\ExcelExports\AthlopaideiesSheet;

class AthlopaideiesExport implements WithMultipleSheets
{
    protected $all_sheets;

    public function __construct($all_sheets)
    {
        $this->all_sheets = $all_sheets;
    }

    public function sheets(): array
    {
        $sheets = [];

        foreach($this->all_sheets as $key=>$students){
            $sheets[$key] = new AthlopaideiesSheet($students, $key);
        }
        
        return $sheets;

    }
}