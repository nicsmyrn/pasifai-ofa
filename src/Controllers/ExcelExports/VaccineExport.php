<?php

namespace Pasifai\Ofa\Controllers\ExcelExports;

use Maatwebsite\Excel\Concerns\WithMultipleSheets;


class VaccineExport implements WithMultipleSheets
{
    protected $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function sheets(): array
    {
        return [
            '1' => new VaccineSheet($this->data)
        ];
    }
}