<?php

namespace Pasifai\Ofa\Controllers\ExcelExports;

use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class StatementsExport implements WithMultipleSheets
{
    protected $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function sheets(): array
    {
        return [
            '1' => new StatementsSheetGroup1($this->data),
            '2' => new StatementsSheetGroup2($this->data)
        ];
    }
}