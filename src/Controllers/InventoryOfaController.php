<?php

namespace Pasifai\Ofa\Controllers;

use App\Http\Controllers\Controller;

class InventoryOfaController extends Controller
{
    public function __construct()
    {
        $this->middleware('ofaAdmin');
    }

    public function index()
    {
        return view('ofa::admin.inventory.create');
    }

}