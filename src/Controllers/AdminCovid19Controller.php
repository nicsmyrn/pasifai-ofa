<?php

namespace Pasifai\Ofa\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Year;
use App\School;
use Carbon\Carbon;
use Maatwebsite\Excel\Excel as ExcelExcel;
use Pasifai\Ofa\Models\Vaccine;
use Pasifai\Pysde\Models\Praxi;
use Pasifai\Ofa\Controllers\Traits\OutputOfaTrait;
use Maatwebsite\Excel\Facades\Excel;
use Pasifai\Ofa\Controllers\ExcelExports\VaccineExport;


class AdminCovid19Controller extends Controller
{

    use OutputOfaTrait;
    
    private $sort = [
        'birth'         => 'asc',
        'last_name'     => 'asc',
        'first_name'    => 'asc'
    ];
    

    public function __construct()
    {
        $this->middleware('isAdmin');
    }


public function vaccineCovid19excel()
    {
        $all = collect();

        $from = config('requests.covid_from');
        $to = config('requests.covid_to');

        $vaccine_requests = Vaccine::with('user')->whereBetween('created_at', [$from,$to])->get();
        
        foreach($vaccine_requests as $key=>$request){
            
            $user = $request->user;

            $teacher =  $user->userable;
            $myschool = $teacher->myschool;
            
            //############################

            $year = Year::where('current', true)->first();
            $praxeisList = Praxi::where('year_id', $year->id)->pluck('id');
            
            if($myschool != null && $myschool->birth != null){
                $base_placement = $myschool->placements
                ->whereIn('praxi_id', $praxeisList)
                ->whereIn('placements_type', [1,6,2,7])
                ->where('deleted_at', null)
                ->first(); // προσωρινή τοποθέτηση αν υπάρχει

                if($base_placement == null){
                    $school = $this->getProsoriniTopothetisi($myschool);
                }else{
                    $school = School::find($base_placement->to_id);

                    if($school == null){
                        $school = $this->getProsoriniTopothetisi($myschool);
                    }
                }
            //#############################                
                $teacher_rec = [
                    'last_name' => $user->last_name,
                    'first_name'    => $user->first_name,
                    'mobile'        => $teacher->mobile,
                    'phone'         => $teacher->phone,
                    'birth'         => $myschool->birth,
                    'base_school'   => $school->name,
                    'amka'          => $myschool->amka
                ];
            }
            $all->push($teacher_rec);
        }

        $sorted = $all->sort($this->compareCollection());        

        return $this->createExcelCovid19($sorted->values());
    }

    private function createExcelCovid19($teachers)
    {                
        return Excel::download(new VaccineExport(compact('teachers')), "test.xlsx");
    }

    private function getProsoriniTopothetisi($myschoolModel)
    {
        $prosorini = School::where('identifier', $myschoolModel->organiki_prosorini)->first();

        if($prosorini == null){
            $school = School::find(60);
        }else{
            $school = $prosorini;
        }

        return $school;
    }


    private function compareCollection()
    {
        return function ($a, $b) {
            foreach($this->sort as $sortField=>$orderType){
                if($a[$sortField] < $b[$sortField]){
                    return $orderType === 'asc' ? -1 : 1;
                }elseif($a[$sortField] > $b[$sortField]){
                    return $orderType === 'asc' ? 1 : -1;
                };
            }
            return 0;
        };
    }
    

}



