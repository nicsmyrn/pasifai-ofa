<?php

namespace Pasifai\Ofa\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Year;
use App\School;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Pasifai\Ofa\Mail\FormFromTeacher;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Pasifai\Ofa\Mail\Covid19Vaccine;
use Pasifai\Ofa\Mail\Covid19VaccineDelete;
use Pasifai\Ofa\Models\TeacherForm;
use Pasifai\Ofa\Models\Vaccine;
use Pasifai\Ofa\Requests\VaccineRequest;
use Pasifai\Pysde\Models\Praxi;

class TeacherApiController extends Controller
{
    protected $user;

    public function __construct()
    {
        $this->middleware('auth:api'); 
    }

    public function sentFormRequest(Request $request)
    {
        $data = [
            'fullname' => $request->get('fullname'),
            'email' => $request->get('email'),
            'phone' => $request->get('phone'),
            'subject' => $request->get('subject'),
            'message' => $request->get('message')
        ];

        $uuid = (string) Str::uuid();

        TeacherForm::create([
            'id' => $uuid, 
            'fullname' => $data['fullname'], 
            'from_user' => $request->user()->id, 
            'email' => $data['email'], 
            'subject' => $data['subject'], 
            'message' => $data['message']
        ]);

        Mail::queue(new FormFromTeacher($request->user(), $data));

        return $request->all();
    }

    public function sentVaccineRequest(VaccineRequest $request)
    {
        DB::beginTransaction();
            $teacherModel = $request->user()->userable;   
            
            $teacherModel->phone = $request->get('phone');
            $teacherModel->mobile = $request->get('mobile');
            $teacherModel->save();

            $myschoolModel = $teacherModel->myschool;          
            
            $myschoolModel->amka = $request->get('amka');
            $myschoolModel->birth = $request->get('date');
            $myschoolModel->save();

            Vaccine::create([
                'uid'   => $request->user()->id,
                'school_base'   => $request->get('school')
            ]);
        DB::commit();

        if($request->user()->provider == 'gsis'){
            $userMail = $request->user()->email;
        }elseif($request->user()->provider == 'sch.gr'){
            $userMail = $request->user()->sch_mail;
        }else{
            $userMail = '';
        }

        if($userMail != ''){
            $school = School::find($request->get('school'));
            
            if($school != null){
                $schoolName = $school->name;
            }else{
                $schoolName = 'Χωρίς Σχολείο';
            }

            $data = [
                'fullname' => $request->get('fullname'),
                'email' => $userMail,
                'phone' => $request->get('phone'),
                'mobile' => $request->get('mobile'),
                'birth' => $request->get('date'),
                'school'    => $schoolName,
                'amka' => $request->get('amka')
            ];
    
            Mail::queue(new Covid19Vaccine($request->user(), $data));
    
            return 'Η αίτηση για εμβολιασμό πραγματοποιήθηκε με επιτυχία. Θα σταλεί E-mail επιβεβαίωσης';
        }
        return 'Η αίτηση για εμβολιασμό πραγματοποιήθηκε με επιτυχία. E-mail ΔΕΝ στάλθηκε';

    }

    public function deleteVaccineRequest(Request $request)
    {
        $user_id = $request->user()->id;

        $myschool = $request->user()->userable->myschool;

        if($request->user()->provider == 'gsis'){
            $userMail = $request->user()->email;
        }elseif($request->user()->provider == 'sch.gr'){
            $userMail = $request->user()->sch_mail;
        }else{
            $userMail = '';
        }

        $record = Vaccine::where('uid', $user_id)->first();

        $record->delete();

        if($userMail != ''){
            $data = [
                'fullname' => $myschool->full_name,
                'email' => $userMail,
                'amka' => $myschool->amka
            ];
    
            Mail::queue(new Covid19VaccineDelete($request->user(), $data));
        }

        return 'Η Διαγραφή πραγματοποιήθηκε.';
    }

    public function getTeacherProfile(Request $request)
    {

        $teacherModel = $request->user()->userable;        

        $myschoolModel = $teacherModel->myschool;

        $year = Year::where('current', true)->first();
        $praxeisList = Praxi::where('year_id', $year->id)->pluck('id');
        
        $base_placement = $myschoolModel->placements
            ->whereIn('praxi_id', $praxeisList)
            ->whereIn('placements_type', [1,6,2,7])
            ->where('deleted_at', null)
            ->first(); // προσωρινή τοποθέτηση αν υπάρχει

        if($base_placement == null){
            $school = $this->getProsoriniTopothetisi($myschoolModel);
        }else{
            $school = School::find($base_placement->to_id);

            if($school == null){
                $school = $this->getProsoriniTopothetisi($myschoolModel);
            }
        }

        $birth = $myschoolModel->birth == null ? null : Carbon::parse($myschoolModel->birth)->format('Y-m-d');

        $profile = collect([
            'fullname'      => $myschoolModel->full_name,
            'middle_name'   => $teacherModel->middle_name,
            'mobile'        => $teacherModel->mobile,
            'phone'         => $teacherModel->phone,
            'email'         => $myschoolModel->email,
            'birth'         => $birth,
            'schoolBase'    => $school,
            'amka'          => $myschoolModel->amka
        ]);

        $notRegistered = true;

        $registered = Vaccine::where('uid', $request->user()->id)->first();

        if($registered != null){
            $notRegistered = false;
        }

        return compact('profile', 'notRegistered');
    }

    private function getProsoriniTopothetisi($myschoolModel)
    {
        $prosorini = School::where('identifier', $myschoolModel->organiki_prosorini)->first();

        if($prosorini == null){
            $school = School::find(60);
        }else{
            $school = $prosorini;
        }

        return $school;
    }


}