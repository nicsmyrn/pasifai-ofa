<?php

namespace Pasifai\Ofa\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Year;
use App\School;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Pasifai\Ofa\Mail\FormFromTeacher;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Pasifai\Ofa\Mail\Covid19Vaccine;
use Pasifai\Ofa\Mail\Covid19VaccineDelete;
use Pasifai\Ofa\Models\TeacherForm;
use Pasifai\Ofa\Models\Vaccine;
use Pasifai\Ofa\Requests\ChessRequest;
use Pasifai\Ofa\Requests\VaccineRequest;
use Pasifai\Pysde\Models\Praxi;

class StudentApiController extends Controller
{
    protected $user;

    public function __construct()
    {
        $this->middleware('auth:api'); 
    }

    public function sentChessRequest(ChessRequest $request)
    {
            $studentModel = $request->user()->userable;   
            
            $studentModel->student_class = $request->get('student_class');
            $studentModel->school_name = $request->get('school_name');
            $studentModel->lichess_username = $request->get('lichess_username');

            $studentModel->save();

            $userMail = $request->user()->sch_mail;

        if($userMail != ''){

            // $data = [
            //     'fullname' => $request->get('fullname'),
            //     'email' => $userMail,
            //     'phone' => $request->get('phone'),
            //     'mobile' => $request->get('mobile'),
            //     'birth' => $request->get('date'),
            //     'school'    => $schoolName,
            //     'amka' => $request->get('amka')
            // ];
    
            // Mail::queue(new Covid19Vaccine($request->user(), $data));
    
            return 'Η αίτηση για το Παγκρήτριο πρωτάθλημα Σκάκι έγινε με επιτυχία!!';
        }
        return 'Η αίτηση για το Παγκρήτριο πρωτάθλημα Σκάκι έγινε με επιτυχία!!. E-mail ΔΕΝ στάλθηκε';
    }

    public function deleteChessRequest(Request $request)
    {
        $user = $request->user();

        $studentModel = $user->userable;

        $studentModel->lichess_username = null;
        $studentModel->school_name = '';
        $studentModel->student_class = '';

        $studentModel->save();

        $userMail = $request->user()->sch_mail;


        if($userMail != ''){
            // $data = [
            //     'fullname' => $myschool->full_name,
            //     'email' => $userMail,
            //     'amka' => $myschool->amka
            // ];
    
            // Mail::queue(new Covid19VaccineDelete($request->user(), $data));
        }

        return 'Η Διαγραφή πραγματοποιήθηκε.';
    }

    public function getStudentProfile(Request $request)
    {
        $user = $request->user();

        $studentModel = $user->userable;  
        
        $profile = collect([
            'fullname'      => $user->first_name . ' ' . $user->last_name,
            'usernameSCH'   => $studentModel->uid,
            'vathmida'        => $studentModel->vathmida,
            'school_mail'         => $studentModel->school_mail,
            'school_name'         => $studentModel->school_name,
            'student_class'         => $studentModel->student_class,
            'county_name'         => $studentModel->county_name,
            'lichess_username'         => $studentModel->lichess_username,
        ]);

        $notRegistered = true;

        // $registered = Vaccine::where('uid', $request->user()->id)->first();

        if($studentModel->lichess_username != null){
            $notRegistered = false;
        }

        return compact('profile', 'notRegistered');
    }
}