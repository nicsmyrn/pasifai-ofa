<?php

namespace Pasifai\Ofa\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Pasifai\Ofa\Models\Sport;
use App\Models\Year;
use Pasifai\Ofa\Models\Student;
use Pasifai\Ofa\Requests\StudentRequest;
use Pasifai\Ofa\Controllers\Traits\OutputOfaTrait;
use Pasifai\Ofa\Models\SportList;
use Pasifai\Ofa\Models\SportParticipation;
use Pasifai\Ofa\Models\SportListDetails;
use Pasifai\Ofa\Models\SportParticipationDetails;

class SchoolGamesApiController extends Controller      //TODO must change permissions, E-mail and Notifications
{

    use OutputOfaTrait;

    protected $user;

    public function __construct()
    {
        $this->middleware('auth:api'); 
    }

    public function getAllStudents(Request $request)
    {        
        $collection = collect();

        $this->user = $request->user();

        if($this->user->userable->type == "Γυμνάσιο"){
            $route_string = 'OFA::primary.editStudent';
        }else{
            $route_string = 'OFA::editStudent';
        }

        $students =  Student::where('school_id', $this->user->userable->id)->get();

        foreach($students as $student)
        {
            $collection->push([
                'am'        =>  $student->am,
                'class'     => $student->class,
                'first_name'    => $student->first_name,
                'last_name'     => $student->last_name,
                'middle_name'   => $student->middle_name,
                'mothers_name'  => $student->mothers_name,
                'sex'           => $student->sex,
                'year_birth'    => $student->year_birth,
                'edit_url'      => route($route_string, $student->am)
            ]);
        }

        return $collection;
    }

    public function destroy(Request $request, $am)
    {
        $school_id = $request->user()->userable->id;

        $student = Student::where('school_id', $school_id)->where('am', $am)->delete();

        return ['ok'];
    }

    // public function insertNewParticipationPrimary(Request $request)
    // {
    //     if($request->ajax()){
    //         $list_id = $request->get('existedListId');

    //         $attr = [
    //             'studentsList'  => $request->get('studentsList'),
    //             'gender'   => $request->get('current_sex'),
    //             'sport' => $request->get('current_sport'),
    //             'year'       => $request->get('year'),
    //             'school'        => \Auth::user()->userable,
    //             'teacher_name'  => $request->get('teacher_name'),
    //             'synodos'       => $request->get('synodos')
    //         ];

    //         $school_folder = str_slug($attr['school']->name). '/' . $attr['year']['name'] . '/OfaSportEducation/';
    //         $sport_name = $attr['sport']['name'];

    //         $gender = $attr['gender'] == 0 ? 'ΚΟΡΙΤΣΙΩΝ' : 'ΑΓΟΡΙΩΝ';

    //         if($attr['gender'] == 0){
    //             $gender = 'ΚΟΡΙΤΣΙΩΝ';
    //         }elseif($attr['gender'] == 1){
    //             $gender = 'ΑΓΟΡΙΩΝ';
    //         }else{
    //             $gender = 'ΜΙΚΤΗ';
    //         }

    //         $file_path = $school_folder .str_slug($sport_name).'-'.str_slug($gender)  . '.pdf';

    //         if($list_id == 0){
    //             $newList = $this->createTablesInDatabase($attr, $file_path);
    //         }else{
    //             $newList = $this->updateTablesInDatabase($attr, $list_id, $file_path);
    //         }

    //         $this->sportEducationToPDF($attr, $school_folder,  $file_path, 'ofa::school.PDF.sport-education');

    //         // $this->sportListToPDF($attr, $school_folder,  $file_path, 'ofa::school.PDF.sport-list');

    //         return route('OFA::Primary::downloadPdfList',[$attr['year']['name'], $attr['sport']['name'], $attr['gender']]);
    //     }
    // }

    public function checkIfSportEducationAlreadyExists(Request $request)
    {
        if($request->ajax()){
            $data = array();
            
            $sport = $request->get('sport');
            $year_id = $request->get('year_id');
            $year_name = $request->get('year_name');
            $gender = $request->get('gender');
            $school_id = \Auth::user()->userable->id;

            // if($gender != 2){
                $list = SportList::where('year_id', $year_id)
                    ->where('school_id', $school_id)
                    ->where('sport_id', $sport['id'])
                    ->where('gender', $gender)
                    ->first();
            // }else{
                // $list = SportList::where('year_id', $year_id)
                    // ->where('school_id', $school_id)
                    // ->where('sport_id', $sport['id'])
                    // ->first();
            // }


            if($list == null){
                $data['response'] = 'canMakeSchoolSportEducation';
            }else{
                if($list->locked){
                    $data['response'] = 'isLocked';
                    $data['teacher_name'] = $list->teacher_name;
                    $data['synodos'] = $list->synodos;
                    
                    $school_type = \Auth::user()->userable->type;
                    
                    if($school_type == 'Λύκειο' || $school_type == 'ΕΠΑΛ'){
                        $data['downloadUrl'] = route('OFA::downloadPdfList',[$year_name, $sport['name'], $gender]);
                    }else{
                        $data['downloadUrl'] = route('OFA::Primary::downloadPdfList',[$year_name, $sport['name'], $gender]);
                    }
        
                }else{
                    $data['response'] = 'temporarySportEducationExists';
                    $data['list'] = $list->details->pluck('student_id')->all();
                    $data['list_id'] = $list->id;
                    $data['teacher_name'] = $list->teacher_name;
                    $data['synodos'] = $list->synodos;
                }
            }
            return $data;
        }
    }

    public function fetchDataForParticipationStatus(Request $request)
    {
        if($request->ajax()){
            $data = array();

            $data['sports'] = Sport::all();
            $data['years'] = Year::all();
            $data['students'] = \Auth::user()->userable->students;

            return $data;
        }     
    }

    public function checkIfListAlreadyExistsForParticipationStatus(Request $request)
    {
        if($request->ajax()){
            $data = array();

            $sport = $request->get('sport');
            $year_id = $request->get('year_id');
            $gender = $request->get('gender');
            $school_id = \Auth::user()->userable->id;

            $list = SportList::with('details')
                ->where('year_id', $year_id)
                ->where('school_id', $school_id)
                ->where('sport_id', $sport['id'])
                ->where('gender', $gender)
                ->where('locked', true)                 // Last change 01-02-2018
                ->first();

            if($list == null){
                $data['message'] = 'listDoesNotExist';
            }else{
                $data['message'] = 'listExistsAndCanMakeParticipationStatus';
                $data['list'] = $list->details->pluck('student_id');
                $data['list_id'] = $list->id;
                $data['synodos'] = $list->synodos;
            }

            return $data;
        } 
    }

    public function fetchStudentsFromParticipationStatus(Request $request)
    {
        if($request->ajax()) {
            $data = array();

            $data['p_status_id'] = null;
            $data['p_status_students_ids'] = array();            

            $phase = $request->get('phase');
            $list_id = $request->get('list_id');

            $participateStatus = SportParticipation::with('details')
                ->where('list_id', $list_id)
                ->where('phase', $phase)
                ->first();

            if($participateStatus != null){
                $data['p_status_id'] = $participateStatus->id;

                foreach($participateStatus->details as $detail){
                    $data['p_status_students_ids'][] = $detail->student_in_list->student_id;
                }
            }
            return $data;
        }
    }

    public function removeFromStatus(Request $request)
    {
        if($request->ajax()){

            $am = $request->get('am');
            $p_status = SportParticipation::find($request->get('p_status_id'));

            if($p_status != null){
                foreach($p_status->details as $detail){
                    if($detail->student_id == $am) $detail->delete();
                }
            }
            return $am;
        }
    }

    public function AddToStudents(Request $request)
    {
        if($request->ajax()){

            $am = $request->get('am');
            $p_status = SportParticipation::find($request->get('p_status_id'));

            if($p_status == null){
                $p_status = SportParticipation::create([
                    'list_id'   => $request->get('list_id'),
                    'description' => '',
                    'phase'     => $request->get('phase')
                ]);
            }

            $list_details_id = SportListDetails::where('list_id', $request->get('list_id'))
                ->where('student_id', $am)
                ->first()
                ->id;

            $p_status_new_detail = new SportParticipationDetails();
            $p_status_new_detail->list_details_id = $list_details_id;

            $p_status->details()->save($p_status_new_detail);

            return $p_status->id;
            return 'add to students';
        }
    }

    public function fetchDataForLists(Request $request)
    {
        if($request->ajax()){
            $data = array();

            $data['sports'] = Sport::where('individual', $request->get('individual'))->get();
            $data['year'] = Year::where('current', true)->first();
            $data['students'] = \Auth::user()->userable->students;

            return $data;
        }
    }

    public function fetchDataForSportEducation(Request $request)
    {
        if($request->ajax()){
            $data = array();

            $school_type = $request->user()->userable->type;
            if($school_type == 'Λύκειο' || $school_type == 'ΕΠΑΛ'){
                $data['sports'] = Sport::where('identifier', '1st')->get();
            }else{
                $data['sports'] = Sport::all();
            }

            $data['year'] = Year::where('current', true)->first();
            $data['students'] = \Auth::user()->userable->students;

            return $data;
        }
    }

    // public function fetchDataForListsIndividual(Request $request)
    // {
    //     if($request->ajax()){
    //         $data = array();

    //         $data['sports'] = Sport::where('individual', true)->get();
    //         $data['year'] = Year::where('current', true)->first();
    //         $data['students'] = \Auth::user()->userable->students;  // this goes the sorting

    //         return $data;
    //     }
    // }

    public function checkIfListAlreadyExists(Request $request)
    {
        if($request->ajax()){
            $data = array();

            $sport = $request->get('sport');
            $year_id = $request->get('year_id');
            $year_name = $request->get('year_name');
            $gender = $request->get('gender');
            $school_id = \Auth::user()->userable->id;
            $school_type = \Auth::user()->userable->type;

            if($sport['individual']) $data['special_sports'] = Sport::find($sport['id'])->special;

            $list = SportList::where('year_id', $year_id)
                        ->where('school_id', $school_id)
                        ->where('sport_id', $sport['id'])
                        ->where('gender', $gender)
                        ->first();

            if($list == null){
                $data['response'] = 'canMakeSchoolList';
            }else{
                $data['teacher_name'] = $list->teacher_name;
                $data['synodos'] = $list->synodos;

                if($list->locked){
                    $data['response'] = 'isLocked';

                    if($school_type == 'Λύκειο' || $school_type == 'ΕΠΑΛ'){
                        $data['downloadUrl'] = route('OFA::downloadPdfList',[$year_name, $sport['name'], $gender]);
                    }else{
                        $data['downloadUrl'] = route('OFA::Primary::downloadPdfList',[$year_name, $sport['name'], $gender]);
                    }
                }else{
                    $data['response'] = 'temporaryListExists';
                    $data['studentListIndividual'] = $list->details;
                    $data['list'] = $data['studentListIndividual']->pluck('student_id')->all();
                    $data['list_id'] = $list->id;
                }
            }
            return $data;
        }
    }

    public function checkIfListAlreadyExistsForIndividual(Request $request)
    {
        if($request->ajax()){
            $data = array();

            $sport = $request->get('sport');
            $year_id = $request->get('year_id');
            $school_id = \Auth::user()->userable->id;

            $data['special_sports'] = Sport::find($sport['id'])->special;

            $list = SportList::where('year_id', $year_id)
                ->where('school_id', $school_id)
                ->where('sport_id', $sport['id'])
                ->first();

            if($list == null){
                $data['response'] = 'canMakeSchoolListIndividual';
            }else{
                if($list->locked){
                    $data['response'] = 'isLockedIndividual';
                    $data['synodos'] = $list->synodos;
                }else{
                    $data['response'] = 'temporaryListExistsIndividual';
                    $data['list'] = $list->details->pluck('student_id')->all();
                    $data['studentListIndividual'] = $list->details;
                    $data['list_id'] = $list->id;
                    $data['synodos'] = $list->synodos;
                }
            }
            return $data;
        }

    }

    public function checkIfParticipationPrimaryAlreadyExists(Request $request)
    {
        return $request->all();

    }

    public function insertNewList(Request $request)
    {
        if($request->ajax()){
            
            $list_id = $request->get('existedListId');

            $attr = [
                'studentsList'  => $request->get('studentsList'),
                'gender'   => $request->get('current_sex'),
                'sport' => $request->get('current_sport'),
                'year'       => $request->get('year'),
                'school'        => \Auth::user()->userable,
                'teacher_name'  => $request->get('teacher_name'),
                'synodos'       => $request->get('synodos')
            ];

            if($attr['school']['type'] === 'Γυμνάσιο'){
                $subfolder = 'OfaSportEducation';
            }else{
                $subfolder = 'OfaLists';
            }

            $school_folder = str_slug($attr['school']->name). '/' . $attr['year']['name'] . '/'.$subfolder.'/'; 
            $sport_name = $attr['sport']['name'];

            if($attr['gender'] == 0){
                $gender = 'ΚΟΡΙΤΣΙΩΝ';
            }elseif($attr['gender'] == 1){
                $gender = 'ΑΓΟΡΙΩΝ';
            }else{
                $gender = 'ΜΙΚΤΗ';
            }

            $file_path = $school_folder .str_slug($sport_name).'-'.str_slug($gender)  . '.pdf';

            if($list_id == 0){
                $newList = $this->createTablesInDatabase($attr, $file_path);
            }else{
                $newList = $this->updateTablesInDatabase($attr, $list_id, $file_path);
            }

            if($request->get('isIndividual')){
                if($attr['school']['type'] === 'Γυμνάσιο'){
                    $file_path_for_ofa = $school_folder . 'for_OFA_only-' .str_slug($sport_name).'-'.str_slug($gender)  . '.pdf';
                    $this->sportEducationToPDF($attr, $school_folder,  $file_path, 'ofa::school.PDF.sport-education-individual', $file_path_for_ofa, 'ofa::school.PDF.sport-education-for-ofa-only');
                }else{
                    $this->sportListToPDF($attr, $school_folder,  $file_path, 'ofa::school.PDF.sport-list-individual');
                }
            }else{
                if($attr['school']['type'] === 'Γυμνάσιο'){
                    $this->sportEducationToPDF($attr, $school_folder,  $file_path, 'ofa::school.PDF.sport-education');
                }else{
                    $this->sportListToPDF($attr, $school_folder,  $file_path, 'ofa::school.PDF.sport-list');
                }
            }

            if($attr['school']['type'] === 'Γυμνάσιο'){
                return route('OFA::Primary::downloadPdfList',[$attr['year']['name'], $attr['sport']['name'], $attr['gender']]);
            }
            return route('OFA::downloadPdfList',[$attr['year']['name'], $attr['sport']['name'], $attr['gender']]);

        }
    }

 
    public function temporarySaveNewList(Request $request)
    {
        if($request->ajax()){
            $list_id = $request->get('existedListId');
            $attr = [
                'studentsList'  => $request->get('studentsList'),
                'gender'   => $request->get('current_sex'),
                'sport' => $request->get('current_sport'),
                'year'       => $request->get('year'),
                'school'        => \Auth::user()->userable,
                'teacher_name'  => $request->get('teacher_name'),
                'synodos'       => $request->get('synodos')
            ];
            
            if($list_id == 0){
                $newList = $this->createTablesInDatabase($attr);
            }else{
                $newList = $this->updateTablesInDatabase($attr, $list_id);
            }
            return $newList->id;
        }
    }

    public function insertNewStudent(StudentRequest $request) // StudentRequest $request
    {
        if($request->ajax()){
            $new_student = $request->get('new_student');

            $student = Student::create($new_student + [
                    'school_id'     => \Auth::user()->userable->id
                ]);
            return $student;
        }
    }

    public function insertNewStudentPrimary(Request $request)
    {
        return $request->all();
        
    }

}