<?php

namespace Pasifai\Ofa\Controllers\API;

use App\Http\Controllers\Controller;
use Pasifai\Ofa\Models\InventoryCategory;
use Illuminate\Http\Request;
use Pasifai\Ofa\Models\Inventory;
use Pasifai\Ofa\Requests\InventoryRequest;

class AdminInventoryApiController extends Controller
{
    public function __construct()
    {
        // $this->middleware('ofaAdmin');
        $this->middleware('auth:api'); 
    }

    public function getCategories()
    {
        return InventoryCategory::all();
    }

    public function saveInventory(InventoryRequest $request)
    {        
        $remarks = $request->get('description') != null ? $request->get('description') : '';

        $new = Inventory::create([
            'name' => $request->get('name'),
            'quantity' => $request->get('quantity'),
            'date_in' => $request->get('date_in'),
            'donation' => $request->get('donation'),
            'amount' => $request->get('amount'),
            'remarks' => $remarks,
            'category_id' => $request->get('category'),
        ]);

        return "Το υλικό {$new->name} αποθηκεύτηκε με επιτυχία...";
    }
}