<?php

return array(
    'stivos_extra_students' => env("STIVOS_EXTRA_STUDENTS",2),
    'ofa_offline'   => env("OFA_OFFLINE", false),

    'ofa_school_type' => [
        0 => 'ΛΥΚΕΙΩΝ',
        1 => 'ΓΥΜΝΑΣΙΩΝ'
    ],

    'middleware' => ['auth'],

    'covid_available' => env("COVID_AVAILABLE", false),
    'covid_from' => env("COVID_FROM", '2021-03-31 00:33:39'),
    'covid_to' => env("COVID_TO", '2021-04-01 23:28:04'),

//    'user_class' => \App\User::class,

    'guard' => 'web',

    'MAIL_OFA' => env('MAIL_OFA', 'nicsmyrn@gmail.com'),
    'NAME_OFA' => env('NAME_OFA', 'Test User OFA'),
    'CITY_OFA' => env('CITY_OFA', 'Χανιά'),

    'OFA_STATEMENTS_END' => env('OFA_STATEMENTS_END', '2019/11/12 12:00:00'),

    'class' => [
        1 => 'Α',
        2 => 'Β',
        3 => 'Γ',
        4 => 'Δ',
        5 => 'Ε',
        6 => 'ΣΤ'
    ],

);