copy js files to published folder


composer require pasifai/ofa

php artisan db:seed --class=Pasifai\\Ofa\\Seeders\\DatabaseSeeder

To the server: 
1. composer update
2. php artisan vendor:publish --tag=ofa_public --force
3. change to server mix-manifest.json with the one in published folder

To the client:
    git tag v.2.0.1
    git push or v.3.1.2